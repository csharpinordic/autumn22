﻿using System.Collections;
using System.Text;

namespace Recursion
{
    public class Program
    {
        public static long Factorial(int n)
        {
            // 0! = 1
            if (n == 0) return 1;
            // Факториал над отрицательными числами не определён
            if (n < 0) return 0;
            checked
            {
                long f = n * Factorial(n - 1);
                return f;
            }
        }

        static int counter = 0;

        /// <summary>
        /// Подсчёт чисел Фибоначчи рекурсией
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        static long Fibonacci(int n)
        {
            counter++;
            if (n == 0) return 0;
            if (n == 1) return 1;
            return Fibonacci(n - 1) + Fibonacci(n - 2);
        }

        /// <summary>
        /// Генерация последовательности чисел Фибоначчи
        /// </summary>
        /// <returns></returns>
        static IEnumerator<long> GetFibonacci()
        {
            // yield return "привет";
            long first = 0;
            yield return first;

            long second = 1;
            yield return second;

            while (true) // for (int i = 2; i < count; i++)
            {
                // проверка на целочисленное переполнение
                long next;
                try
                {
                    next = checked(first + second);
                }
                catch
                {
                    yield break; // завершаем обработку при переполнении
                }
                yield return next;
                first = second;
                second = next;
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.Write("Введите число > ");
            int n = int.Parse(Console.ReadLine());

            IEnumerator e = GetFibonacci();
            while (e.MoveNext())
            {
                Console.Write($"{e.Current} ");
            }
            Console.WriteLine();

            // long f = Factorial(n);
            // Console.WriteLine($"{n}! = {f}");
            // Console.WriteLine($"F({n}) = {Fibonacci(n)}");
            // Console.WriteLine($"counter = {counter}");
        }
    }
}