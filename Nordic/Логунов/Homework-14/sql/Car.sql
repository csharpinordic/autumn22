/****** Object:  Table [dbo].[Car]    Script Date: 12.11.2022 21:28:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Car](
	[Number] [nvarchar](9) NOT NULL,
	[VIN] [nvarchar](17) NOT NULL,
	[Year] [smallint] NOT NULL,
	[ChassisNumber] [nvarchar](50) NULL,
	[MotorPowerKW] [decimal](18, 1) NULL,
	[MotorPowerLS] [decimal](18, 1) NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


