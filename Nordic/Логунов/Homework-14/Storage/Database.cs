﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Homework_14.Extensions;

namespace Homework_14.Storage
{

    internal class Database : IDisposable
    {
        /// <summary>
        /// Соединение с БД
        /// </summary>
        private SqlConnection _connection;

        internal Database()
        {
            _connection = new SqlConnection(ConfigurationManager.Configuration.GetSection("ConnectionString").Value);
        }

        /// <summary>
        /// Создание SQL-команды с открытием соедиения при необходимости
        /// </summary>
        /// <returns></returns>
        private SqlCommand CreateCommand()
        {
            // Проверка на повторное открытие соединения
            if (_connection.State != System.Data.ConnectionState.Open)
            {
                _connection.Open();
            }

            return _connection.CreateCommand();
        }

        /// <summary>
        /// Добавление автомобиля
        /// </summary>
        /// <param name="car"></param>
        public void AddCar(Car car)
        {
            using var command = CreateCommand();
            command.CommandText = "INSERT INTO Car(Number, VIN, Year, ChassisNumber, MotorPowerKW, MotorPowerLS)" +
                "VALUES (@Number, @Vin, @Year, @ChassisNumber, @MotorPowerKW, @MotorPowerLS)";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("Number", car.Number);
            command.Parameters.AddWithValue("Vin", car.Vin);
            command.Parameters.AddWithValue("Year", car.Year);
            command.Parameters.AddWithNullValue("ChassisNumber", car.ChassisNumber);
            command.Parameters.AddWithNullValue("MotorPowerKW", car.MotorPowerKW);
            command.Parameters.AddWithNullValue("MotorPowerLS", car.MotorPowerLS);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Обновление автомобиля
        /// </summary>
        /// <param name="car"></param>
        public void UpdateCar(Car car)
        {
            using var command = CreateCommand();
            command.CommandText = "UPDATE Car SET " +
                "VIN=@Vin," +
                "Year=@Year," +
                "ChassisNumber=@ChassisNumber," +
                "MotorPowerKW=@MotorPowerKW," +
                "MotorPowerLS=@MotorPowerLS " +
                "WHERE Number=@Number";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("Number", car.Number);
            command.Parameters.AddWithValue("Vin", car.Vin);
            command.Parameters.AddWithValue("Year", car.Year);
            command.Parameters.AddWithNullValue("ChassisNumber", car.ChassisNumber);
            command.Parameters.AddWithNullValue("MotorPowerKW", car.MotorPowerKW);
            command.Parameters.AddWithNullValue("MotorPowerLS", car.MotorPowerLS);

            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Получение автомобиля
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public Car GetCar(string number)
        {
            using var command = CreateCommand();
            command.CommandText = "SELECT [number], [vin], [year], [chassisNumber], [MotorPowerKW], [MotorPowerLS]" +
                " FROM Car WHERE Number = @Number";
            command.Parameters.Clear();
            command.Parameters.AddWithValue("Number", number);

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    var num = (string)reader[0];
                    var vin = (string)reader[1];
                    var year = (short)reader[2];
                    string chassisNumber = reader[3] == DBNull.Value ? null : (string)reader[3];
                    decimal? motorPowerKW = reader[4] == DBNull.Value ? null : (decimal)reader[4];
                    decimal? motorPowerLS = reader[5] == DBNull.Value ? null : (decimal)reader[5];

                    return new Car(num, vin, year)
                    {
                        ChassisNumber = chassisNumber,
                        MotorPowerKW = motorPowerKW,
                        MotorPowerLS = motorPowerLS
                    };
                }
            }

            return null;
        }

        /// <summary>
        /// Очистка управляемых и неуправляемых ресурсов
        /// </summary>
        public void Dispose()
        {
            if (_connection.State == System.Data.ConnectionState.Open)
                _connection.Close();

            _connection.Dispose();
        }
    }
}
