﻿using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Homework_14
{
    /// <summary>
    /// Класс транспортных средств
    /// </summary>
    internal class Car
    {

        #region "Variables"
        /// <summary>
        /// Словать для интерпретации русских символов в английские
        /// </summary>
        private static readonly Dictionary<char, char> _replaceableNumberChars = new Dictionary<char, char>()
        {
            {'А', 'A' },
            {'В', 'B' },
            {'Е', 'E' },
            {'К', 'K' },
            {'М', 'M' },
            {'Н', 'H' },
            {'О', 'O' },
            {'Р', 'P' },
            {'С', 'C' },
            {'Т', 'T' },
            {'У', 'Y' },
            {'Х', 'X' }
        };


        /// <summary>
        /// Регистрационный номер автомобиля
        /// </summary>
        private string _number;

        /// <summary>
        /// Vin (идентификационный номер)
        /// </summary>
        private string _vin;

        /// <summary>
        /// Год выпуска
        /// </summary>
        private int _year;

        /// <summary>
        /// Номер шасси
        /// </summary>
        private string? _chassisNumber;

        /// <summary>
        /// Мощность двигателя (квт)
        /// </summary>
        private decimal? _motorPowerKW;

        /// <summary>
        /// Мощность двигателя (л\с)
        /// </summary>
        private decimal? _motorPowerLS;
        #endregion

        #region "Properties"
        /// <summary>
        /// Регистрационный номер автомобиля
        /// </summary>
        public string Number
        {
            get => _number;
            set
            {
                value = value.ToUpper();

                foreach(var replaceableChar in _replaceableNumberChars)
                    value = value.Replace(replaceableChar.Key, replaceableChar.Value);
                
                var match = Regex.Match(value, "^([A,B,E,K,M,H,O,P,C,T,Y,X]{1})([0-9]{3})([A,B,E,K,M,H,O,P,C,T,Y,X]{2})([0-9]{2,3}$)");

                if (!match.Success)
                    throw new ArgumentException("Неверный формат номера.", nameof(Number));
                
                _number = value;
            }
        }

        /// <summary>
        /// Vin (идентификационный номер)
        /// </summary>
        public string Vin
        {
            get => _vin;
            set
            {
                var match = Regex.Match(value, "^([0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,R,S,T,U,V,W,X,Y,Z]{17})$");

                if (!match.Success)
                    throw new ArgumentException("Неверный формат VIN.", nameof(Vin));

                _vin = value;
            }
        }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public int Year
        {
            get => _year;
            set
            {
                if (value > DateTime.Now.Year || value < 1983)
                    throw new ArgumentException("Неверный формат года выпуска.", nameof(Year));

                _year = value;
            }
        }

        /// <summary>
        /// Номер шасси
        /// </summary>
        public string? ChassisNumber
        {
            get => _chassisNumber;
            set => _chassisNumber = value;
        }

        /// <summary>
        /// Мощность двигателя (квт)
        /// </summary>
        public decimal? MotorPowerKW
        {
            get => _motorPowerKW;
            set => _motorPowerKW = value;
        }

        /// <summary>
        /// Мощность двигателя (л\с)
        /// </summary>
        public decimal? MotorPowerLS
        {
            get => _motorPowerLS;
            set => _motorPowerLS = value;
        }
        #endregion

        #region "Constructors"
        /// <summary>
        /// Создает экземпляр класса транспортных средств
        /// </summary>
        /// <param name="number">Регистрационный номер</param>
        /// <param name="vin">Идентификационный номер</param>
        /// <param name="year">Год выпуска</param>
        public Car(string number, string vin, int year)
        {
            Number = number;
            Vin = vin;
            Year = year;
        }
        #endregion

        #region "Override"
        public override string ToString() => Number;
        #endregion
    }
}
