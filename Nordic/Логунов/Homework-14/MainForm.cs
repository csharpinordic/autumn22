using Homework_14.Storage;

namespace Homework_14
{
    public partial class MainForm : Form
    {
        Database _database;
        public MainForm()
        {
            InitializeComponent();
            _database = new Database();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tbMotorPowerLS.Text) && string.IsNullOrEmpty(tbMotorPowerKW.Text))
                    throw new ArgumentException("���������� ������� �������� ��������� � ����� �� ������ ���������.");

                if (!int.TryParse(tbYear.Text, out int year))
                    throw new ArgumentException("�������� ������ ��������� ������ ���� �������.", nameof(tbYear));

                decimal motorPowerKW = 0;
                if (!string.IsNullOrEmpty(tbMotorPowerKW.Text) && !decimal.TryParse(tbMotorPowerKW.Text, out motorPowerKW))
                    throw new ArgumentException("�������� ������ ��������� ������ �������� ��������� (���).", nameof(tbMotorPowerKW));

                decimal motorPowerLS = 0;
                if (!string.IsNullOrEmpty(tbMotorPowerLS.Text) && !decimal.TryParse(tbMotorPowerLS.Text, out motorPowerLS))
                    throw new ArgumentException("�������� ������ ��������� ������ �������� ��������� (�\\�).", nameof(tbMotorPowerLS));

                Car car = _database.GetCar(tbNumber.Text);
                if (car != null)
                {
                    car.Vin = tbVin.Text;
                    car.Year = year;
                    car.ChassisNumber = string.IsNullOrEmpty(tbChassisNumber.Text) ? null : tbChassisNumber.Text;
                    car.MotorPowerKW = string.IsNullOrEmpty(tbMotorPowerKW.Text) ? null : motorPowerKW;
                    car.MotorPowerLS = string.IsNullOrEmpty(tbMotorPowerLS.Text) ? null : motorPowerLS;

                    _database.UpdateCar(car);
                    MessageBox.Show("������ ��������!", "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                car = new Car(tbNumber.Text, tbVin.Text, year)
                {
                    ChassisNumber = string.IsNullOrEmpty(tbChassisNumber.Text) ? null : tbChassisNumber.Text,
                    MotorPowerKW = string.IsNullOrEmpty(tbMotorPowerKW.Text) ? null : motorPowerKW,
                    MotorPowerLS = string.IsNullOrEmpty(tbMotorPowerLS.Text) ? null: motorPowerLS
                };

                _database.AddCar(car);
                MessageBox.Show("������ ������!", "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}