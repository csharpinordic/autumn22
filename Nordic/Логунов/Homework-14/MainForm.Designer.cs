﻿namespace Homework_14
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.tbVin = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.labelNumber = new System.Windows.Forms.Label();
            this.labelVin = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.labelChassisNumber = new System.Windows.Forms.Label();
            this.tbChassisNumber = new System.Windows.Forms.TextBox();
            this.labelMotorPowerKW = new System.Windows.Forms.Label();
            this.labelMotorPowerLS = new System.Windows.Forms.Label();
            this.tbMotorPowerKW = new System.Windows.Forms.TextBox();
            this.tbMotorPowerLS = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(355, 40);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(259, 39);
            this.tbNumber.TabIndex = 0;
            // 
            // tbVin
            // 
            this.tbVin.Location = new System.Drawing.Point(355, 97);
            this.tbVin.Name = "tbVin";
            this.tbVin.Size = new System.Drawing.Size(259, 39);
            this.tbVin.TabIndex = 1;
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(355, 157);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(259, 39);
            this.tbYear.TabIndex = 2;
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(48, 43);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(89, 32);
            this.labelNumber.TabIndex = 3;
            this.labelNumber.Text = "Номер";
            // 
            // labelVin
            // 
            this.labelVin.AutoSize = true;
            this.labelVin.Location = new System.Drawing.Point(64, 100);
            this.labelVin.Name = "labelVin";
            this.labelVin.Size = new System.Drawing.Size(53, 32);
            this.labelVin.TabIndex = 4;
            this.labelVin.Text = "VIN";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(21, 160);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(150, 32);
            this.labelYear.TabIndex = 5;
            this.labelYear.Text = "Год выпуска";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(355, 409);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(259, 46);
            this.btnCreate.TabIndex = 6;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // labelChassisNumber
            // 
            this.labelChassisNumber.AutoSize = true;
            this.labelChassisNumber.Location = new System.Drawing.Point(11, 230);
            this.labelChassisNumber.Name = "labelChassisNumber";
            this.labelChassisNumber.Size = new System.Drawing.Size(163, 32);
            this.labelChassisNumber.TabIndex = 7;
            this.labelChassisNumber.Text = "Номер шасси";
            // 
            // tbChassisNumber
            // 
            this.tbChassisNumber.Location = new System.Drawing.Point(355, 227);
            this.tbChassisNumber.Name = "tbChassisNumber";
            this.tbChassisNumber.Size = new System.Drawing.Size(259, 39);
            this.tbChassisNumber.TabIndex = 3;
            // 
            // labelMotorPowerKW
            // 
            this.labelMotorPowerKW.AutoSize = true;
            this.labelMotorPowerKW.Location = new System.Drawing.Point(12, 294);
            this.labelMotorPowerKW.Name = "labelMotorPowerKW";
            this.labelMotorPowerKW.Size = new System.Drawing.Size(297, 32);
            this.labelMotorPowerKW.TabIndex = 9;
            this.labelMotorPowerKW.Text = "Мощность двигателя(кВт)";
            // 
            // labelMotorPowerLS
            // 
            this.labelMotorPowerLS.AutoSize = true;
            this.labelMotorPowerLS.Location = new System.Drawing.Point(12, 356);
            this.labelMotorPowerLS.Name = "labelMotorPowerLS";
            this.labelMotorPowerLS.Size = new System.Drawing.Size(301, 32);
            this.labelMotorPowerLS.TabIndex = 10;
            this.labelMotorPowerLS.Text = "Мощность двигателя (л\\с)";
            // 
            // tbMotorPowerKW
            // 
            this.tbMotorPowerKW.Location = new System.Drawing.Point(355, 294);
            this.tbMotorPowerKW.Name = "tbMotorPowerKW";
            this.tbMotorPowerKW.Size = new System.Drawing.Size(259, 39);
            this.tbMotorPowerKW.TabIndex = 4;
            // 
            // tbMotorPowerLS
            // 
            this.tbMotorPowerLS.Location = new System.Drawing.Point(355, 353);
            this.tbMotorPowerLS.Name = "tbMotorPowerLS";
            this.tbMotorPowerLS.Size = new System.Drawing.Size(259, 39);
            this.tbMotorPowerLS.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 475);
            this.Controls.Add(this.tbMotorPowerLS);
            this.Controls.Add(this.tbMotorPowerKW);
            this.Controls.Add(this.labelMotorPowerLS);
            this.Controls.Add(this.labelMotorPowerKW);
            this.Controls.Add(this.tbChassisNumber);
            this.Controls.Add(this.labelChassisNumber);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.labelVin);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.tbVin);
            this.Controls.Add(this.tbNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Регистрация транспортного средства";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox tbNumber;
        private TextBox tbVin;
        private TextBox tbYear;
        private Label labelNumber;
        private Label labelVin;
        private Label labelYear;
        private Button btnCreate;
        private Label labelChassisNumber;
        private TextBox tbChassisNumber;
        private Label labelMotorPowerKW;
        private Label labelMotorPowerLS;
        private TextBox tbMotorPowerKW;
        private TextBox tbMotorPowerLS;
    }
}