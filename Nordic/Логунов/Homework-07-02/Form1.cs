namespace Homework_07_02
{
	public partial class Form1 : Form
	{
		private double a;
		private double k;
		private int step;

		public Form1()
		{
			InitializeComponent();
			step = trackStep.Value;
			trackLabel.Text = trackStep.Value.ToString();
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			Pen pen1 = new Pen(Color.Black, 3);
			Pen pen2 = new Pen(Color.Red, 3);

			if (step == 0)
				return;

			// ������������ ���
			e.Graphics.DrawLine(pen1, 0, panel1.Height / 2, panel1.Width, panel1.Height / 2);
			e.Graphics.DrawLine(pen1, panel1.Width / 2, 0, panel1.Width / 2, panel1.Height);

			float r1 = (float)(a * Math.Sin(k * 0));
			float x1 = (float)(r1 * Math.Cos(0)) + (panel1.Width / 2);
			float y1 = (panel1.Height / 2) - ((float)(r1 * Math.Sin(0)));
			float radians;

			for (int delta = 0; delta <= 360; delta += step)
			{
				radians = (float)(delta * (Math.PI / 180));
				float r = (float)(a * Math.Sin(k * radians));
				float x = (float)(r * Math.Cos(radians)) + (panel1.Width / 2);
				float y = (panel1.Height / 2) - (float)(r * Math.Sin(radians));

				e.Graphics.DrawLine(pen2, x1, y1, x, y);

				x1 = x;
				y1 = y;
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			panel1.Invalidate();
		}

		private void trackStep_ValueChanged(object sender, EventArgs e)
		{
			step = trackStep.Value;
			trackLabel.Text = trackStep.Value.ToString();
		}

		private void panel1_SizeChanged(object sender, EventArgs e)
		{
			panel1.Invalidate();
		}

		private void tbA_TextChanged(object sender, EventArgs e)
		{
			var textBox = (TextBox)sender;
			if (!double.TryParse(textBox.Text, out double parsedValue))
			{
				textBox.BackColor = Color.Red;
				return;
			}

			if (textBox == tbA)
				a = parsedValue;
			else if (textBox == tbK)
				k = parsedValue;
			else
				throw new ArgumentException(); // ����������� textBox

			textBox.BackColor = Color.White;
		}
	}
}