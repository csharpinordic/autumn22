﻿namespace Homework_07_02
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbA = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.tbK = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.trackStep = new System.Windows.Forms.TrackBar();
			this.trackLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackStep)).BeginInit();
			this.SuspendLayout();
			// 
			// tbA
			// 
			this.tbA.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.tbA.Location = new System.Drawing.Point(532, 459);
			this.tbA.Name = "tbA";
			this.tbA.Size = new System.Drawing.Size(427, 39);
			this.tbA.TabIndex = 0;
			this.tbA.TextChanged += new System.EventHandler(this.tbA_TextChanged);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(947, 434);
			this.panel1.TabIndex = 1;
			this.panel1.SizeChanged += new System.EventHandler(this.panel1_SizeChanged);
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(497, 462);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 32);
			this.label1.TabIndex = 2;
			this.label1.Text = "A";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(468, 55);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(8, 8);
			this.button1.TabIndex = 4;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button2.Location = new System.Drawing.Point(756, 549);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(203, 45);
			this.button2.TabIndex = 6;
			this.button2.Text = "Нарисовать";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// tbK
			// 
			this.tbK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.tbK.Location = new System.Drawing.Point(532, 504);
			this.tbK.Name = "tbK";
			this.tbK.Size = new System.Drawing.Size(427, 39);
			this.tbK.TabIndex = 7;
			this.tbK.TextChanged += new System.EventHandler(this.tbA_TextChanged);
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(498, 507);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(28, 32);
			this.label2.TabIndex = 8;
			this.label2.Text = "K";
			// 
			// trackStep
			// 
			this.trackStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.trackStep.Location = new System.Drawing.Point(12, 453);
			this.trackStep.Maximum = 360;
			this.trackStep.Minimum = 1;
			this.trackStep.Name = "trackStep";
			this.trackStep.Size = new System.Drawing.Size(427, 90);
			this.trackStep.TabIndex = 9;
			this.trackStep.Value = 1;
			this.trackStep.ValueChanged += new System.EventHandler(this.trackStep_ValueChanged);
			// 
			// trackLabel
			// 
			this.trackLabel.AutoSize = true;
			this.trackLabel.Location = new System.Drawing.Point(205, 504);
			this.trackLabel.Name = "trackLabel";
			this.trackLabel.Size = new System.Drawing.Size(27, 32);
			this.trackLabel.TabIndex = 10;
			this.trackLabel.Text = "0";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(971, 602);
			this.Controls.Add(this.trackLabel);
			this.Controls.Add(this.trackStep);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tbK);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.tbA);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.trackStep)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private TextBox tbA;
		private Panel panel1;
		private Label label1;
		private Button button1;
		private Button button2;
		private TextBox tbK;
		private Label label2;
		private TrackBar trackStep;
		private Label trackLabel;
	}
}