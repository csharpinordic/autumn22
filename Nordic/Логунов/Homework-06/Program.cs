﻿namespace Homework_06
{
	internal class Program
	{
		/// <summary>
		///  Символ для отображения рисунков
		/// </summary>
		private const char symbol = 'X';

		/// <summary>
		/// Точка входа в программу
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			int size = ReadNumber();
			int variant = ReadVariant();
			var array = CreateArrayAndInitialize(variant, size);
			PrintArray(array);
		}

		/// <summary>
		/// Создает массив и инициализирует в соотвтетсвии с вариантом 
		/// </summary>
		/// <param name="variant"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		private static char[,] CreateArrayAndInitialize(int variant, int size)
		{
			char[,] array = new char[size, size];
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					array[i, j] = ' ';
				}
			}

			switch (variant)
			{
				case 1:
					InitializeFirstVariantArray(array);
					break;
				case 2:
					InitializeSecondVariantArray(array);
					break;
				case 3:
					InitializeThirdVariantArray(array);
					break;
				default: break;
			}

			return array;
		}

		/// <summary>
		/// Считывает из потока ввода номер варианта (1-3) рисунка
		/// Выполняется до тех пор, пока не будет введен корректный номер
		/// </summary>
		/// <param name="array"></param>
		static int ReadVariant()
		{
			while (true)
			{
				Console.Write("Введите номер варианта рисунка (1-3) > ");
				string inputString = Console.ReadLine().Trim();

				switch (inputString)
				{
					case "1":
						return 1;
					case "2":
						return 2;
					case "3":
						return 3;
					default:
						Console.WriteLine("Неверный номер варианта.");
						break;
				}
			}
		}

		/// <summary>
		/// Метод принимающий поток ввода из консоли и преобразующий в целочисленное значение
		/// Выполняется до тех пор, пока не будет введено корректное целочисленнное значение от 1 до 32
		/// или символ пустой строки, или пробел
		/// </summary>
		/// <returns></returns>
		static int ReadNumber()
		{
			while (true)
			{
				Console.Write("Введите размер массива > ");
				string inputString = Console.ReadLine().Trim();

				switch (inputString)
				{
					case "":
						return 8;
					default:
						int value;

						if (int.TryParse(inputString, out value) && value > 0 && value < 33)
						{
							return value;
						}
						continue;
				}
			}
		}

		/// <summary>
		/// Выводит символы массива на экран
		/// </summary>
		/// <param name="array"></param>
		static void PrintArray(char[,] array)
		{
			for (int i = 0; i < array.GetLength(0); i++)
			{
				for (int j = 0; j < array.GetLength(1); j++)
				{
					Console.Write(array[i, j] + " ");
				}
				Console.WriteLine();
			}
		}

		/// <summary>
		///  Инициализирует массив символами первого варианта рисунка
		/// </summary>
		/// <param name="array"></param>
		static void InitializeFirstVariantArray(char[,] array)
		{
			for (int row = 0; row < array.GetLength(0); row++)
			{
				for (int cell = 0; cell < array.GetLength(1); cell++)
				{
					if (cell == 0 || cell == array.GetLength(1) - 1 ||
						row == 0 || row == array.GetLength(0) - 1)
						array[row, cell] = symbol;
				}
			}
		}
		/// <summary>
		/// Инициализирует массив символами второго варианта рисунка
		/// </summary>
		/// <param name="array"></param>
		static void InitializeSecondVariantArray(char[,] array)
		{
			for (int row = 0; row < array.GetLength(0); row++)
			{
				array[row, row] = symbol;
			}

			for (int row2 = 0; row2 < array.GetLength(0); row2++)
			{
				array[row2, array.GetLength(0) - 1 - row2] = symbol;
			}
		}

		/// <summary>
		///  Инициализирует массив символами третьего варианта рисунка
		/// </summary>
		/// <param name="array"></param>
		static void InitializeThirdVariantArray(char[,] array)
		{
			// середина массива
			int middleIndex = (array.GetLength(0) - 1) / 2;

			for (int row = 0; row <= middleIndex; row++)
			{
				array[row, middleIndex - row] = symbol;
				array[row, array.GetLength(0) - 1 - middleIndex + row] = symbol;
				array[array.GetLength(0) - 1 - middleIndex + row, row] = symbol;
				array[array.GetLength(0) - 1 - middleIndex + row, array.GetLength(0) - 1 - row] = symbol;
			}
		}
	}
}