﻿using System.Xml.Serialization;

namespace Homework_11.model
{
	public class UserVariable 
	{
		[XmlElement("name")]
		public string Name;

        [XmlElement("defaultValue")]
        public string DefaultValue;

		/// <summary>
		/// Строковое изображение объекта
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return Name;
		}
	}
}
