﻿using System.Xml.Serialization;

namespace Homework_11.model
{
	public class BrightAuthor
	{
		[XmlAttribute("version")]
		public string Version;

		[XmlAttribute("BrightAuthorVersion")]
		public string BrightAuthorVersion;

		[XmlAttribute("type")]
		public string Type;

		[XmlElement("meta")]
		public BrightAuthorMeta Meta;
	}
}
