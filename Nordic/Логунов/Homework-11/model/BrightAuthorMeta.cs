﻿using System.Xml.Serialization;

namespace Homework_11.model
{
	public class BrightAuthorMeta
	{
		[XmlArray("userVariables")]
		[XmlArrayItem("userVariable")]
		public UserVariable[] UserVariables;
	}
}
