using Homework_11.model;
using System.Xml;
using System.Xml.Serialization;

namespace Homework_11
{
	public partial class Form1 : Form
	{
		private const string filename = "homework-11.xml";

		public Form1()
		{
			InitializeComponent();
			RuntimeInitialize();
		}

		private void RuntimeInitialize()
		{
			var obj = DeserializeXmlFile(filename);
			if (obj == null)
				return;

			lbVersionText.Text = obj.Version;
			lbAutorVersionText.Text = obj.BrightAuthorVersion;
			lbTypeText.Text = obj.Type;
			
			lbNames.DataSource = obj.Meta.UserVariables;
		}

		[Obsolete("����� �� ������������")]
		private string[] GetSortedVariableNames(BrightAuthor obj)
		{
			string[] names = new string[obj.Meta.UserVariables.Length];
			int i = 0;
			foreach (var item in obj.Meta.UserVariables)
			{
				names[i] = item.Name;
				i++;
			}

			Array.Sort(names);
			return names;
		}

		private BrightAuthor DeserializeXmlFile(string xmlFile)
		{
			try
			{
				using XmlReader xmlReader = XmlReader.Create(xmlFile);

				XmlSerializer serializer = new XmlSerializer(typeof(BrightAuthor));
				var obj = (BrightAuthor)serializer.Deserialize(xmlReader);

				return obj;
			}
			catch (Exception e)
			{
				MessageBox.Show("��������� ������ ��� ������������.", "������", MessageBoxButtons.OK);
				return null;
			}
		}

		private void lbNames_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lbNames.SelectedItem != null)
			{
				lbVariable.Text = ((model.UserVariable)lbNames.SelectedItem).DefaultValue;
            }
        }
	}
}