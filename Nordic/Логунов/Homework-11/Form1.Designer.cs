﻿namespace Homework_11
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbNames = new System.Windows.Forms.ListBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelAuthorVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbVersionText = new System.Windows.Forms.Label();
            this.lbAutorVersionText = new System.Windows.Forms.Label();
            this.lbTypeText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbVariable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbNames
            // 
            this.lbNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNames.FormattingEnabled = true;
            this.lbNames.ItemHeight = 20;
            this.lbNames.Location = new System.Drawing.Point(15, 8);
            this.lbNames.Margin = new System.Windows.Forms.Padding(2);
            this.lbNames.Name = "lbNames";
            this.lbNames.Size = new System.Drawing.Size(171, 244);
            this.lbNames.Sorted = true;
            this.lbNames.TabIndex = 0;
            this.lbNames.SelectedIndexChanged += new System.EventHandler(this.lbNames_SelectedIndexChanged);
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(309, 8);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(60, 20);
            this.labelVersion.TabIndex = 1;
            this.labelVersion.Text = "Version:";
            // 
            // labelAuthorVersion
            // 
            this.labelAuthorVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAuthorVersion.AutoSize = true;
            this.labelAuthorVersion.Location = new System.Drawing.Point(260, 33);
            this.labelAuthorVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelAuthorVersion.Name = "labelAuthorVersion";
            this.labelAuthorVersion.Size = new System.Drawing.Size(108, 20);
            this.labelAuthorVersion.TabIndex = 2;
            this.labelAuthorVersion.Text = "Author version:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Type:";
            // 
            // lbVersionText
            // 
            this.lbVersionText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVersionText.AutoSize = true;
            this.lbVersionText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbVersionText.Location = new System.Drawing.Point(372, 8);
            this.lbVersionText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbVersionText.Name = "lbVersionText";
            this.lbVersionText.Size = new System.Drawing.Size(60, 20);
            this.lbVersionText.TabIndex = 4;
            this.lbVersionText.Text = "version";
            // 
            // lbAutorVersionText
            // 
            this.lbAutorVersionText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAutorVersionText.AutoSize = true;
            this.lbAutorVersionText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbAutorVersionText.Location = new System.Drawing.Point(372, 33);
            this.lbAutorVersionText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbAutorVersionText.Name = "lbAutorVersionText";
            this.lbAutorVersionText.Size = new System.Drawing.Size(111, 20);
            this.lbAutorVersionText.TabIndex = 5;
            this.lbAutorVersionText.Text = "author version";
            // 
            // lbTypeText
            // 
            this.lbTypeText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTypeText.AutoSize = true;
            this.lbTypeText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbTypeText.Location = new System.Drawing.Point(372, 59);
            this.lbTypeText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTypeText.Name = "lbTypeText";
            this.lbTypeText.Size = new System.Drawing.Size(40, 20);
            this.lbTypeText.TabIndex = 6;
            this.lbTypeText.Text = "type";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(211, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Текущая переменная";
            // 
            // lbVariable
            // 
            this.lbVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVariable.AutoSize = true;
            this.lbVariable.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbVariable.Location = new System.Drawing.Point(372, 156);
            this.lbVariable.Name = "lbVariable";
            this.lbVariable.Size = new System.Drawing.Size(51, 20);
            this.lbVariable.TabIndex = 8;
            this.lbVariable.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 281);
            this.Controls.Add(this.lbVariable);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTypeText);
            this.Controls.Add(this.lbAutorVersionText);
            this.Controls.Add(this.lbVersionText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelAuthorVersion);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.lbNames);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private ListBox lbNames;
		private Label labelVersion;
		private Label labelAuthorVersion;
		private Label label3;
		private Label lbVersionText;
		private Label lbAutorVersionText;
		private Label lbTypeText;
        private Label label1;
        private Label lbVariable;
    }
}