﻿namespace Homework_07
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.AlphaLabel = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.stepTrack = new System.Windows.Forms.TrackBar();
			this.button1 = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.stepLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.stepTrack)).BeginInit();
			this.SuspendLayout();
			// 
			// AlphaLabel
			// 
			this.AlphaLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.AlphaLabel.Location = new System.Drawing.Point(532, 469);
			this.AlphaLabel.Name = "AlphaLabel";
			this.AlphaLabel.Size = new System.Drawing.Size(427, 39);
			this.AlphaLabel.TabIndex = 0;
			this.AlphaLabel.TextChanged += new System.EventHandler(this.AlphaLabel_TextChanged);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(947, 362);
			this.panel1.TabIndex = 1;
			this.panel1.SizeChanged += new System.EventHandler(this.panel1_SizeChanged);
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(435, 476);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 32);
			this.label1.TabIndex = 2;
			this.label1.Text = "Alpha";
			// 
			// stepTrack
			// 
			this.stepTrack.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stepTrack.Location = new System.Drawing.Point(532, 378);
			this.stepTrack.Maximum = 100;
			this.stepTrack.Minimum = 1;
			this.stepTrack.Name = "stepTrack";
			this.stepTrack.Size = new System.Drawing.Size(427, 90);
			this.stepTrack.TabIndex = 3;
			this.stepTrack.Value = 1;
			this.stepTrack.ValueChanged += new System.EventHandler(this.stepTrack_ValueChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(468, 55);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(8, 8);
			this.button1.TabIndex = 4;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(435, 403);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(83, 32);
			this.label2.TabIndex = 5;
			this.label2.Text = "Step(t)";
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button2.Location = new System.Drawing.Point(12, 387);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(203, 126);
			this.button2.TabIndex = 6;
			this.button2.Text = "Нарисовать";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// stepLabel
			// 
			this.stepLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stepLabel.AutoSize = true;
			this.stepLabel.Location = new System.Drawing.Point(743, 434);
			this.stepLabel.Name = "stepLabel";
			this.stepLabel.Size = new System.Drawing.Size(27, 32);
			this.stepLabel.TabIndex = 7;
			this.stepLabel.Text = "0";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(971, 530);
			this.Controls.Add(this.stepLabel);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.stepTrack);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.AlphaLabel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.stepTrack)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private TextBox AlphaLabel;
		private Panel panel1;
		private Label label1;
		private TrackBar stepTrack;
		private Button button1;
		private Label label2;
		private Button button2;
		private Label stepLabel;
	}
}