namespace Homework_07
{
	public partial class Form1 : Form
	{
		private double _alpha;
		private int _deltaT;
		private Color _alphaLabelDefaultBackColor;

		public Form1()
		{
			InitializeComponent();
			_alphaLabelDefaultBackColor = AlphaLabel.BackColor;
			_deltaT = stepTrack.Value;
			stepLabel.Text = stepTrack.Value.ToString();
		}

		private void stepTrack_ValueChanged(object sender, EventArgs e)
		{
			stepLabel.Text = stepTrack.Value.ToString();
			_deltaT = stepTrack.Value;
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			if (_deltaT <= 0)
				return;

			Pen pen1 = new Pen(Color.Black, 3);
			Pen pen2 = new Pen(Color.Red, 3);

			// ������������ ���
			e.Graphics.DrawLine(pen1, 0, panel1.Height / 2, panel1.Width, panel1.Height / 2);
			e.Graphics.DrawLine(pen1, panel1.Width / 2, 0, panel1.Width / 2, panel1.Height);

			Draw(pen2, e, out float nextSX, out float nextSY);
		}

		/// <summary>
		/// ���������� ������������ ������ � ����� ����� <paramref name="t"/>
		/// </summary>
		/// <param name="pen">�����</param>
		/// <param name="e">��������� ����������� ��������� ����������</param>
		/// <param name="sx">��������� ���������� X �� ��������� �����</param>
		/// <param name="sy">��������� ���������� Y �� ��������� �����</param>
		/// <param name="t">�������� (���) ����� ������������ �����</param>
		private void Draw(Pen pen, PaintEventArgs e, out float sx, out float sy, double t = 0)
		{
			var x = (float)(2f * _alpha * Math.Cos(t) - _alpha * Math.Cos(2f * t));
			var y = (float)(2f * _alpha * Math.Sin(t) - _alpha * Math.Sin(2f * t));

			sx = x + panel1.Width / 2f;
			sy = panel1.Height / 2f - y;

			t += 2 * Math.PI / _deltaT;
			if (t >= 2 * Math.PI)
				return;

			Draw(pen, e, out float nextX, out float nextY, t);
			e.Graphics.DrawLine(pen, sx, sy, nextX, nextY);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			panel1.Invalidate();
		}

		private void AlphaLabel_TextChanged(object sender, EventArgs e)
		{
			if (!double.TryParse(AlphaLabel.Text, out double alpha))
			{
				AlphaLabel.BackColor = Color.Red;
				return;
			}

			AlphaLabel.BackColor = _alphaLabelDefaultBackColor;
			_alpha = alpha;
		}

		private void panel1_SizeChanged(object sender, EventArgs e)
		{
			panel1.Invalidate();
		}
	}
}