﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Homework_12
{
    internal class Program
    {
        private static Stopwatch Stopwatch { get; } = new Stopwatch();

        static void Main(string[] args)
        {
            var variant = GetInputNumber("Введите номер варианта > ");
            
            switch(variant)
            {
                case 1:
                    RunFirstVariant();
                    break;
                case 2:
                    RunSecondVariant();
                    break;
                case 3:
                    RunFactorialVariant();
                    break;
                default:
                    Console.WriteLine("Неверный номер варианта.");
                    break;
            }
        }

        private static void RunFactorialVariant()
        {
            int n = GetInputNumber("Введите число для вычисления факториала > ");
            Factorial factorial = new Factorial();

            Console.WriteLine($"Факториал от числа {n} = {factorial[n]}");
        }

        private static int GetInputNumber(string text)
        {
            while (true)
            {
                Console.Write(text);
                string inputStr = Console.ReadLine();

                if (int.TryParse(inputStr, out int value))
                    return value;
            }
        }

        private static void RunFirstVariant()
        {
            Stopwatch.Reset();

            int n = GetInputNumber("Введите N > ");

            Stopwatch.Start();
            int recursiveSum = GetRecursiveSum(n);
            Stopwatch.Stop();
            double recursiveTime = Stopwatch.Elapsed.TotalMilliseconds;

            Stopwatch.Reset();
            Stopwatch.Start();
            int sum = GetSum(n);
            Stopwatch.Stop();
            double sumTime = Stopwatch.Elapsed.TotalMilliseconds;


            Console.WriteLine($"N={n}\tRecursiveSum={recursiveSum}\tSum={sum}\tRecursiveSumTime={recursiveTime}\tNormalTime={sumTime}");
        }

        private static int GetRecursiveSum(int n)
        {
            if (n == 0)
                return 0;

            return n + GetRecursiveSum(n - 1);
        }

        private static int GetSum(int n)
        {
            return n * (n + 1) / 2;
        }

        private static void RunSecondVariant()
        {
            int a = GetInputNumber("Введите A > ");
            int b = GetInputNumber("Введите B > ");

            int mul = GetRecursiveMul(a, b);

            Console.WriteLine($"Результат умножения {a} на {b} = {mul}");
        }

        private static int GetRecursiveMul(int a, int b)
        {
            if (b == 1)
                return a;

            return a + GetRecursiveMul(a, b - 1);
        }
    }

    class Factorial
    {
        private Dictionary<int, int> _factorials;

        public Factorial()
        {
            _factorials = new Dictionary<int, int>();
        }

        public int this[int num]
        {
            get
            {
                if (!_factorials.ContainsKey(num))
                    _factorials.Add(num, CalculateFactorial(num));

                return _factorials[num];
            }
        }

        private int CalculateFactorial(int num)
        {
            if (num == 0) return 1;

            return num * CalculateFactorial(num - 1);
        }
    }
}