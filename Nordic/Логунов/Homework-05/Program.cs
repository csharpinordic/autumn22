﻿using System.Security.Cryptography.X509Certificates;

namespace Homework_05
{
    internal class Program
    {
        static void SetPixel(int x, int y)
        {
            int sx = x + Console.WindowWidth / 2;
            int sy = y + Console.WindowHeight / 2;
            if (sx >= 0 && sy >= 0 && sx < Console.BufferWidth && sy < Console.BufferHeight)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.SetCursorPosition(sx, sy);
                Console.Write("*");
            }
        }

        /// <summary>
        /// Задание вычисление точки, в которой прямая с указанными
        /// коэффициентами пересекает оси координат
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.ResetColor();
            Console.Write("Введите коэффициент A > ");
            double a = double.Parse(Console.ReadLine());

            Console.Write("Введите коэффициент B > ");
            double b = double.Parse(Console.ReadLine());

            Console.Write("Введите коэффициент C > ");
            double c = double.Parse(Console.ReadLine());

            // Look => https://zaochnik.com/spravochnik/matematika/prjamaja-ploskost/obschee-uravnenie-prjamoj/
            if (a == 0)
            {
                if (b != 0)
                {
                    Console.WriteLine($"Пересечение по оси Oy = {-c / b}");
                }
                else
                {
                    // A == 0 && B == 0
                    Console.WriteLine("Одновременно A и B не могут быть равны 0.");
                }
            }
            else
            {
                if (b == 0)
                {
                    Console.WriteLine($"Пересечение по оси Ox = {-c / a}");
                }
                else
                {
                    // A != 0 && B != 0 && C != 0 - полное уравнение
                    Console.WriteLine($"Пересечение по оси Ox = {-c / a}");
                    Console.WriteLine($"Пересечение по оси Oy = {-c / b}");
                }
            }

            Console.WriteLine("Для построения графика нажмите Enter");
            Console.ReadLine();
            Console.Clear();

            // Отрисовка графика функции
            if (b == 0 || a == 0)
            {
                Console.WriteLine("Вертикальная и горизонтальная линия не реализованы");
                return;
            }

            // y = kx + m
            double k = -a / b;
            double m = -c / b;

            // x = py + q
            double p = -b / a;
            double q = -c / a;

            if (Math.Abs(k) > 1)
            {
                // Двигаемся по оси Oy
                double miny = -Math.Round(Console.WindowHeight / 2.0d);
                double maxy = +Math.Round(Console.WindowHeight / 2.0d);
                for (int y = (int)miny; y <= maxy; y++)
                {
                    int x = (int)Math.Round(p * y + q);
                    SetPixel(x, y);
                }
            }
            else
            {
                // Двигаемся по оси Ox
                double minx = -Math.Round(Console.WindowWidth / 2.0d);
                double maxx = +Math.Round(Console.WindowWidth / 2.0d);
                for (int x = (int)minx; x <= maxx; x++)
                {
                    int y = (int)Math.Round(k * x + m);
                    SetPixel(x, y);
                }
            }
        }
    }
}