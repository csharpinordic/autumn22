﻿namespace LogunovIgorHW03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов > ");
            int n = int.Parse(Console.ReadLine());
            Random rnd = new Random();

            double[] array = new double[n];
            double minValue = double.PositiveInfinity;
            double maxValue = double.NegativeInfinity;
            double sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(-100, 100);
            }

            for (int i = 0; i < array.Length; i++)
            {
                // minValue = Math.Min(minValue, array[i]);
                bool condition = array[i] < minValue;
                if (condition)
                {
                    minValue = array[i];
                }
                // maxValue = Math.Max(maxValue, array[i]);
                if (array[i] > maxValue)
                {
                    maxValue = array[i];
                }
                // sum = sum + array[i];
                sum += array[i];
            }

            double average = sum / array.Length;

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("[{0}] = {1}", i, array[i]);
            }

            Console.WriteLine("Минимум: " + minValue);
            Console.WriteLine("Максимум: " + maxValue);
            Console.WriteLine("Сумма: " + sum);
            Console.WriteLine("Среднее арифметическое: " + average);
            Console.ReadLine();
        }
    }
}