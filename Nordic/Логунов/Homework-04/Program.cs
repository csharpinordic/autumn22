﻿namespace Homework_04
{
	internal class Program
	{
		/// <summary>
		/// Точка входа в программу
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			Console.Write("Введите номер домашнего задания > ");
			string currentExercise = Console.ReadLine();

			switch (currentExercise.Trim())
			{
				case "1":
					HomeworkFourWithFirstAddExercise();
					break;
				case "2":
					HomeworkFourAddExerciseTwo(); 
					break;
				default:
					Console.WriteLine("Неверный формат с номером задания. " +
						"Необходимо ввести номер задания в целочисленом формате в диапазоне от 1 до 2.");
					break;
			}
		}

		/// <summary>
		/// Метод выполняющий дополнительное домашнее задание №2 по уроку №4
		/// Работа с методом Split
		/// </summary>
		static void HomeworkFourAddExerciseTwo()
		{
			Console.Write("Введите строку > ");
			string str = Console.ReadLine();

			Console.Write("Введите символ-разделитель > ");
			char splitter = (char)Console.Read();

			int max = 0;

			var array = str.Split(splitter);

			for (int element = 0; element < array.Length; element++) 
			{ 
				max = Math.Max(max, array[element].Length);
			}

			Console.WriteLine($"Максимальная длина слова: {max} символов.");
		}

		/// <summary>
		/// Метод выполняющий домашнее задание №4 (с дополнительным заданием №1)
		/// Подсчет количество символов в строке с учетом регистра и без.
		/// </summary>
		static void HomeworkFourWithFirstAddExercise()
		{
			Console.Write("Введите строку > ");
			string str = Console.ReadLine();

			Console.Write("Введите символ > ");
			char symbol = (char)Console.Read();

			var regCount = 0;
			var withoutRegCount = 0;

			var currentColor = Console.ForegroundColor;
			for (int i = 0; i < str.Length; i++)
			{
				if (str[i] == symbol)
				{
					regCount++;
				}
				else if (char.ToUpper(str[i]) == char.ToUpper(symbol))
				{
					withoutRegCount++;
				}
			}

			Console.Write($"С учетом регистра символов {regCount} : ");
			for (int i = 0; i < str.Length; i++)
			{
				if (str[i] == symbol)
				{
					Console.ForegroundColor = ConsoleColor.Green;
				}
				Console.Write(str[i]);
				Console.ForegroundColor = currentColor;
			}

			Console.Write($"\nБез учета регистра символов: {withoutRegCount + regCount} ");

			for (int i = 0; i < str.Length; i++)
			{
				if (char.ToUpper(str[i]) == char.ToUpper(symbol))
				{
					Console.ForegroundColor = ConsoleColor.Red;
				}
				Console.Write(str[i]);
				Console.ForegroundColor = currentColor;
			}
		}
	}
}