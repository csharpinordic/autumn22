-- ���������� ������� ������
SELECT Count(DISTINCT [Post]) AS [PostCount]
FROM [dbo].[Buttons]

-- PostCount
-----------
-- 14

-- ��������� ������
SELECT DISTINCT [Post] AS [Post]
FROM [dbo].[Buttons]
ORDER BY [Post] ASC

--Post
-------------
--0
--1
--2
--3
--4
--5
--6
--7
--8
--9
--10
--11
--12
--13

-- ���������� ������ ������
SELECT Count(DISTINCT [Number]) AS [NumberCount]
FROM [dbo].[Buttons]

--NumberCount
-------------
--20

-- ��������� ������ ������
SELECT DISTINCT [Number] AS [Number]
FROM [dbo].[Buttons]
ORDER BY [Number] ASC

--Number
-------------
--0
--1
--2
--3
--4
--5
--6
--7
--8
--9
--10
--11
--12
--13
--14
--15
--16
--17
--18
--19

-- ���������� ������
SELECT Count(DISTINCT [Side]) AS [SideCount]
FROM [dbo].[Buttons]

--SideCount
-------------
--2

-- ��������� ������
SELECT DISTINCT ([Side]) AS [Side]
FROM [dbo].[Buttons]
ORDER BY [Side]

--Side
-------------
--0
--1

/***************************************
 * Accidents
 ***************************************/

-- ���� � �����, ����� ����� ������ ������� ������������
SELECT TOP(1)
		[Post] AS [Post], 
		[StartTime] AS [StartTime]
FROM [dbo].[Accidents]
ORDER BY [StartTime] ASC

-- [Code Review]
-- ��� ��� ������ ������������, ��� ��� �������� ������ ������� ������ � ��������� �� ������������
-- (����� ����, ���������������� �������� ������ ������ ������ ��� ������ �����������)
-- ������������� ��������� ����� WITH TIES � ������ ���������� ������ ���������


--Post       StartTime
------------ -----------------------
--9          2019-06-20 18:05:02.197

-- ���� � �����, ����� ����� ������� ������������ 2021 ����
SELECT TOP(1)
		[Post] AS [Post], 
		[StartTime] as [StartTime]
FROM [dbo].[Accidents]
WHERE Year([StartTime]) = 2021
ORDER BY [StartTime] ASC

-- [Code Review]
-- �� �� ����������� �� ������ WITH TIES

--Post       StartTime
------------ -----------------------
--10 �       2021-01-06 08:25:09.947

-- ������������ ������������ ������������
SELECT MAX(DATEDIFF(SECOND, [StartTime], [EndTime])) AS [MaxAccindentSeconds]
FROM [dbo].[Accidents]
WHERE [EndTime] IS NOT NULL

--MaxAccindentSeconds
---------------------
--765051

-- [Code Review]
-- � �� ������ ������� ������������ � EndTime IS NULL -- ��� ��� ���� �������, 
-- ��� �������� � ������� ������ ������������, ������� ������� �����������

-- ���������� ������������, ��� ������� �� ��������� ��� ������������
SELECT Count(*) AS [NotTypedAccidentCount]
FROM [dbo].[Accidents]
WHERE [AType_ID] IS NULL

--NotTypedAccidentCount
-----------------------
--15368

/***************************************
 * Plans
 ***************************************/

-- ���� ������� �������������������� ��������� �� 2020 ���
SELECT SUM([PlanCount]) AS [PlanCount],
	   SUM([FactCount]) AS [FactCount]
FROM [dbo].[Plans]
WHERE [PType] = 1 -- ������� ����
	AND [ProductionClassID] = '3098C45D-79A5-4A71-987C-91E120EDFFEB' -- �������������������� ���������
	AND YEAR([TimeStamp]) = 2020

-- [Code Review]
-- � ������� �������: �� ��������� ���������� ������ 
-- ������ ������������ ��������� �����
-- �� ������������� �������

--PlanCount   FactCount
----------- -----------
--3660        3117

/***************************************
 * ButtonEvents
 ***************************************/

 -- ���������� ������� ������� ������ �� ����� ����� 7
 SELECT Count([Counter1]) AS [GreenButtonClicks]
 FROM [dbo].[ButtonEvents]
 WHERE [Number] = 7

 -- [Code Review]
 -- ������� ��������, ��� ����� �������� COUNT(*), COUNT(1) � � ������
 -- ������ ������ COUNT(��-���-������)
 -- ��������� ����� ����������

-- GreenButtonClicks
-------------------
--15354

 -- ���������� ������� ������� ������ �� ����� ����� 13
 SELECT Count([Counter3]) AS [RedButtonClicks]
 FROM [dbo].[ButtonEvents]
 WHERE [Number] = 13

-- RedButtonClicks
-----------------
--13498

 -- ���� ������� ������������
 -- Accidents (Post - 9, Date: 2019-06-20 18:05:02.197)
 -- ButtonsEvents (Date: 2019-06-28 13:48:33.193)
 SELECT TOP (1) [TimeStamp] AS [AccidentDate]
 FROM [dbo].[ButtonEvents]
 ORDER BY [TimeStamp] ASC

 -- [Code Review]
 -- ����� ������ ������ SELECT TOP 1 ��� ������

-- AccidentDate
-------------------------
--2019-06-28 13:48:33.193

 -- ������� ��������� ������ �� ����� ����� 10
 SELECT TOP(1) WITH TIES
	[BType] AS [CurrentState]
 FROM [dbo].[ButtonEvents]
 WHERE [Number] = 10
 ORDER BY [TimeStamp] DESC

-- CurrentState
--------------
--2

 /***************************************
 * ButtonSnapshots
 ***************************************/
 
 -- ������� ��������� ������ �� ����� ����� 10
 -- � ButtonEvents - 2
 ---------------------------------------
 -- ����� �������
 SELECT [BType] AS [CurrentState]
 FROM [dbo].[ButtonSnapshots]
 WHERE [Post_ID] = '347D1666-EDE5-4FCE-95CD-EF4447B3C3AB' -- 6-� ���� (10 ����)
	AND [Side] = 0 -- �����

--CurrentState
--------------
--2

-- ������ �������
 SELECT [BType] AS [CurrentState]
 FROM [dbo].[ButtonSnapshots]
 WHERE [Post_ID] = '347D1666-EDE5-4FCE-95CD-EF4447B3C3AB' -- 6-� ���� (10 ����)
	AND [Side] = 1 -- ������

--CurrentState
--------------
--2
----------------------------------------

/***************************************
 * Users
 ***************************************/

 -- ��� ������������ � ������������� �������
 SELECT TOP (1) WITH TIES
	[Name] AS [Name]
 FROM [dbo].[Users]
 ORDER BY [AccessList] DESC

 -- [Code Review]
 -- ��� ���� ����� - ��� ����� "������������ �����"
 -- ���� (��������) ������� ������������� ������� ������������ ���������� ��������� ����,
 -- �� ��� ������������� ������� ����� ����������, ��� ����� 0111 ������,
 -- ��� ����� 1000, �� ����� 1000 > 0111.
 -- ������, ��� � ���� ������ WITH TIES, �������, ������������ �� ���� :-)
 
-- Name
------------
--User_8F19
--User_1891
--User_FC31
--User_81CB

 -- ����� ������������ User_B5AC
 SELECT [Role] AS [RoleNumber], -- �������� �������
 [Code] AS [Code] -- ��� ������
 FROM [dbo].[UserAccesses]
 WHERE [User_ID] = 'DE7C73E8-6AB3-4B63-A3E3-F54FBBFB5AC8' -- User_B5AC
	AND [Allow] = 1 -- ����������

-- [Code Review]
-- 

--RoleNumber  Code
------------- -------------------------------
--10          Accidents
--10          Cycles
--10          WorkTime
