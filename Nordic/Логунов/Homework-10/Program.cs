﻿using Newtonsoft.Json;

namespace Homework_10
{
	public class Program
	{
		/// <summary>
		/// Входящий файл для десериализации
		/// </summary>
		private const string InputFileName = "homework-10.json";

		/// <summary>
		/// Исходящий файл для сериализации
		/// </summary>
		private const string OutputFileName = "homework-10-serialized.json";

		/// <summary>
		/// Основной сценарий программы:
		/// получает и десериализирует массив объектов <seealso cref="HomeworkObject"/> из файла <see cref="InputFileName"/>,
		/// выводит на экран информацию о минимальном и максимальном значении <seealso cref="HomeworkObject.Value"/>,
		///	записывает новый массив объектов с положительным значением в файл <see cref="OutputFileName"/>
		/// </summary>
		public static void Main()
		{
			var array = DeserializeHomeworkObjects(InputFileName);

			if (array == null)
				return;

			Console.WriteLine($"Полученный массив: {array.Length} элементов.");

			if (!TryGetMinMaxValue(array, out int min, out int max))
				return;

			Console.WriteLine($"Максимальное значение: {max}");
			Console.WriteLine($"Минимальное значение: {min}");

			var positiveObjects = GetObjectsWithPositiveValue(array);
			SerializeHomeworkObjects(positiveObjects, OutputFileName);

			Console.ReadLine();
		}

		/// <summary>
		/// Поиск элементов массива с положительным значением <seealso cref="HomeworkObject.Value"/>
		/// </summary>
		/// <param name="array">Входящий массив</param>
		/// <returns>Лист, содержащий элементы с положительным значением <see cref="HomeworkObject.Value"/></returns>
		private static List<HomeworkObject> GetObjectsWithPositiveValue(HomeworkObject[] array)
		{
			var positiveValuesList = new List<HomeworkObject>();
			foreach (var obj in array)
			{
				if (obj.Value > 0)
					positiveValuesList.Add(obj);
			}

			return positiveValuesList;
		}

		/// <summary>
		/// Поиск минимального и максимального значения <see cref="HomeworkObject.Value"/>
		/// среди элементов <paramref name="array"/>
		/// </summary>
		/// <param name="array">Массив элементов</param>
		/// <param name="min">Минимальное значение</param>
		/// <param name="max">Максимальное значение</param>
		/// <returns>true - элементы найдены, false - входящий массив пуст или содержит 0 элементов</returns>
		private static bool TryGetMinMaxValue(HomeworkObject[] array, out int min, out int max)
		{
			min = int.MaxValue;
			max = int.MinValue;

			if (array == null || array.Length == 0) 
				return false;

			foreach (var obj in array)
			{
				max = Math.Max(max, obj.Value);
				min = Math.Min(min, obj.Value);
			}

			return true;
		}

		/// <summary>
		/// Сериализация списка <paramref name="items"/> в json файл <paramref name="filePath"/>
		/// </summary>
		/// <param name="items">Список элементов для сериализации</param>
		/// <param name="filePath">Путь к файлу для записи</param>
		private static void SerializeHomeworkObjects(List<HomeworkObject> items, string filePath)
		{
			try
			{
				FileInfo fi = new FileInfo(filePath);

				if (fi.Exists)
				{
					var movedFile = Path.ChangeExtension(filePath, ".backup");
					File.Move(filePath, movedFile, true);
				}

				JsonSerializerSettings settings = new JsonSerializerSettings()
				{
				   NullValueHandling = NullValueHandling.Ignore,
				   Formatting = Formatting.Indented,	
				};
				var jsonString = JsonConvert.SerializeObject(items, settings);

				File.WriteAllText(filePath, jsonString);
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Ошибка сериализации: {ex.Message}");
			}
		}

		/// <summary>
		/// Десериализация файла в массив элементов <see cref="HomeworkObject"/>
		/// </summary>
		/// <param name="filePath">Входящий файл для десериализации</param>
		/// <returns>Массив объектов <see cref="HomeworkObject"/></returns>
		private static HomeworkObject[]? DeserializeHomeworkObjects(string filePath)
		{
			try
			{
				FileInfo fi = new FileInfo(filePath);

				if (!fi.Exists)
				{
					Console.WriteLine("Файл не найден!");
					return null;
				}

				var array = JsonConvert.DeserializeObject<HomeworkObject[]>(File.ReadAllText(fi.FullName));

				return array;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Ошибка десериализации: {ex.Message}");
				return null;
			}
		}
	}
}
