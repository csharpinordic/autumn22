﻿namespace Homework_10
{
	public class HomeworkObject
	{
		public string ValueUuid;
		public long TimestampStart;
		public long TimestampEnd;
		public int Value;
		public Guid ParameterId;
	}
}
