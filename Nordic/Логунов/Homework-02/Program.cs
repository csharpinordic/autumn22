﻿namespace StudentLogunovIgor
{
	/// <summary>
	/// Класс программы, содержащий точку входа
	/// </summary>
	internal class Program
	{
		/// <summary>
		/// Погрешность сравнения вещественных чисел
		/// </summary>
        const double precision = 1E-6; // 10 в минус шестой степени

        /// <summary>
        /// Точка входа в программу
        /// </summary>
        /// <param name="args">Список аргументов, который был передан программе с параметрами запуска</param>
        static void Main(string[] args)
		{
			Console.Write("Введите номер домашнего задания (1 - 3) > ");

			switch (Console.ReadLine().Trim())
			{
				case "1":
					FirstHomeWork();
					break;
				case "2":
					SecondHomeWork();
					break;
				case "3":
					ThirdHomeWork();
					break;
				default:
					Console.WriteLine("Введен неверный формат номера задания.");
					Console.WriteLine("Требуется целочисленное значение диапазоном от 1 до 3.");
					break;
			}
			Console.ReadLine();
		}

		/// <summary>
		/// Дополнительное домашнее задание №1.
		/// Решение квадратного уравнения.
		/// </summary>
		static void FirstHomeWork()
		{
			double a, b, c;

			Console.Write("Введите A > ");
			if (!IsNumberFormat(Console.ReadLine(), out a))
				return;

			Console.Write("Введите B > ");
			if (!IsNumberFormat(Console.ReadLine(), out b))
				return;

			Console.Write("Введите C > ");
			if (!IsNumberFormat(Console.ReadLine(), out c))
				return;

			double discriminant = (b * b) - (4 * a * c);

			if (discriminant < 0)
			{
				Console.WriteLine("Дискриминант < 0. Корней нет.");
				return;
			}

			double x1;

			// Сравнение с нулём с заданной точностью
			if (Math.Abs(discriminant - 0) < precision)
			{
				x1 = -b / (2 * a);
				Console.WriteLine("Дискриминант = 1, есть один корень: {0} ", x1);
				return;
			}

			x1 = (-b - Math.Sqrt(discriminant)) / (2 * a);
			double x2 = (-b + Math.Sqrt(discriminant)) / (2 * a);

			Console.WriteLine("X1 = {0}", x1);
			Console.WriteLine("X2 = {0}", x2);
		}

		/// <summary>
		/// Дополнительное домашнее задание №2
		/// Вычисление площади правильного многоугольника с заданным числом сторон.
		/// </summary>
		static void SecondHomeWork()
		{
			int n;
			double a;

			Console.Write("Введите сторону (а) многоугольника > ");
			if (!IsNumberFormat(Console.ReadLine(), out a))
				return;

			Console.Write("Введите количество сторон > ");
			if (!IsNumberFormat(Console.ReadLine(), out n))
				return;

			// Площадь
			double s = (a * a * n) / (4 * Math.Tan(Math.PI / n));
			Console.WriteLine("Площадь правильного многоугольника = {0}", s);
		}

		/// <summary>
		/// Парадокс дней рождения
		/// Вычисление вероятности совпадения дней рождения в группе из	N человек
		/// </summary>
		static void ThirdHomeWork()
		{
			int n;
			Console.Write("Введите количество человек в группе > ");
			if (!IsNumberFormat(Console.ReadLine(), out n))
				return;

			double p = 1 - Math.Pow(Math.E, -(n * n - n * 1) / (2.0 * 365));
			Console.WriteLine("Вероятность {0:P1}", p);
		}

		/// <summary>
		/// Метод проверяет является ли входная строка целочисленной.
		/// При несоответствии формата выводит сообщение в консоль.
		/// </summary>
		/// <param name="inputString">Входная строка</param>
		/// <param name="value">Преобразованное целочисленное значение</param>
		/// <returns></returns>
		static bool IsNumberFormat(string inputString, out int value)
		{
			if (int.TryParse(inputString, out value))
				return true;

			Console.WriteLine("Неверный формат: {0} ", inputString);
			Console.WriteLine("Требуется целочисленное значение.");
			return false;
		}

        /// <summary>
        /// Метод проверяет является ли входная строка числом с плавающей точкой.
        /// При несоответствии формата выводит сообщение в консоль.
        /// </summary>
        /// <param name="inputString">Входная строка</param>
        /// <param name="value">Преобразованное вещественное значение</param>
        /// <returns></returns>
        static bool IsNumberFormat(string inputString, out double value)
        {
            if (double.TryParse(inputString, out value))
                return true;

            Console.WriteLine("Неверный формат: {0} ", inputString);
            Console.WriteLine("Требуется число с плавающей точкой.");
            return false;
        }
    }
}