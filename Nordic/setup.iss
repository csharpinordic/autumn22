; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{2B4F1DAC-ABE8-4C5A-B987-7FECEFED1B07}
AppName=������� ���� Nordic IT School
AppVersion=1.0.0.0
;AppVerName=������� ���� Nordic IT School 0.1
AppPublisher=Nordic IT School
AppPublisherURL=https://inordic.ru/
AppSupportURL=https://inordic.ru/
AppUpdatesURL=https://inordic.ru/
DefaultDirName={autopf}\Nordic
DisableProgramGroupPage=yes
LicenseFile=..\LICENSE
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
OutputDir=setup
OutputBaseFilename=SetupNordic
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Types]
Name: "full"; Description: "������ ���������"
Name: "custom"; Description: "����� ����������� �����������"; Flags: iscustom

[Components]
Name: "api"; Description: "REST-������� API"; Types: full custom
Name: "client"; Description: "���������� ����������"; Types: full custom

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "installsvc"; Description: "��������� �����"; Components: api
Name: "runsvc"; Description: "������ �����"; Components: api

[Files]
Source: "GalaxyApi\bin\Debug\net6.0\GalaxyApi.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: api
Source: "GalaxyApi\bin\Debug\net6.0\*.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs; Components: api
Source: "GalaxyApi\bin\Debug\net6.0\*.json"; DestDir: "{app}"; Flags: ignoreversion; Components: api
Source: "GalaxyApi\bin\Debug\net6.0\*.pdb"; DestDir: "{app}"; Flags: ignoreversion; Components: api
Source: "GalaxyApi\bin\Debug\net6.0\Nlog.config"; DestDir: "{app}"; Flags: ignoreversion; Components: api
Source: "GalaxyApi\bin\Debug\net6.0\XmlDocumentation.xml"; DestDir: "{app}"; Flags: ignoreversion; Components: api
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\Nordic\������� ���� Nordic IT School"; Filename: "{app}\GalaxyApi.exe"
Name: "{autodesktop}\������� ���� Nordic IT School"; Filename: "{app}\GalaxyApi.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\GalaxyApi.exe"; Flags: nowait postinstall skipifsilent; Description: "{cm:LaunchProgram,������� ���� Nordic IT School}"
Filename: "{sys}\sc.exe"; Parameters: "create GalaxyAPI start=auto binPath= ""{app}\GalaxyApi.exe"""; Flags: waituntilterminated; Description: "��������� ������"; StatusMsg: "��������� ������"; Components: api; Tasks: installsvc
Filename: "net.exe"; Parameters: "start GalaxyApi"; WorkingDir: "{sys}"; Flags: waituntilterminated; Description: "������ ������"; Components: api; Tasks: runsvc

[UninstallDelete]
Type: filesandordirs; Name: "{app}"
Type: dirifempty; Name: "{app}"
