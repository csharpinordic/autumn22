﻿using System;
using System.Net.WebSockets;

namespace WebSocketClient
{
    internal class Program
    {
        private const string ServerAddress = "ws://localhost:5050";

        private static void Receiver(ClientWebSocket client)
        {
            try
            {
                while (client.State == WebSocketState.Open)
                {
                    byte[] buffer = new byte[1024];
                    WebSocketReceiveResult received = client.ReceiveAsync(buffer, CancellationToken.None).Result;
                    if (client.State == WebSocketState.CloseReceived)
                    {
                        Console.WriteLine(received.CloseStatusDescription);
                        // мы тут должны как-то завершить основной цикл опроса
                    }
                    string text = System.Text.Encoding.UTF8.GetString(buffer, 0, received.Count);
                    if (!string.IsNullOrEmpty(text))
                    {
                        Console.Write("\r");
                        Console.WriteLine(text);
                        Console.Write(">");
                    }
                }
                Console.WriteLine("Соединение закрыто. Нажмите Enter для выхода из программы");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"*** {ex.Message}");
            }
        }

        /// <summary>
        /// Гарантированное установление соединения
        /// </summary>
        /// <returns></returns>
        static ClientWebSocket Connect()
        {
            while (true)
            {
                try
                {
                    Uri uri = new Uri(ServerAddress);
                    ClientWebSocket client = new ClientWebSocket();
                    client.ConnectAsync(uri, CancellationToken.None).Wait();
                    return client;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Task.Delay(TimeSpan.FromSeconds(2)).Wait();
                }
            }
        }

        static void Send(ref ClientWebSocket client, string message)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(message);

            while (true)
            {
                try
                {
                    client.SendAsync(data, WebSocketMessageType.Text, true, CancellationToken.None).Wait();
                    return;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    client = Connect();
                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                var client = Connect();

                // Запуск ожидания текста в отдельном потоке
                Task task = Task.Factory.StartNew(() => Receiver(client));

                while (client.State == WebSocketState.Open)
                {
                    if (task.IsCompleted)
                    {
                        Console.WriteLine("reconnect");
                        task = Task.Factory.StartNew(() => Receiver(client));
                    }

                    Console.Write(">");
                    string message = Console.ReadLine();
                    if (message.ToLower() == "quit") break;

                    Send(ref client, message);
                }
                client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Bye bye", CancellationToken.None);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}