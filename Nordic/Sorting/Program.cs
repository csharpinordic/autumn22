﻿using System.Diagnostics;

namespace Sorting
{
    internal class Program
    {
        static void Print(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();

            Console.Write("Введите длину массива: ");
            int n = int.Parse(Console.ReadLine());

            int[] array1 = new int[n];
            int[] array2 = new int[n];            

            var random = new Random();
            for (int i = 0; i < n; i++)
            {
                array1[i] = random.Next(0, 1000);
                array2[i] = array1[i];
            }

            // Print(array);

            watch.Start();
            // Сортировка массива методом модифицированного пузырька
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    // Проверяем, по порядку ли элементы
                    if (array1[i] > array1[j])
                    {
                        // Меняем их местами
                        var swap = array1[i];
                        array1[i] = array1[j];
                        array1[j] = swap;  
                    }
                }
            }
            watch.Stop();
            var ms1 = watch.ElapsedMilliseconds;

            // Print(array);

            // Стандартная сортировка массива 
            watch.Restart();
            Array.Sort(array2);
            watch.Stop();
            var ms2 = watch.ElapsedMilliseconds;          

            // Затраченное время в мс
            Console.WriteLine(ms1);
            Console.WriteLine(ms2);

            Console.ReadLine();
        }
    }
}