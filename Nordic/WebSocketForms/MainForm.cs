using System.Windows.Forms;

namespace WebSocketForms
{
    public partial class MainForm : Form
    {
        private const string ServerAddress = "ws://localhost:5050";

        private Client client;

        public MainForm()
        {
            InitializeComponent();
            client = new(ServerAddress);
            client.ErrorOccured += LogError;
            client.TextReceived += LogMessage;
            client.SessionIsClosed += LogMessage;
        }

        /// <summary>
        /// ���������������� ���������
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            list.Items.Add(message);
        }

        /// <summary>
        /// ���������������� ������
        /// </summary>
        /// <param name="message"></param>
        private void LogError(string message)
        {
            list.Items.Add($"*ERROR* {message}");
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            // ����������� ������������ ����������
            bgConnect.RunWorkerAsync();          
        }

        private void bgConnect_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            client.Connect();
        }

        private void text_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                client.Send(text.Text);
                text.Text = string.Empty;
            }
        }

        private void bgConnect_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            LogMessage("���������� �����������");
        }
    }
}