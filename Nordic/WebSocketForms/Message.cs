﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSocketForms
{
    /// <summary>
    /// Сообщение
    /// </summary>
    internal class Message
    {
        internal MessageType Type;
        internal string Text;

        internal Message(MessageType type, string text)
        {
            Text = text;
            Type = type;
        }
    }
}
