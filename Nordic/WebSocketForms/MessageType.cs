﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSocketForms
{
    /// <summary>
    /// Тип сообщения
    /// </summary>
    internal enum MessageType
    {
        None,
        Message,
        Closure,
        Error
    }
}
