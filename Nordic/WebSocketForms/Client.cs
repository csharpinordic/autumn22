﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.WebSockets;

namespace WebSocketForms
{
    public class Client
    {
        /// <summary>
        /// Адрес сервера
        /// </summary>
        private string ServerAddress;

        /// <summary>
        /// Клиент Web Socket
        /// </summary>
        private ClientWebSocket client;

        /// <summary>
        /// Задача по приёму даннных
        /// </summary>
        private Task receiver;

        private System.Windows.Forms.Timer timer;

        /// <summary>
        /// Очередь сообщений
        /// </summary>
        private System.Collections.Concurrent.ConcurrentQueue<Message> queue = new();

        /// <summary>
        /// Метод (делегат), который принимает одно строковое значение
        /// </summary>
        /// <param name="message"></param>
        public delegate void ProcessMessage(string message);

        /// <summary>
        /// Событие - ошибка при обмене данными с сервером
        /// </summary>
        public event ProcessMessage ErrorOccured;

        private event ProcessMessage? textReceived;
        /// <summary>
        /// Событие - получен текст от сервера
        /// </summary>
        public event ProcessMessage TextReceived
        { 
            add
            {
                textReceived += value;
            }
            remove
            {
                textReceived -= value;
            }
        }

        /// <summary>
        /// Событие - закрытие соединения
        /// </summary>
        public event ProcessMessage SessionIsClosed;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="serverAddress">Адрес сервера Web Socket</param>
        public Client(string serverAddress)
        {
            this.ServerAddress = serverAddress;
            timer = new()
            {
                Interval = 100
            };
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        /// <summary>
        /// Обработчик таймера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object? sender, EventArgs e)
        {
            if (queue.TryDequeue(out var message))
            {
                switch (message.Type)
                {
                    case MessageType.Message:
                        textReceived?.Invoke(message.Text);
                        break;
                    case MessageType.Closure:
                        SessionIsClosed?.Invoke(message.Text);
                        break;
                    case MessageType.Error:
                        ErrorOccured?.Invoke(message.Text);
                        break;
                    default:
                        ErrorOccured?.Invoke("Внутренняя ошибка");
                        break;
                }
            }
        }

        /// <summary>
        /// Обработка входящих сообщений
        /// </summary>       
        private void Receiver()
        {
            try
            {
                while (client.State == WebSocketState.Open)
                {
                    byte[] buffer = new byte[1024];
                    WebSocketReceiveResult received = client.ReceiveAsync(buffer, CancellationToken.None).Result;
                    if (client.State == WebSocketState.CloseReceived)
                    {
                        queue.Enqueue(new Message(MessageType.Closure, received.CloseStatusDescription));
                        // мы тут должны как-то завершить основной цикл опроса
                    }
                    string text = System.Text.Encoding.UTF8.GetString(buffer, 0, received.Count);
                    if (!string.IsNullOrEmpty(text))
                    {
                        queue.Enqueue(new Message(MessageType.Message,text));
                    }
                }
            }
            catch (Exception ex)
            {
                queue.Enqueue(new Message(MessageType.Error,ex.Message));
            }
        }

        /// <summary>
        /// Гарантированное установление соединения
        /// </summary>
        /// <returns></returns>
        public void Connect()
        {
            while (true)
            {
                try
                {
                    Uri uri = new Uri(ServerAddress);
                    client = new ClientWebSocket();
                    client.ConnectAsync(uri, CancellationToken.None).Wait();
                    // Запуск ожидания текста в отдельном потоке
                    receiver = Task.Factory.StartNew(() => Receiver());
                    return;
                }
                catch (Exception ex)
                {
                    queue.Enqueue(new Message(MessageType.Error, ex.Message));
                    Task.Delay(TimeSpan.FromSeconds(2)).Wait();
                }
            }
        }

        /// <summary>
        /// Отправка сообщения на сервер
        /// </summary>     
        /// <param name="message"></param>
        public void Send(string message)
        {
            if (client == null)
            {
                queue.Enqueue(new Message(MessageType.Error, "Требуется установить соединение"));
                return;
            }
            byte[] data = System.Text.Encoding.UTF8.GetBytes(message);

            while (true)
            {
                try
                {
                    client.SendAsync(data, WebSocketMessageType.Text, true, CancellationToken.None).Wait();
                    return;
                }
                catch (Exception ex)
                {
                    queue.Enqueue(new Message(MessageType.Error, ex.Message));
                    Connect();
                }
            }
        }
        /*
        static void Main(string[] args)
        {
            try
            {
                var client = Connect();

                // Запуск ожидания текста в отдельном потоке
                Task task = Task.Factory.StartNew(() => Receiver(client));

                while (client.State == WebSocketState.Open)
                {
                    if (task.IsCompleted)
                    {
                        Console.WriteLine("reconnect");
                        task = Task.Factory.StartNew(() => Receiver(client));
                    }

                    Console.Write(">");
                    string message = Console.ReadLine();
                    if (message.ToLower() == "quit") break;

                    Send(ref client, message);
                }
                client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Bye bye", CancellationToken.None);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        */
    }
}
