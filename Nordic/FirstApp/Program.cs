﻿using System.ComponentModel;

namespace FirstApp
{
    internal class Program
    {
        static void PrintLine(int size)
        {
            for (int i = 0; i < size; i = i + 1)
            {
                Console.Write("+");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            // кортеж
            var tuple = new Tuple<dynamic, object, object, object>(1, "привет", 1.1, false);

            if (tuple.Item4 is bool test && test)
            {
                Console.WriteLine("кортеж это хорошо");
            }

            tuple.Item1.AnyFantasyForYourMoney(false);

            Console.Write("Размер квадрата > ");
            string s = Console.ReadLine();
            // Преобразование строки в целое число
            // Если строка некорректная, будет ошибка
            int size = int.Parse(s);
            // Верхняя часть квадрата
            PrintLine(size);
            // Стенки
            for (int i = 0; i < size - 2; i = i + 1)
            {
                Console.Write("+");
                for (int j = 0; j < size - 2; j = j + 1)
                {
                    Console.Write("=");
                }
                Console.WriteLine("+");
            }
            // Нижняя часть квадрата
            PrintLine(size);
            Console.ReadLine();
        }
    }
}