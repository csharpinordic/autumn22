﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.NUnit
{
    public class TestData
    {
        public static int count = 0;

        public int n;
        public long f;

        public TestData(int n, long f)
        {
            this.n = n;
            this.f = f;
            count++;
        }

        ~TestData()
        {
            count--;
        }

        public override string ToString() => $"{n}! = {f}";        
    }
}
