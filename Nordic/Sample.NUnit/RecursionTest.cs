using NUnit.Framework;

namespace Sample.NUnit
{
    public class RecursionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// ������������ ����������, ������ �����
        /// </summary>
        [Test()]
        public void TestFactoral1()
        {
            Assert.AreEqual(1, Recursion.Program.Factorial(0));
            Assert.AreEqual(1, Recursion.Program.Factorial(1));
            Assert.AreEqual(2, Recursion.Program.Factorial(2));
            Assert.AreEqual(6, Recursion.Program.Factorial(3));
            Assert.AreEqual(24, Recursion.Program.Factorial(4));
            Assert.AreEqual(120, Recursion.Program.Factorial(5));
        }

        /// <summary>
        /// ������������ ����������, ����� �����
        /// </summary>
        [Test]
        public void TestFactoral2()
        {
            Assert.That(Recursion.Program.Factorial(0), Is.EqualTo(1));
            Assert.That(Recursion.Program.Factorial(1), Is.EqualTo(1));
            Assert.That(Recursion.Program.Factorial(2), Is.EqualTo(2));
            Assert.That(Recursion.Program.Factorial(3), Is.EqualTo(6));
            Assert.That(Recursion.Program.Factorial(4), Is.EqualTo(24));
            Assert.That(Recursion.Program.Factorial(5), Is.EqualTo(120));
        }

        [TestCase(0, 1)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(3, 6)]
        [TestCase(4, 24)]
        [TestCase(5, 120)]
        [TestCase(6, 720)]
        public void TestFactorial3(int n, long f)
        {
            Assert.That(Recursion.Program.Factorial(n), Is.EqualTo(f));
        }

        public static List<TestData> GetData()
        {
            var list = new List<TestData>()
            {
                new TestData(0, 1),
                new TestData(1, 1),
                new TestData(2, 2),
                new TestData(3, 6),
                new TestData(4, 24)
            };
            return list;
        }

        [TestCaseSource(typeof(RecursionTest), nameof(GetData))]
        public void TestFactorial4(TestData item)
        {
            Assert.That(Recursion.Program.Factorial(item.n), Is.EqualTo(item.f));
        }

        public static IEnumerable<TestData> YieldData()
        {
            yield return new TestData(-1, 0);
            yield return new TestData(0, 1);
            yield return new TestData(1, 1);
            yield return new TestData(2, 2);
            yield return new TestData(3, 6);
            yield return new TestData(4, 24);
        }

        [TestCaseSource(typeof(RecursionTest), nameof(YieldData))]
        public void TestFactorial5(TestData item)
        {
            // �������� ���������� ��������
            Assume.That(item.n, Is.GreaterThanOrEqualTo(0));
            // ������������ ����������������
            Assert.That(Recursion.Program.Factorial(item.n), Is.EqualTo(item.f));
        }
    }
}