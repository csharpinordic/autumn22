﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBot
{
    /// <summary>
    /// Интерфейс состояния
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// Признак необходимости записи на диск
        /// </summary>
        bool IsDirty();

        /// <summary>
        /// Список пользователей
        /// </summary>
        /// <returns></returns>
        IEnumerable<User> GetUsers();

        /// <summary>
        /// Сохранение состояния в файл
        /// </summary>
        /// <param name="name"></param>
        void Save(string name);

        /// <summary>
        /// Проверка существования пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        bool UserExists(long id);

        /// <summary>
        /// Пользователь по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        User? GetUser(long id);

        /// <summary>
        /// Добавление нового пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        void AddUser(User user);
    }
}
