﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CSharpBot.Extenders;

namespace CSharpBot.Storage
{
    /// <summary>
    /// База данных ADO.NET
    /// </summary>
    internal class Database : IDisposable
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Соединение с СУБД
        /// </summary>
        private SqlConnection connection;

        internal Database()
        {
            connection = new SqlConnection()
            {
                ConnectionString = @"data source=.\SQLEXPRESS; initial catalog=BOT; integrated security=True; MultipleActiveResultSets=True; App=CSharpBot; "
            };
        }

        /// <summary>
        /// Создание SQL-команды с открытием соедиения при необходимости
        /// </summary>
        /// <returns></returns>
        private SqlCommand CreateCommand()
        {
            // Проверка на повторное открытие соединения
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }

            return connection.CreateCommand();
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        internal User GetUser(long id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Добавление пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        internal void AddUser(User user)
        {
            try
            {
                using SqlCommand command = CreateCommand();
                command.CommandText = "INSERT INTO [dbo].[Users] ([ID],[FirstName],[LastName],[UserName],[PhoneNumber],[State],[LastTimeSeen],[Room])" +
                    $"VALUES (@ID, @FirstName, @LastName, @UserName, @PhoneNumber, @State, @LastTimeSeen, @Room)";

                // Значения параметров запроса
                command.Parameters.Clear(); // демонстрация предварительной очистки списка параметров
                command.Parameters.AddWithValue("ID", user.ID);
                command.Parameters.AddWithNullValue("FirstName", user.FirstName);
                command.Parameters.AddWithNullValue("LastName", user.LastName);
                command.Parameters.AddWithNullValue("UserName", user.UserName);                
                command.Parameters.AddWithNullValue("PhoneNumber", user.PhoneNumber);
                command.Parameters.AddWithValue("State", user.State);
                command.Parameters.AddWithValue("LastTimeSeen", user.LastTimeSeen);
                command.Parameters.AddWithValue("Room", user.Room);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        internal void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        internal void WriteLog(UserLog data)
        {
            try
            {
                using SqlCommand command = CreateCommand();
                command.CommandText = "INSERT INTO [dbo].[UserLog] ([UserID],[UpdateType],[MessageType],[MessageText],[TimeStamp])" +
                    $"VALUES (@user,@updatetype,@messagetype,@text,@data)";

                // Значения параметров запроса
                command.Parameters.Clear(); // демонстрация предварительной очистки списка параметров
                command.Parameters.AddWithValue("user", data.UserID);
                command.Parameters.AddWithValue("updatetype", data.UpdateType);
                command.Parameters.AddWithValue("messagetype", data.MessageType);
                command.Parameters.AddWithValue("text", data.MessageText);
                command.Parameters.AddWithValue("data", data.TimeStamp);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
