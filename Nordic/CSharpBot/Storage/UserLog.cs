﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBot.Storage
{
    /// <summary>
    /// POCO - Plain Old C# Object
    /// </summary>
    internal class UserLog
    {
        internal Nullable<long> UserID;
        internal int UpdateType;
        internal Nullable<int> MessageType;
        internal string MessageText;
        internal DateTime TimeStamp;
    }
}
