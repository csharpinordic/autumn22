﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CSharpBot
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Признак модификации данных
        /// </summary>
        [JsonIgnore]
        public bool Dirty { get; private set; } = false;

        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Уникальный идентификатор пользователя
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Регистрационное имя
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        private UserState state;
        /// <summary>
        /// Состояние пользователя
        /// </summary>
        public UserState State
        {
            get => state;
            set
            {
                if (state != value) Dirty = true;
                state = value;
            }
        }

        private DateTime lastTimeSeen = DateTime.Now;
        /// <summary>
        /// Время последней активности пользователя
        /// </summary>
        public DateTime LastTimeSeen
        {
            get => lastTimeSeen;
            set
            {
                if (lastTimeSeen != value) Dirty = true;
                lastTimeSeen = value;
            }
        }

        private int room;
        /// <summary>
        /// Номер текущей комнаты пользователя
        /// </summary>
        public int Room
        {
            get => room;
            set
            {
                if (room != value) Dirty = true;
                room = value;
            }
        }

        /// <summary>
        /// Словарь игровых переменных 
        /// </summary>
        public Dictionary<string, double> Variables { get; set; } = new();

        /// <summary>
        /// Пример индексатора
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public double this[string name]
        {
            get
            {
                return GetVariable(name);
            }
            set
            {
                SetVariable(name, value);
            }
        }

        /// <summary>
        /// Сохранение значения переменной в словаре
        /// </summary>
        /// <param name="name"></param>
        /// <param name="number"></param>
        private void SetVariable(string name, double number)
        {
            if (Variables.ContainsKey(name))
            {
                Variables[name] = number;
            }
            else
            {
                Variables.Add(name, number);
            }
        }

        /// <summary>
        /// Значение переменной по имени
        /// </summary>
        /// <param name="name">Имя или шаблон переменной</param>
        /// <returns>По умолчанию значение переменной равно нулю</returns>
        private double GetVariable(string name)
        {
            // Если задано значение со звёздочкой, это означает сумму всех переменных,
            // которые соответствуют этому шаблону (начинаются с этих символов)
            if (name.EndsWith('*'))
            {
                // отрезать символ '*'
                string prefix = name.Substring(0, name.Length - 1);
                double summa = 0;
                foreach (var item in Variables)
                {
                    if (item.Key.StartsWith(prefix))
                    {
                        summa += item.Value;
                    }
                }
                return summa;
            }

            // Проверка существования переменной с заданным конкретным именем
            if (Variables.TryGetValue(name, out double number))
            {
                return number;
            }
            else
            {
                return default;
            }
        }

        /// <summary>
        /// Очистка строки от пробелов, преобразование многострочной строки в массив
        /// </summary>
        /// <param name="s">Строка (команда или условие)</param>
        /// <returns></returns>
        private string[] CleanString(string s)
        {
            // Удаление всех пробелов из строки
            s = s.Replace(" ", "");

            // Список команд, исключая пустые строки
            var strings = s.Split("\n", StringSplitOptions.RemoveEmptyEntries);

            return strings;
        }

        /// <summary>
        /// Предкомпилированный шаблон
        /// </summary>
        private static System.Text.RegularExpressions.Regex regex = new("^([A-Za-z][A-Za-z0-9]*)=([0-9]+([,.][0-9]+)?)$", System.Text.RegularExpressions.RegexOptions.Compiled);

        /// <summary>
        /// Обработка команды
        /// </summary>
        /// <param name="command">список команд, каждая команда с новой строки</param>
        public void ProcessCommand(string command)
        {
            // <variable> = <number>         

            var commands = CleanString(command);

            // Обработка команд последовательно
            foreach (var cmd in commands)
            {
                var match = regex.Match(cmd);
                if (!match.Success)
                {
                    log.Warn($"Некорректная команда: {cmd}");
                    return;
                }

                string variable = match.Groups[1].Value;
                string number = match.Groups[2].Value;
                double n = double.Parse(number, System.Globalization.CultureInfo.InvariantCulture);

                // Пример использования индексатора
                this[variable] = n;
            }
        }

        /// <summary>
        /// Проверка выполнения условия
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public bool CheckCondition(string condition)
        {
            // Если условие не задано, оно выполняется
            if (string.IsNullOrEmpty(condition)) return true;

            var conditions = CleanString(condition);

            // [!] доработать несколько условий
            foreach (var c in conditions)
            {
                var match = System.Text.RegularExpressions.Regex.Match(c, "^([A-Za-z][A-Za-z0-9]*(|\\*))(==|!=|>|<|>=|<=)([0-9]+([,.][0-9]+)?)$");
                if (!match.Success)
                {
                    log.Warn($"Некорректное условие: {c}");
                    return true;
                }

                // Пример использования индексатора
                double variable = this[match.Groups[1].Value];

                string operation = match.Groups[3].Value;
                double number = double.Parse(match.Groups[4].Value, System.Globalization.CultureInfo.InvariantCulture);

                switch (operation)
                {
                    case "==":
                        return variable == number;
                    case "!=":
                        return variable != number;
                    case ">=":
                        return variable >= number;
                    case "<=":
                        return variable <= number;
                    case ">":
                        return variable > number;
                    case "<":
                        return variable < number;
                    default:
                        log.Warn($"Некорректная операция сравнения: {operation}");
                        return true;
                }
            }

            return true;
        }

        /// <summary>
        /// Строкове представление пользователя
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                return string.IsNullOrEmpty(UserName) ? ID.ToString() : $"@{UserName}";

            if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
                return $"{FirstName} {LastName}";

            return string.IsNullOrEmpty(FirstName) ? LastName : FirstName;
        }

        /// <summary>
        /// Сброс признака необходимости записи данных
        /// </summary>
        public void SetClean()
        {
            Dirty = false;
        }
    }
}