﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace CSharpBot.Extenders
{
    /// <summary>
    /// Расширение стандартных классов
    /// </summary>
    public static class Extender
    {
        /// <summary>
        /// Добавление параметра со значением, которое может быть null
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра, в том числе поддерживается null</param>
        public static void AddWithNullValue(this SqlParameterCollection collection, string name, object value)
        {
            collection.AddWithValue(name, value ?? DBNull.Value);
        }
    }
}
