﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBot
{
    /// <summary>
    /// Состояние пользователя 
    /// (сохраняется в БД в виде числа)
    /// </summary>
    public enum UserState
    {
        None = 0,        
        Register = 1,
        Game = 2,
    }
}
