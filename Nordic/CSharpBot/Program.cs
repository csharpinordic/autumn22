﻿using Microsoft.Extensions.Configuration;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSharpBot
{
    public class Program
    {
        static string ReadToken()
        {
            // Имя текущего исполняемого файла
            string name = System.Reflection.Assembly.GetExecutingAssembly().Location;
            // Путь к каталогу с текущим исполняемый файлом
            string path = System.IO.Path.GetDirectoryName(name);
            // Полный путь к файлу конфигурации
            string configName = System.IO.Path.Combine(path, "appsettings.json");
            // Конфигурация, загруженная в память
            IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile(configName).Build();
            string token = config.GetSection("Token").Value;
            return token;
        }

        static void Main()
        {
            try
            {
                // Чтение ключа доступа
                string token = ReadToken();

                // Создание клиента для бота
                var client = new TelegramBotClient(token);

                // Запрос сведений о боте
                var user = client.GetMeAsync().Result;
                Console.WriteLine(user);

                // Подписка на сообщения от бота
                using var handler = new MessageHandler(client);
                client.StartReceiving(handler);

                // Ожидать
                Console.WriteLine("Бот запущен");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.GetType().FullName}: {ex.Message}");
            }
        }
    }
}