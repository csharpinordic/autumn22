﻿using CSharpBot.Storage;
using System.Reflection;
using System.Xml.Linq;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace CSharpBot
{
    /// <summary>
    /// Обработчик сообщений для бота
    /// </summary>
    public class MessageHandler : IUpdateHandler, IDisposable
    {
        /// <summary>
        /// Имя файла состояния
        /// </summary>
        private const string StateFileName = "state.json";

        private const string PathForInputDocuments = @"./documents/";

        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// База данных
        /// </summary>
        Database db = new();

        ITelegramBotClient client;

        /// <summary>
        /// Состояние бота, сохраняется в файл JSON
        /// </summary>
        IState state;

        GameFile.Game game;

        System.Timers.Timer timer;

        /// <summary>
        /// Наблюдение за событиями файловой системы
        /// </summary>
        private System.IO.FileSystemWatcher watch;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MessageHandler(ITelegramBotClient client)
        {
            // При одноименном поле класса и параметре для уточнения,
            // что речь идёт о поле класса, используется this
            this.client = client;

            // Загрузка состояния игры
            state = State.Load(StateFileName);

            // Загрузка сценария игры
            game = GameFile.Game.Load("quest.xml");

            // Подсчет количества концовок
            var ends = game.GetGameEnds();
            log.Info($"В игре {ends} концовок.");

            // Проверяем наличие папки для документов, при отсутствии - создаем
            Directory.CreateDirectory(PathForInputDocuments);

            // Создание сторожевого таймера
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            // Наблюдение за изменениями файла состояния
            string path = AppContext.BaseDirectory;
            watch = new FileSystemWatcher(path, StateFileName);
            watch.Changed += State_Changed;
            watch.Created += State_Changed;
            watch.Renamed += State_Changed;
            // Наверное, не надо следить именно за удалением
            // watch.Deleted += State_Changed;
            watch.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Обработка изменения файла состояния бота
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void State_Changed(object sender, FileSystemEventArgs e)
        {
            log.Info($"Файл {e.Name}: {e.ChangeType}");
            // Перезагрузка состояния игры
            state = State.Load(StateFileName);
        }

        /// <summary>
        /// Цикл таймера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void Timer_Elapsed(object? sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                DateTime now = DateTime.Now;

                // Перебираем всех пользователей
                foreach (var user in state.GetUsers())
                {
                    if (user.State != UserState.None && (now - user.LastTimeSeen).TotalSeconds > 60)
                    {
                        user.State = UserState.None;
                        client.SendTextMessageAsync(user.ID, $"Вы про меня забыли :-(");
                    }
                }

                // Временно перестанем наблюдать за файлом состояния
                if (state.IsDirty())
                {
                    watch.EnableRaisingEvents = false;
                    state.Save(StateFileName);
                    log.Trace($"Состояние бота записано в файл {StateFileName}");
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                watch.EnableRaisingEvents = true;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        #region "Прикладные методы"

        /// <summary>
        /// Определение текущего пользователя
        /// </summary>
        /// <param name="message">Входящее сообщение</param>
        /// <param name="user">Пользователь или null, если не найден</param>
        /// <returns>true - пользователь найден</returns>
        [Obsolete("Надо бы перейти на метод State.GetUser")]
        private bool GetUser(Message message, out User? user)
        {
            // Защитная проверка
            if (!state.UserExists(message.Chat.Id))
            {
                user = null;
                return false;
            }
            // Текущий пользователь
            user = state.GetUser(message.Chat.Id);
            return true;
        }

        /// <summary>
        /// Вывод комнаты с её действиями
        /// </summary>
        /// <param name="client"></param>
        /// <param name="id"></param>
        /// <param name="user">Игрок</param>
        private void ShowRoom(ITelegramBotClient client, long id, User user)
        {
            GameFile.Location location = game.Locations.First(x => x.ID == user.Room);

            // По умолчанию кнопки действий располагаются в две колонки
            int columns = location.Columns > 0 ? location.Columns : 2;

            // Клавиатура из нескольких строк кнопок
            List<List<KeyboardButton>> buttons = new();
            // Строка кнопок
            List<KeyboardButton> row = new();

            // Формирование кнопки для каждого допустимого действия
            foreach (var action in location.Actions)
            {
                if (!user.CheckCondition(action.Condition))
                {
                    continue;
                }

                // Создание очередной кнопки
                KeyboardButton button = new(action.Name);
                row.Add(button);

                if (row.Count >= columns)
                {
                    buttons.Add(row);
                    row = new List<KeyboardButton>();
                }
            }

            // Добавление последней кнопки
            if (row.Count > 0)
            {
                buttons.Add(row);
            }

            var markup = new ReplyKeyboardMarkup(buttons)
            {
                ResizeKeyboard = true,
                OneTimeKeyboard = true
            };

            // Вывести текст комнаты
            client.SendTextMessageAsync(id, location.Text, replyMarkup: markup);
        }

        #endregion

        /// <summary>
        /// Обработка ошибок
        /// </summary>
        /// <param name="botClient"></param>
        /// <param name="ex"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleErrorAsync(ITelegramBotClient botClient, Exception ex, CancellationToken cancellationToken)
        {
            log.Error(ex.Message);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Обработка входящих обновлений
        /// </summary>
        /// <param name="client"></param>
        /// <param name="update"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleUpdateAsync(ITelegramBotClient client, Update update, CancellationToken cancellationToken)
        {
            UserLog item;
            switch (update.Type)
            {
                case Telegram.Bot.Types.Enums.UpdateType.Message:
                    item = new UserLog()
                    {
                        UserID = update.Message.Chat.Id,
                        UpdateType = (int)update.Type,
                        MessageType = (int)update.Message.Type,
                        MessageText = update.Message.Text,
                        TimeStamp = DateTime.Now
                    };
                    db.WriteLog(item);
                    DispatchMessage(client, update.Message);
                    break;

                default:
                    item = new UserLog()
                    {
                        UpdateType = (int)update.Type,
                        TimeStamp = DateTime.Now
                    };
                    db.WriteLog(item);
                    log.Warn($"Тип сообщений не обрабатывается: {update.Type}");
                    break;
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Обработка сообщения
        /// </summary>
        /// <param name="message">Сообщение</param>
        private void DispatchMessage(ITelegramBotClient client, Message message)
        {
            // Фиксация метки времени активности пользователя
            if (GetUser(message, out User user))
            {
                user.LastTimeSeen = DateTime.Now;
            }

            // Имя метода, который я хочу вызвать
            string name = $"Process{message.Type}";

            // Описание типа данных для объекта
            Type type = this.GetType();

            // Информация о закрытом методе класса
            MethodInfo method = type.GetMethod(name, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

            // Проверка на существование метода
            if (method == null)
            {
                log.Warn($"Тип сообщений не обрабатывается: {message.Type}");
                client.SendTextMessageAsync(message.Chat.Id, $"Тип сообщений не обрабатывается: {message.Type}");
                return;
            }

            // Вызов метода с двумя параметрами
            try
            {
                method.Invoke(this, new object[] { client, message });
            }
            catch (System.Reflection.TargetParameterCountException)
            {
                log.Warn($"Тип сообщений не обрабатывается: {message.Type}");
                client.SendTextMessageAsync(message.Chat.Id, $"Тип сообщений не обрабатывается: {message.Type}");
                return;
            }
        }

        #region "Обработка различных типов сообщений"

        /// <summary>
        /// Обработка голосовых сообщений
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessVoice(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал голосовое сообщение.");
            client.SendTextMessageAsync(message.Chat.Id, $"{message.Chat.FirstName}, а я отвечу текстом.");
        }

        /// <summary>
        /// Обработка видео
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessVideo(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал видео.");
            client.SendTextMessageAsync(message.Chat.Id, $"{message.Chat.FirstName}, отличное видео.");
        }

        /// <summary>
        /// Обработка стикеров
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessSticker(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал стикер.");

            // Получаем коллекцию стикеров по имени присланного
            var stikerSet = client.GetStickerSetAsync(message.Sticker.SetName).Result;

            // Берем последний стикер в коллекции
            var sticker = stikerSet.Stickers.Last();

            // Создаем экземпляр входящего файла для стикера
            InputOnlineFile file = new InputOnlineFile(sticker.FileId);

            // Отправляем стикер в ответ
            client.SendStickerAsync(message.Chat.Id, file);
        }

        /// <summary>
        /// Обработка фото
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessPhoto(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал фото.");

            if (message.Photo == null || message.Photo.Length == 0)
                return;

            var photo = message.Photo.FirstOrDefault();

            if (photo == null)
                return;

            client.SendPhotoAsync(message.Chat.Id, new InputOnlineFile(photo.FileId), caption: "Вы мне прислали вот это фото.").Wait();
        }

        /// <summary>
        /// Обработка документов
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessDocument(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал документ.");
            if (message.Document == null)
                return;

            var fileInfo = client.GetFileAsync(message.Document.FileId).Result;

            if (fileInfo == null || fileInfo.FilePath == null)
                return;

            // Открываем поток записи файла
            using (FileStream fs = new FileStream(PathForInputDocuments + message.Document.FileName, FileMode.Create))
            {
                // Скачиваем файл
                client.DownloadFileAsync(fileInfo.FilePath, fs).Wait();
            }

            // Открываем поток чтения файла
            using (FileStream fs = new FileStream(PathForInputDocuments + message.Document.FileName, FileMode.Open))
            {

                // Читаем файл
                InputOnlineFile file = new InputOnlineFile(fs, message.Document.FileName);
                // Отправляем назад пользователю
                client.SendDocumentAsync(message.Chat.Id, file, caption: "Я сохранил ваш файл, получился вот такой.").Wait();
            }
        }

        /// <summary>
        /// Обработка аудио
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessAudio(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: прислал аудио.");

            if (message.Audio == null)
                return;

            client.SendTextMessageAsync(message.Chat.Id, $"Имя файла: {message.Audio.FileName}").Wait();
            client.SendTextMessageAsync(message.Chat.Id, $"Размер: {message.Audio.FileSize} байт").Wait();
            client.SendTextMessageAsync(message.Chat.Id, $"Формат: {message.Audio.MimeType}").Wait();
        }

        /// <summary>
        /// Обработка текстового сообщения
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessText(ITelegramBotClient client, Message message)
        {
            // Протоколирование
            log.Trace($"{message.Chat.FirstName}: {message.Text}");
            if (message.Text[0] == '/')
            {
                DispatchCommand(client, message);
                return;
            }

            if (!GetUser(message, out User user))
            {
                client.SendTextMessageAsync(message.Chat.Id, $"Вы не зарегистрированы");
                return;
            }

            // Если пользователь сейчас играет
            if (user.State == UserState.Game)
            {
                // тут игра
                GameFile.Location location = game.Locations.First(x => x.ID == user.Room);
                var action = location.Actions.FirstOrDefault(x => x.Name == message.Text);
                if (action == null)
                {
                    client.SendTextMessageAsync(message.Chat.Id, $"Неправильное действие: {message.Text}");
                    return;
                }

                // показ текста, который соответствует действию
                if (!string.IsNullOrEmpty(action.Text))
                {
                    // обязательно подождать завершения асинхронного вызова
                    client.SendTextMessageAsync(message.Chat.Id, action.Text).Wait();
                }

                // проверка на наличие команды
                if (!string.IsNullOrEmpty(action.Command))
                {
                    user.ProcessCommand(action.Command);
                }

                // проверка на переход в новую комнату
                if (action.Next.HasValue)
                {
                    user.Room = action.Next.Value;
                }

                // вывод комнаты (новой или текущей)
                ShowRoom(client, message.Chat.Id, user);
            }
            else
            {
                client.SendTextMessageAsync(message.Chat.Id, $"{message.Chat.FirstName}, вы сказали мне: {message.Text}");
            }
        }

        /// <summary>
        /// Приём номера телефона
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void ProcessContact(ITelegramBotClient client, Message message)
        {
            // Защитная проверка
            if (!GetUser(message, out User user))
            {
                client.SendTextMessageAsync(message.Chat.Id, $"Я вас не знаю!");
                return;
            }
            else
            {
                // Проверка на регистрацию
                if (user.State != UserState.Register)
                {
                    client.SendTextMessageAsync(message.Chat.Id, $"Зачем мне сейчас номер телефона?");
                    return;
                }
                // Проверка на чужой номер телефона
                if (user.ID != message.Contact.UserId)
                {
                    client.SendTextMessageAsync(message.Chat.Id, $"Это не ваш номер телефона!");
                    return;
                }

                // Сохранение данных
                user.PhoneNumber = message.Contact.PhoneNumber;
                user.State = UserState.None;
                client.SendTextMessageAsync(message.Chat.Id, $"Спасибо, номер записан");
            }
        }

        #endregion

        /// <summary>
        /// Обработка команды
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void DispatchCommand(ITelegramBotClient client, Message message)
        {
            // Определение имени команды - отсечение первого символа
            string command = message.Text.Substring(1);
            if (!string.IsNullOrEmpty(command))
            {
                string methodName = $"Command{char.ToUpper(command[0])}{command.Remove(0, 1)}";

                var methodInfo = GetType().GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);

                if (methodInfo != null)
                {
                    methodInfo.Invoke(this, new object[] { client, message });
                    return;
                }
            }

            string s = $"Команда {command} не поддерживается";
            log.Warn(s);
            client.SendTextMessageAsync(message.Chat.Id, s);
        }

        #region "Команды"

        /// <summary>
        /// Начало взаимодействия с ботом
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void CommandStart(ITelegramBotClient client, Message message)
        {
            if (GetUser(message, out User _))
            {
                client.SendTextMessageAsync(message.Chat.Id, "Рад вашему возвращению!");
                return;
            }

            // Создание пользователя
            var user = new User()
            {
                ID = message.Chat.Id,
                FirstName = message.Chat.FirstName,
                LastName = message.Chat.LastName,
                UserName = message.Chat.Username
            };

            // Добавление пользователя в словарь
            state.AddUser(user);

            // Сохранение пользователя в БД
            db.AddUser(user);

            client.SendTextMessageAsync(message.Chat.Id, "Привет, рад знакомству!");
        }

        private void CommandPlay(ITelegramBotClient client, Message message)
        {
            if (!GetUser(message, out User user))
            {
                client.SendTextMessageAsync(message.Chat.Id, "Я с незнакомцами не играю!");
            }
            else
            {
                // Ожидаем выполнения асинхронной операции
                client.SendTextMessageAsync(message.Chat.Id, "Сейчас сыграем!").Wait();

                // переходим в состояние игры
                user.State = UserState.Game;

                // Начальная комната игры
                user.Room = game.Locations.Min(location => location.ID);

                // вывести комнату по идентификатору 
                ShowRoom(client, message.Chat.Id, user);
            }
        }

        private void CommandReset(ITelegramBotClient client, Message message)
        {
            if (!GetUser(message, out User user))
            {
                client.SendTextMessageAsync(message.Chat.Id, "Я с незнакомцами не играю!");
            }
            else
            {
                // Очистка всех игровых переменных
                user.Variables.Clear();
                client.SendTextMessageAsync(message.Chat.Id, "Игра сброшена к началу");
            }
        }

        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        private void CommandRegister(ITelegramBotClient client, Message message)
        {
            // Защитная проверка
            if (!GetUser(message, out User user))
            {
                client.SendTextMessageAsync(message.Chat.Id, $"Я вас не знаю!");
            }
            else
            {
                // Проверка на повторный запуск
                if (!string.IsNullOrEmpty(user.PhoneNumber))
                {
                    client.SendTextMessageAsync(message.Chat.Id, $"Спасибо, ваш номер уже записан!");
                    return;
                }

                // Создание объекта при помощи конструктора и иницализатора
                var button = new KeyboardButton("Отправить номер")
                {
                    RequestContact = true
                };

                var reply = new ReplyKeyboardMarkup(button)
                {
                    OneTimeKeyboard = true,
                    ResizeKeyboard = true
                };
                client.SendTextMessageAsync(message.Chat.Id, "Для регистрации сообщите номер телефона", replyMarkup: reply);

                user.State = UserState.Register;
            }
        }


        #endregion
    }
}
