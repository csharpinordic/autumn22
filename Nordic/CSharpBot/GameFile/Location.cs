﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CSharpBot.GameFile
{
    public class Location
    {
        public int ID;

        public string Name;

        public string Text;

        [XmlAttribute()]
        public int Columns;

        /// <summary>
        /// Список возможных действий комнаты
        /// </summary>
        [XmlElement(ElementName = "Action")]
        public Action[] Actions;

        /// <summary>
        /// Строковое представление объекта - идентификатор комнаты
        /// </summary>
        /// <returns></returns>
        public override string ToString() => ID.ToString();
    }
}
