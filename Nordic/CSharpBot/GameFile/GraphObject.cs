﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBot.GameFile
{
    /// <summary>
    /// Модель графа
    /// </summary>
    internal class GraphObject
    {
        public Location Current { get; set; }
        public Location Next { get; set; }

        public bool IsFinal { get; set; }
        public bool IsStart { get; set; }

        public override string ToString() => $"{Current.ID} -> {Next.ID}";
    }
}
