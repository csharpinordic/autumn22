﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CSharpBot.GameFile
{
    /// <summary>
    /// Сценарий игры
    /// </summary>
    [XmlRoot(Namespace = "http:/quiz.orioner.ru/12floor/quest.xsd")]
    public class Game
    {
        public string Title;

        public string Description;

        [XmlElement(ElementName = "Location")]
        public Location[] Locations;

        public static Game Load(string name)
        {
            using XmlReader reader = XmlReader.Create(name);
            XmlSerializer serializer = new XmlSerializer(typeof(Game));
            var game = (Game)serializer.Deserialize(reader);
            return game;
        }

        /// <summary>
        /// Сохранить объект в XML-файл
        /// </summary>
        /// <param name="name"></param>
        public void Save(string name)
        {
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Indent = true // XML, отформатированный для человека
            };
            // Освобождение ресурсов в конце блока
            using XmlWriter writer = XmlWriter.Create(name, settings);
            XmlSerializer serializer = new XmlSerializer(typeof(Game));
            serializer.Serialize(writer, this);
        }

        /// <summary>
        /// Расчет количества концовок игры
        /// </summary>
        public int GetGameEnds()
        {
            int startLocationID = Locations.Min(a => a.ID);
            var startLocation = Locations.FirstOrDefault(a => a.ID == startLocationID);

            if (startLocation == null)
                return 0;

            GraphObject startTop = new GraphObject
            {
                Current = startLocation,
                Next = startLocation,
                IsStart = true
            };

            List<GraphObject> allTops = new List<GraphObject>();
            CalculateAllTops(startTop, allTops, startLocationID);

            var endTops = allTops.Where(top => top.IsFinal && !top.IsStart);

            return endTops.Count();            
        }

        /// <summary>
        /// Расчет всех вершин
        /// </summary>
        /// <param name="currentTop">Текущая вершина</param>
        /// <param name="tops">Коллекция для записи</param>
        /// <param name="startLocationID">Идентификатор стартовой вершины (для исключения зацикливаний)</param>
        private void CalculateAllTops(GraphObject currentTop, List<GraphObject> tops, int startLocationID)
        {
            // Игра заканчивается в вершине, из которой существует только одна дуга – в начальную вершину.
            if (currentTop.Current.Actions.Length == 1 && currentTop.Current.Actions[0].Next == startLocationID)
            {
                currentTop.IsFinal = true;
                return;
            }

            foreach(var action in currentTop.Next.Actions)
            {
                var loc = Locations.FirstOrDefault(a => a.ID == action.Next);

                // Проверяем, есть ли следующая вершина в которой мы еще не были
                if (loc == null || tops.FirstOrDefault(l=>l.Current == currentTop.Next && l.Next.ID == loc.ID) != null)
                    continue;

                var newTop = new GraphObject
                {
                    Current = currentTop.Next,
                    Next = loc
                };

                tops.Add(newTop);

                CalculateAllTops(newTop, tops, startLocationID);
            }
        }
    }
}
