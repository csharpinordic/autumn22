﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpBot.GameFile
{
    public class Action
    {
        public string Name;

        /// <summary>
        /// Идентификатор следующей комнаты - класс <seealso cref="Location"/>
        /// </summary>
        public int? Next;

        public string Text;

        /// <summary>
        /// Условие, при котором действие доступно для игрока
        /// </summary>
        public string Condition;

        /// <summary>
        /// Команда, которая выполняется при выборе действия
        /// </summary>
        public string Command;
    }
}
