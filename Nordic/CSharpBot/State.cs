﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Telegram.Bot.Types;

namespace CSharpBot
{
    /// <summary>
    /// Состояние бота
    /// </summary>
    public class State : IState
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Признак необходимости записи состояния в файл
        /// </summary>       
        private bool Dirty = false;

        /// <summary>
        /// Словарь пользователей
        /// </summary>
        public Dictionary<long, User> Users { get; set; } = new();

        /// <summary>
        /// Чтение состояния из файла в формате JSON
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal static State Load(string name)
        {
            try
            {
                // Чтение файла
                string json = System.IO.File.ReadAllText(name);
                // Десериализация
                var state = JsonSerializer.Deserialize<State>(json);

                // Прочитали файл, но он не содержит данных
                if (state == null)
                    state = new State();

                log.Info($"Файл {name} успешно загружен");

                return state;
            }
            catch (System.IO.FileNotFoundException)
            {
                log.Warn($"Не найден файл {name}, создаётся пустое состояние");
                return new();
            }
            catch (Exception ex) // [!] уточнить
            {
                System.IO.File.Move(name, System.IO.Path.ChangeExtension(name, ".backup"));
                log.Error($"Некорректный формат файла {name}, файл переименован");
                return new();
            }
        }

        /// <summary>
        /// Сохранение состояния в файле в формате JSON
        /// </summary>
        /// <param name="name"></param>
        public void Save(string name)
        {
            // Настройки сериализации
            var settings = new JsonSerializerOptions()
            {
                // Отмена экранирования любых символов 
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),

                // Сделать JSON человекочитаемым
                WriteIndented = true // Newtonsoft.Json: Formatting = Formatting.Indented,  

                // NullValueHandling = NullValueHandling.Ignore // не записывать значения null
            };
            // Сериализация в строку
            string json = JsonSerializer.Serialize(this, this.GetType(), settings);
            // Запись строки в файл
            System.IO.File.WriteAllText(name, json);

            // Состояние успешно записано в файл
            Dirty = false;
            foreach (var user in Users.Values)
            {
                user.SetClean();
            }
        }

        /// <summary>
        /// Добавление нового пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        public void AddUser(User user)
        {
            Users.Add(user.ID, user);
            Dirty = true;
        }

        /// <summary>
        /// Список пользователей
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetUsers() => Users.Values;

        /// <summary>
        /// Проверка существования пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        public bool UserExists(long id) => Users.ContainsKey(id);

        /// <summary>
        /// Пользователь по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns></returns>
        public User? GetUser(long id) => Users.ContainsKey(id) ? Users[id] : null;

        /// <summary>
        /// Признак необходимости записи на диск
        /// </summary>
        /// <returns></returns>
        public bool IsDirty()
        {
            return Dirty || Users.Values.Where(x => x.Dirty).Any();
        }
    }
}
