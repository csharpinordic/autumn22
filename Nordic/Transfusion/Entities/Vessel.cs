﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transfusion.Entities
{
    /// <summary>
    /// Сосуд (кувшин, банка, всё такое)
    /// </summary>
    public class Vessel
    {
        /// <summary>
        /// Номер сосуда, начиная с 1
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Емкость сосуда
        /// </summary>
        public int Size { get; set; }
    }
}
