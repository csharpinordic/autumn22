﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transfusion.Entities
{
    /// <summary>
    /// Состояние игры
    /// </summary>
    public class State
    {
        /// <summary>
        /// Уровень жидкости в каждом сосуде
        /// </summary>
        public int?[] Level { get; set; }

        /// <summary>
        /// Признак начальной вершины графа (стартовое состояние игры)
        /// </summary>
        public bool IsStart { get; set; }

        /// <summary>
        /// Признак конечной вершины графа (финальное состояние игры)
        /// </summary>
        public bool IsFinal { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="level">Уровень жидкости</param>
        public State(params int?[] level)
        {
            Level = new int?[level.Length];
            for (int i = 0; i < Level.Length; i++)
            {
                Level[i] = level[i];
            }
        }

        /// <summary>
        /// Копирующий (клонирующий) конструктор
        /// </summary>
        /// <param name="state">Исходное состояние</param>
        public State(State state)
        {
            Level = new int?[state.Level.Length];
            for (int i = 0; i < Level.Length; i++)
            {
                Level[i] = state.Level[i];
            }
        }

        /// <summary>
        /// Строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString() => string.Join(' ', Level);

        /// <summary>
        /// Сравнение двух объектов на равенство по значениям
        /// </summary>
        /// <param name="state">Сравниваемый объект</param>
        /// <returns></returns>
        private bool IsEqual(State state)
        {
            // [10, 0, 1] == [10, 0, null]
            // [10, 1, 0] != [10, 0, 1]
            // [10, 0, null] == [10, 0, null]

            for (int i = 0; i < Level.Length; i++)
            {
                // Ищем первый несовпадающий элемент
                if (Level[i].HasValue
                    && state.Level[i].HasValue
                    && (Level[i] != state.Level[i]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Оператор сравнения на равенство по значениям
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(State a, State b)
        {
            return a.IsEqual(b);
        }

        /// <summary>
        /// Оператор сравнения на неравенство по значениям
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(State a, State b) => !(a == b);
    }
}
