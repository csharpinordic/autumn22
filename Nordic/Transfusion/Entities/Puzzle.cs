﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transfusion.Entities
{
    /// <summary>
    /// Головоломка
    /// </summary>
    public class Puzzle
    {
        /// <summary>
        /// Массив сосудов
        /// </summary>
        public Vessel[] Vessels { get; set; }

        /// <summary>
        /// Список состояний (вершин графа)
        /// </summary>
        public List<State> States { get; set; } = new();

        /// <summary>
        /// Список переливаний (ориентированных рёбер графа)
        /// </summary>
        public List<Move> Moves { get; set; } = new();

        /// <summary>
        /// Финальное состояние
        /// </summary>
        public State FinalState { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="size">Размеры сосудов</param>
        public Puzzle(params int[] size)
        {
            Vessels = new Vessel[size.Length];

            for (int i = 0; i < Vessels.Length; i++)
            {
                Vessels[i] = new Vessel()
                {
                    Number = i + 1,
                    Size = size[i]
                };
            }
        }

        /// <summary>
        /// Формирование всех возможных ходов из заданного состояния
        /// </summary>
        /// <param name="state">Исходное состояние</param>
        /// <returns></returns>
        public List<Move> MakeMoves(State state)
        {
            List<Move> result = new();

            // Откуда переливаем
            for (int from = 0; from < Vessels.Length; from++)
            {
                // Куда переливаем
                for (int to = 0; to < Vessels.Length; to++)
                {
                    // Нельзя перелить из сосуда в него же
                    if (from == to) continue;

                    // Нельзя перелить из пустого сосуда
                    if (state.Level[from] == 0) continue;

                    // Нельзя перелить в полный сосуд
                    if (state.Level[to] == Vessels[to].Size) continue;

                    var move = new Move()
                    {
                        FromVessel = from,
                        FromState = state,
                        ToVessel = to
                    };
                    result.Add(move);
                }
            }

            return result;
        }

        /// <summary>
        /// Формирование нового состояния
        /// </summary>
        /// <param name="state">Исходное состояние</param>
        /// <param name="move">Ход</param>
        /// <returns></returns>
        public State NextState(State state, Move move)
        {
            // Сколько надо перелить жидкости
            int n1 = state.Level[move.FromVessel].Value;

            // Сколько можно перелить жидкости
            int n2 = Vessels[move.ToVessel].Size - state.Level[move.ToVessel].Value;

            // Копируем число сосудов в новое состояние
            State newState = new(state);

            if (n1 > n2) // Неполное переливание
            {
                newState.Level[move.FromVessel] -= n2;
                newState.Level[move.ToVessel] += n2;
            }
            else // Полное переливание
            {
                newState.Level[move.FromVessel] -= n1;
                newState.Level[move.ToVessel] += n1;
            }

            return newState;
        }

        /// <summary>
        /// Решение головоломки
        /// </summary>
        /// <param name="startState">Начальное состояние</param>
        public bool Solve(State startState)
        {
            // Запомним стартовое состояние
            startState.IsStart = true;
            States.Add(startState);

            // Очередь необработанных вершин
            Queue<Entities.State> queue = new();

            // Поместить в очередь
            queue.Enqueue(startState);

            // Пока очередь не пуста, повторяем
            while (queue.TryDequeue(out var currentState))
            {
                // Все возможные ходы из текущего состояния
                var moves = MakeMoves(currentState);

                // Формирование состояний, куда ведут эти ходы
                foreach (var move in moves)
                {
                    // Новое состояние
                    var newState = NextState(currentState, move);

                    // Проверка, не присутствует ли уже это состояние в графе
                    bool exists = false;
                    foreach (var state in States)
                    {
                        if (state == newState)
                        {
                            exists = true;
                            break;
                        }
                    }
                    // Если не присутствует, добавить состояние к графу
                    if (!exists)
                    {
                        move.ToState = newState;
                        Moves.Add(move);
                        States.Add(newState);

                        // Проверка на нахождение финальной вершины
                        if (newState == FinalState)
                        {
                            newState.IsFinal = true;
                            // решение найдено
                            return true;
                        }

                        queue.Enqueue(newState);
                    }
                }
            }

            // Решение не найдено
            return false;
        }

        /// <summary>
        /// Список ходов - решение головоломки
        /// </summary>
        /// <returns></returns>
        public List<Move> Solution()
        {
            var list = new List<Move>();
            var stack = new Stack<Move>();
            var state = States.First(x => x.IsFinal);

            while (!state.IsStart)
            {
                var move = Moves.First(x => x.ToState == state);
                stack.Push(move);
                state = move.FromState;
            }

            // Вариант: Можно было бы сложить в список и сделать .Reverse() 

            while (stack.TryPop(out Move item))
            {
                list.Add(item);
            }
            
            return list;
        }
    }
}
