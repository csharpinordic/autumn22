﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transfusion.Entities
{
    /// <summary>
    /// Переливание
    /// </summary>
    public class Move
    {
        /// <summary>
        /// Индекс сосуда, из которого переливаем
        /// </summary>
        public int FromVessel { get; set; }
        /// <summary>
        /// Индекс сосуда, в который переливаем
        /// </summary>
        public int ToVessel { get; set; }
        /// <summary>
        /// Вершина графа, из которой перемещаемся
        /// </summary>
        public State FromState { get; set; }
        /// <summary>
        /// Вершина графа, в которую мы идём
        /// </summary>
        public State ToState { get; set; }  

        /// <summary>
        /// Строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{FromVessel} => {ToVessel}";
    }
}
