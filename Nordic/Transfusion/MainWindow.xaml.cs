﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Transfusion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Entities.Puzzle puzzle;

        public MainWindow()
        {
            InitializeComponent();

            puzzle = new Entities.Puzzle(10, 5, 6);
            puzzle.FinalState = new Entities.State(8, null, null);
            bool success = puzzle.Solve(new Entities.State(10, 0, 0));
            if (success)
            {
                var result = puzzle.Solution();
            }
        }
    }
}
