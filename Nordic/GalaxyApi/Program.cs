using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client.Extensions.Msal;
using NLog.Extensions.Logging;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Galaxy.Api
{
    public class Program
    {
        /// <summary>
        /// ����������������
        /// </summary>
        private readonly static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public static void Main(string[] args)
        {
            try
            {
                var config = Globals.AutoMapperConfig.Config;
                config.AssertConfigurationIsValid();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // ��������� ���-�������
            var configService = Settings.ConfigurationManager.Configuration.GetSection(nameof(Settings.Service)).Get<Settings.Service>();
            var configDatabase = Settings.ConfigurationManager.Configuration.GetSection(nameof(Settings.Database)).Get<Settings.Database>();

            WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers()
                .AddJsonOptions(option =>
                {
                    // ���������������� ������������ JSON
                    option.JsonSerializerOptions.WriteIndented = true;
                    // ��� �������������� ��� �������
                    option.JsonSerializerOptions.PropertyNamingPolicy = null;
                });

            // �������� �������� ������������
            builder.Services.AddScoped<Services.ICalculator, Services.Calculator>();

            // builder.Services.AddSingleton<Services.ICalculator, Services.Calculator>();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(swagger =>
            {
                var info = new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Galaxy API",
                    Description = "Galaxy REST-based API",
                    Version = "v1"
                };
                swagger.SwaggerDoc(info.Version, info);
                string name = System.IO.Path.Combine(System.AppContext.BaseDirectory, "XmlDocumentation.xml");
                if (System.IO.File.Exists(name))
                {
                    swagger.IncludeXmlComments(name);
                }
                else
                {
                    // ���� �� ����������������� ������
                }
            });

            // ��������� ���������� ��� ������ ������������ �������
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                builder.Services.AddWindowsService(configure =>
                {
                    configure.ServiceName = "GalaxyAPI";
                });
            }

            // ��������� ���������� ��� ������ Linux
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                // ...
            }

            // ��������� ���� ������
            builder.Services.AddDbContext<Common.Storage.Database>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer(configDatabase.ConnectionString);
            });
            log.Info($"���������� � ��: {configDatabase.ConnectionString}");

            // ��������� ����������������
            builder.WebHost.ConfigureLogging((context, log) =>
            {
                // �������� ������������ ����������������
                log.ClearProviders();
                // ����������� ���������������� NLog
                log.AddNLog(context.Configuration);
            });

            if (configService.HttpPort.HasValue)
            {
                builder.WebHost.UseUrls($"http://*:{configService.HttpPort}");
                log.Info($"������ ����������� �� http/{configService.HttpPort}");
            }
            else if (configService.HttpsPort.HasValue && !string.IsNullOrEmpty(configService.PfxFile))
            {
                builder.WebHost.UseKestrel(options =>
                {
                    options.ListenAnyIP(configService.HttpsPort.Value, listenOptions =>
                    {
                        // �������� ����������� �� �����
                        var certificate = new X509Certificate2(configService.PfxFile, configService.PfxPassword);
                        log.Info($"�������� ���������� {certificate.Subject}");

                        var connectionOptions = new HttpsConnectionAdapterOptions()
                        {
                            ServerCertificate = certificate
                        };

                        // �������� ����������� � ���������� HTTPS
                        listenOptions.UseHttps(connectionOptions);
                        log.Info($"������ ����������� �� https/{configService.HttpsPort}");
                    });
                });
            }
            else
            {
                log.Fatal("�� ����� ���� ��� ������� �������");
                return;
            }

            // ���������, ������� ��� ����������
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            // if (app.Environment.IsDevelopment()) - ������� ���� � ������ ����������

            if (!string.IsNullOrEmpty(configService.PathBase))
            {
                app.UsePathBase(configService.PathBase);
            }

            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseHttpsRedirection();

            app.UseAuthorization();

            // �������� ������� ������������ � ��������� HTTP
            app.MapControllers();

            app.Run();
        }
    }
}