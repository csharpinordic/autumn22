﻿namespace Galaxy.Api
{
    /// <summary>
    /// Универсальный ответ на выполнение запроса
    /// </summary>
    /// <typeparam name="T">Тип данных полезной нагрузки</typeparam>
    public class Response<T> where T : new()
    {
        /// <summary>
        /// Сообщение об ошибке или null, если всё хорошо
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Признак успешного выполнения
        /// </summary>
        public bool IsSuccess => string.IsNullOrEmpty(ErrorMessage);

        /// <summary>
        /// Полезная нагрузка - данные
        /// </summary>
        public T Payload { get; set; }

        /// <summary>
        /// Конструктор без параметров
        /// </summary>
        public Response() { }

        /// <summary>
        /// Конструктор по сообщению об ошибке
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public Response(string errorMessage)
        {
            ErrorMessage = errorMessage;
            // При ошибке возвращается объект по умолчанию
            Payload = new T();
            // добавить протоколирование сообщений
        }

        /// <summary>
        /// Конструктор по исключению
        /// </summary>
        /// <param name="ex"></param>
        public Response(Exception ex) : this(ex.Message)
        {
            // добавить протоколирование исключений
        }

        /// <summary>
        /// Конструктор по полезной нагрузке
        /// </summary>
        /// <param name="payload">Полезная нагрузка</param>
        public Response(T payload)
        {
            Payload = payload;
        }
    }
}
