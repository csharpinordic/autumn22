﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;

namespace Galaxy.Api.Filters
{
    /// <summary>
    /// Обработка исключенйи в контроллерах
    /// </summary>
    public class ExceptionLoggingAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static readonly NLog.Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Обработка исключения в методе контроллера
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(ExceptionContext context)
        {
            try
            {
                Exception ex = context.Exception;
                // протоколирование исключения
                log.Error(ex);

                // мы обработали исключение
                context.ExceptionHandled = true;

                // Описание метода контроллера
                var action = (Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor;
                // Динамическое создание объекта требуемого типа 
                var result = Activator.CreateInstance(action.MethodInfo.ReturnType, new object[] { ex });

                // возвращаю HTTP 200 OK и объект как результат
                context.Result = new OkObjectResult(result);
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
            }
        }
    }
}
