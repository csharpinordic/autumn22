﻿using Microsoft.AspNetCore.Mvc.Filters;
using NLog;
using NLog.Fluent;

namespace Galaxy.Api.Filters
{
    /// <summary>
    /// Протоколирование всех действий
    /// </summary>
    public class LoggingAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static readonly NLog.Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Имя переменной маршрутизации для хранения времени начала выполнения запроса
        /// </summary>
        private const string TimeID = "time";

        /// <summary>
        /// Вызывается перед выполнением действия
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // сохранение времени начала выполнения запроса
            context.HttpContext.Request.RouteValues.Add(TimeID, DateTime.Now);
        }

        /// <summary>
        /// Вызывается после выполнения действия
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            // Информация о входящем запросе
            var request = context.HttpContext.Request;

            // Время выполнения запроса в миллисекундах
            int elasped = (int)(DateTime.Now - (DateTime)request.RouteValues[TimeID]).TotalMilliseconds;

            // Протоколирование
            // предупреждаем, если выполнение заняло больше одной секунды
            NLog.LogLevel level = (elasped > 1000) ? NLog.LogLevel.Warn : NLog.LogLevel.Trace;
            log.Log(level, $"{request.Method} {request.Path}{request.QueryString} {request.ContentType} -- {elasped} мс");
        }
    }
}
