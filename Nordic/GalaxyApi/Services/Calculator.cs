﻿namespace Galaxy.Api.Services
{
    public class Calculator : ICalculator
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public Calculator() {
            log.Trace($"Создан новый {GetType().Name}");
        }

        /// <summary>
        /// Некоторое вычисление
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compute(int a, int b)
        {
            return a * b;
        }
    }
}
