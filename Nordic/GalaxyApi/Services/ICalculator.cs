﻿namespace Galaxy.Api.Services
{
    public interface ICalculator
    {
        public int Compute(int a, int b);
    }
}
