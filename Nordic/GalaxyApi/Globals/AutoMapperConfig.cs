﻿using AutoMapper;

namespace Galaxy.Api.Globals
{
    /// <summary>
    /// Конфигурация отображения объектов
    /// </summary>
    public static class AutoMapperConfig
    {
        private static MapperConfiguration config = null;

        public static MapperConfiguration Config
        {
            get
            {
                if (config == null)
                {
                    config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Galaxy.Common.Storage.Game, Galaxy.Api.Models.Game>()
                           .ForMember(dest => dest.ServerName, opt => opt.MapFrom(src => src.Server.Name));

                        cfg.CreateMap<Galaxy.Common.Storage.GamePlayer, Galaxy.Api.Models.GamePlayer>();

                        cfg.CreateMap<Galaxy.Api.Models.Game, Galaxy.Common.Storage.Game>()
                           .ForMember(dest => dest.Server, opt => opt.Ignore()) 
                           .ForMember(dest => dest.ServerID, opt => opt.Ignore());
                    });
                }
                return config;
            }
        }
    }
}
