﻿namespace Galaxy.Api.Settings
{
    /// <summary>
    /// Настройки базы данных
    /// </summary>
    public class Database
    {
        /// <summary>
        /// Строка соединения с БД
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
