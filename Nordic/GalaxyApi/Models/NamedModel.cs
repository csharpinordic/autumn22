﻿namespace Galaxy.Api.Models
{
    /// <summary>
    /// Модель, у которой есть имя
    /// </summary>
    public abstract class NamedModel : IdModel
    {
        /// <summary>
        /// Имя сущности
        /// </summary>
        public string Name { get; set; }
    }
}
