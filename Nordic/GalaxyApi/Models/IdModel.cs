﻿namespace Galaxy.Api.Models
{
    /// <summary>
    /// Базовая модель с идентификаторов
    /// </summary>
    public class IdModel
    {
        /// <summary>
        /// Уникальный идентификатор записи
        /// </summary>
        public Guid ID { get; set; }
    }
}
