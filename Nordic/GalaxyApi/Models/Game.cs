﻿using Galaxy.Common.Storage;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Galaxy.Api.Models
{
    /// <summary>
    /// Информация об игре
    /// </summary>
    public class Game : NamedModel
    {           
        /// <summary>
        /// Префикс партии
        /// </summary>       
        public string? Prefix { get; set; }

        /// <summary>
        /// Номер партии
        /// </summary>      
        public int? Number { get; set; }      

        /// <summary>
        /// Название сервера 
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Дата начала партии
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Дата окончания партии
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Размер игрового поля
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// Количество планет
        /// </summary>
        public int? Planets { get; set; }

        /// <summary>
        /// Количество ходов в неделю (turn per week)
        /// </summary>
        public int? TPW { get; set; }
    }
}
