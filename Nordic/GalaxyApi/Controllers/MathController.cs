﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Galaxy.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MathController : ControllerBase
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private Services.ICalculator calculator;       

        public MathController(Common.Storage.Database db, Services.ICalculator calculator)
        {
            log.Trace("MathController(calculator, db)");
            this.calculator = calculator;
        }

        /// <summary>
        /// Операция над целыми числами
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [HttpGet()]
        public int Operation(int a, int b)
        {
            return calculator?.Compute(a, b) ?? 0;
        }
    }
}
