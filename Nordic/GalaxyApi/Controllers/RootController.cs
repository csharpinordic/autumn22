﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace Galaxy.Api.Controllers
{
    /// <summary>
    /// Базовый контроллер API
    /// </summary>
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Filters.ExceptionLogging]
    [Filters.Logging]
    public abstract class RootController : ControllerBase
    {
        /// <summary>
        /// Отображение объектов
        /// </summary>
        protected readonly AutoMapper.IMapper Mapper = Globals.AutoMapperConfig.Config.CreateMapper();

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        protected Common.Storage.Database db;

        /// <summary>
        /// Контроллер с базой данных
        /// </summary>
        /// <param name="db">Контекст базы данных</param>
        public RootController(Common.Storage.Database db)
        {
            this.db = db;
        }

        /// <summary>
        /// Поиск объекта по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор объекта в базе данных</param>
        /// <returns>null, если объект не найден</returns>      
        protected Response<TModel> FindByID<TEntity, TModel>(Guid id)
            where TModel : Models.IdModel, new()
            where TEntity : Common.Storage.Entity
        {
            var game = db.Set<TEntity>().Find(id);
            if (game == null)
            {
                var attr1 = typeof(TEntity).GetCustomAttribute<System.ComponentModel.DataAnnotations.DisplayAttribute>();
                var attr2 = typeof(TEntity).GetCustomAttribute<System.ComponentModel.DisplayNameAttribute>();
                var attr3 = typeof(TEntity).GetCustomAttribute<System.ComponentModel.DescriptionAttribute>();

                /*
                 * Пример развернутой записи
                string n = "";
                if (attr1 != null && attr1.Name != null)
                {
                    n = attr1.Name;
                }
                else if (attr2 != null && attr2.DisplayName != null)
                {
                    n = attr2.DisplayName;
                }
                else if (attr3 != null && attr3.Description != null)
                {
                    n = attr3.Description;
                }
                else
                {
                    n = typeof(TEntity).FullName;
                }
                */

                string name = attr1?.Name ?? attr2?.DisplayName ?? attr3?.Description ?? typeof(TEntity).FullName ?? "Объект";

                return new Response<TModel>($"{name} с идентификатором {id} не найден");
            }
            var result = Mapper.Map<TModel>(game);

            return new Api.Response<TModel>(result);
        }
    }
}
