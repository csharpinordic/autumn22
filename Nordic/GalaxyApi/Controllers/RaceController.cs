﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client.Extensions.Msal;

namespace Galaxy.Api.Controllers
{
    /// <summary>
    /// Данные об игроках
    /// </summary>  
    public class RaceController : RootController
    {
        /// <inheritdoc />
        public RaceController(Common.Storage.Database db) : base(db) { }

        /// <summary>
        /// Список всех игроков
        /// </summary>
        /// <param name="limit">Максимальное количество возвращаемых записей.
        /// Если не задано, возвращаются все записи</param>
        /// <returns></returns>    
        [HttpGet()]
        public Response<List<Models.GamePlayer>> GetAll(int? limit)
        {          
            var result = db.Players
                // (limit != null) ? limit : int.MaxValue
                .Take(limit ?? int.MaxValue)
                .OrderBy(x => x.Name)
                .Select(x => new Models.GamePlayer()
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .ToList();
            return new Api.Response<List<Models.GamePlayer>>(result);
        }

        /// <summary>
        /// Поиск игроков по фрагменту имени
        /// </summary>
        /// <param name="name">Фрагмент имени игрока</param>
        /// <returns></returns>       
        [HttpGet()]
        public Response<List<Models.GamePlayer>> FindByName(string name)
        {          
            var result = db.Players
                .Where(x => x.Name.Contains(name))
                .OrderBy(x => x.Name)
                .Select(x => new Models.GamePlayer()
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .ToList();
            return new Api.Response<List<Models.GamePlayer>>(result);
        }

        /// <summary>
        /// Поиск игрока по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>null, если игрок не найден</returns>       
        [HttpGet()]
        public Response<Models.GamePlayer> FindByID(Guid id)
            => FindByID<Common.Storage.GamePlayer, Models.GamePlayer>(id);

        /// <summary>
        /// Создание нового игрока
        /// </summary>
        /// <param name="player">Создаваемый игрок</param>
        /// <returns></returns>
        [HttpPost]
        public Response<Models.IdModel> Create(Models.GamePlayer player)
        {           
            var race = new Common.Storage.GamePlayer()
            {
                Name = player.Name
            };
            db.Players.Add(race);
            db.SaveChanges();
            return new Response<Models.IdModel>(new Models.IdModel()
            {
                ID = race.ID
            });
        }

        /// <summary>
        /// Изменение игрока
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        [HttpPut]
        public Response<Models.IdModel> Save(Models.GamePlayer player)
        {          
            var race = db.Players.Find(player.ID);
            if (race == null)
            {
                return new Response<Models.IdModel>($"Игрок с идентификатором {player.ID} не найден");
            }
            race.Name = player.Name;
            db.SaveChanges();
            return new Response<Models.IdModel>(new Models.IdModel()
            {
                ID = race.ID
            });
        }

        /// <summary>
        /// Удаление игрока из базы данных
        /// </summary>
        /// <param name="id">Идентификатор игрока</param>
        /// <returns></returns>
        [HttpDelete]
        public Response<Models.IdModel> Delete(Guid id)
        {          
            var race = db.Set<Common.Storage.GamePlayer>().Find(id);
            if (race == null)
            {
                return new Response<Models.IdModel>($"Игрок с идентификатором {id} не найден");
            }
            db.Set<Common.Storage.GamePlayer>().Remove(race);
            db.SaveChanges();
            return new Response<Models.IdModel>(new Models.IdModel()
            {
                ID = id
            });
        }
    }
}
