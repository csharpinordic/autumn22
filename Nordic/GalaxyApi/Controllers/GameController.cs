﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using System.Net;
using System.Numerics;
using System.Text;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Galaxy.Api.Controllers
{
    /// <summary>
    /// Данные об играх
    /// </summary>
    public class GameController : RootController
    {
        /// <inheritdoc />
        public GameController(Common.Storage.Database db) : base(db) { }

        /// <summary>
        /// Список всех игр
        /// </summary>
        /// <param name="limit">Максимальное количество возвращаемых записей.
        /// Если не задано, возвращаются все записи</param>
        /// <returns></returns>    
        [HttpGet()]
        public Response<List<Models.Game>> GetAll(int? limit)
        {
            var query = db.Games
                .OrderBy(x => x.Name)
                .Take(limit ?? int.MaxValue);

            var result = Mapper.Map<List<Models.Game>>(query);

            return new Api.Response<List<Models.Game>>(result);
        }

        /// <summary>
        /// Список всех игр
        /// </summary>
        /// <param name="limit">Максимальное количество возвращаемых записей.
        /// Если не задано, возвращаются все записи</param>
        /// <returns></returns>    
        [HttpGet()]
        public IActionResult GetAllAsJson(int? limit)
        {           
            var query = db.Games
                .Take(limit ?? int.MaxValue)
                .OrderBy(x => x.Name);

            // преобразование класса хранения (сущности) в класс представления (модель)
            var result = Mapper.Map<List<Models.Game>>(query);
            // сериализация в JSON
            string json = System.Text.Json.JsonSerializer.Serialize(result);
            byte[] array = Encoding.UTF8.GetBytes(json);

            return File(array, "application/vnd.ms-excel", "allgames.json");
        }

        /*
         *  using var db = new Common.Storage.Database();
            var race = db.Players.Find(id);
            if (race == null)
            {
                return new Api.Response<Models.GamePlayer>("Игрок не найден");
            };

            var result = Mapper.Map<Models.GamePlayer>(race);

            return new Api.Response<Models.GamePlayer>(result);
         */

        /// <summary>
        /// Поиск игры по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор игры</param>
        /// <returns>null, если игра не найден</returns>
        [HttpGet()]
        public Response<Models.Game> FindByID(Guid id)
            => FindByID<Common.Storage.Game, Models.Game>(id);

        /// <summary>
        /// Изменение игры
        /// </summary>
        /// <param name="model">Игры</param>
        /// <returns></returns>
        [HttpPut]
        public Response<Models.IdModel> Save(Models.Game model)
        {           
            var entity = db.Games.Find(model.ID);
            if (entity == null)
            {
                return new Response<Models.IdModel>($"Игра с идентификатором {model.ID} не найден");
            }
            entity.Name = model.Name;
            entity.Number = model.Number;
            entity.Planets = model.Planets;
            entity.EndDate = model.EndDate;
            entity.Prefix = model.Prefix;
            entity.StartDate = model.StartDate;
            entity.Size = model.Size;
            entity.TPW = model.TPW;

            db.SaveChanges();
            return new Response<Models.IdModel>(new Models.IdModel()
            {
                ID = entity.ID
            });
        }
    }
}
