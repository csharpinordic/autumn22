﻿using System.Globalization;
using System.Runtime.Intrinsics.X86;

namespace SampleApp
{
    /// <summary>
    /// Главный класс программы, который содержит точку входа
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Точка входа в программу
        /// </summary>
        /// <param name="args">Параметры программы из командной строки</param>
        static void Main(string[] args)
        {
            /*
            int a; 
            int b;
            int c;
            double b1;
            double d;

            a = 10;
            a = a + 10;
            b = a + 10;
            c = 2 * b + a + 3;
            b = c - 1;

            b1 = 0;
            b = 0;
            d = b / b1;
            int mod = 5 % 2;

            checked
            {
                byte bt = byte.MaxValue;
                bt = (byte)(bt + 1);
            }
            */

            /*

            double radius;
            Console.Write("Введите радиус > ");
            string s = Console.ReadLine();

            if (!double.TryParse(s,
                NumberStyles.Number,
                CultureInfo.InvariantCulture,
                out radius))
            {
                Console.WriteLine("Введено не число: " + s);
                // [!]
                Console.WriteLine("Следует ввести число (написать подробнее)");
                return;
            }

            double length = 2 * Math.PI * radius;
            double square1 = Math.PI * radius * radius;
            double square2 = Math.PI * Math.Pow(radius, 2);

            double n1 = double.NaN;
            double n2 = double.NaN;
            double n3 = n2 + 1;
            bool result = n1 == n2;
            Console.WriteLine("Сравнение двух чисел: " + result);
            Console.WriteLine(n3);

            Console.WriteLine("Длина окружности = " + length.ToString(System.Globalization.CultureInfo.InvariantCulture));
            Console.WriteLine("Площадь окружности = " + square1.ToString(System.Globalization.CultureInfo.InvariantCulture));
            Console.WriteLine("Площадь окружности = " + square2.ToString(System.Globalization.CultureInfo.InvariantCulture));

            */

            Console.Write("Введите числитель дроби > ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("Введите знаменатель дроби > ");
            int b = int.Parse(Console.ReadLine());

            // Сужение типа данных
            int ia = checked((int)a);

            byte[] array = BitConverter.GetBytes(b);

            if (!(b == 0) && (a / b >= 1))
            {
                Console.WriteLine("Дробь неправильная");
            }
        }
    }
}