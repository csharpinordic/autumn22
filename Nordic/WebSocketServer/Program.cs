﻿using Microsoft.AspNetCore.Server.Kestrel.Https;
using System.Net;
using System.Net.WebSockets;
using System.Security.Cryptography.X509Certificates;
using WebSocketServer.Crypto;
using WebSocketServer.Extensions;
using WebSocketServer.Models;

namespace WebSocketServer
{
    internal class Program
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private static void Notificator(ChatClient client)
        {
            try
            {
                while (true)
                {
                    client.Socket.SendString($"{DateTime.Now}");
                    // затормозить выполнение потока на 2 секунды
                    Thread.Sleep(2000);
                    // альтернативный вариант c использованием задач
                    // Task.Delay(2000).Wait();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        /// <summary>
        /// Обработчки входящего запроса
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static async Task ProcessRequest(HttpContext context)
        {
            ChatClient? client = null;
            try
            {
                if (!context.WebSockets.IsWebSocketRequest)
                {
                    // простой HTTP мы не поддерживаем
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest; // HTTP 400
                    return;
                }

                log.Trace($"Установлено соединение с адресом {context.Connection.RemoteIpAddress}");

                client = new ChatClient(context);

                var task = Task.Factory.StartNew(() => Notificator(client));

                // Цикл 
                while (true)
                {                    
                    // Входящее текстовое сообщение
                    string text = client.ReadText();
                    log.Trace($"Принят текст: {text}");

                    // Проверка на команду
                    if (text.StartsWith('!'))
                    {
                        string[] args = text.Substring(1).Split(' ');
                        string command = args[0].ToLowerInvariant();

                        switch (command)
                        {
                            case "cipher":
                                if (args.Length != 2)
                                {
                                    string message = $"Некорректная команда: {command}, правильно: !CIPHER <text>";
                                    log.Warn(message);
                                    client.Socket.SendString(message);
                                    break;
                                }
                                string enc = args[1].Encrypt();
                                log.Trace(enc);
                                client.Socket.SendString(enc);
                                break;

                            case "login":
                                if (args.Length != 3)
                                {
                                    string message = $"Некорректная команда: {command}, правильно: !LOGIN <login> <password>";
                                    log.Warn(message);
                                    client.Socket.SendString(message);
                                    break;
                                }
                                var user = client.Login(args[1], args[2]);
                                break;

                            case "register": // !register login password
                                // Пробел в имени пользоваля и пароле недопустим                               
                                if (args.Length != 3)
                                {
                                    string message = $"Некорректная команда: {command}, правильно: !REGISTER <login> <password>";
                                    log.Warn(message);
                                    client.Socket.SendString(message);
                                    break;
                                }
                                client.Register(args[1], args[2]);
                                break;

                            case "quit": // !quit
                                await client.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "End of Session", CancellationToken.None);
                                return;

                            default:
                                string msgd = $"Неизвестная команда: {command}";
                                log.Warn(msgd);
                                client.Socket.SendString(msgd);
                                break;
                        }
                    }

                    // await Task.Delay(1000);

                    foreach (var item in ChatClient.Clients) // .Where(x => x != webSocket))
                    {
                        item.Socket.SendString($"{context.Connection.RemoteIpAddress}: {text}");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // трассировка списка пользователей
                using (var db = new Storage.DB())
                {
                    foreach (var user in db.Users)
                    {
                        log.Trace(user);
                    }
                }

                // Настройки веб-сервиса
                var configService = Settings.ConfigurationManager.Configuration.GetSection(nameof(Settings.Service)).Get<Settings.Service>();

                var builder = WebApplication.CreateBuilder(args);

                if (configService.HttpPort.HasValue)
                {
                    builder.WebHost.UseUrls($"http://*:{configService.HttpPort}");
                    log.Info($"Сервис запускается на http/{configService.HttpPort}");
                }
                else if (configService.HttpsPort.HasValue && !string.IsNullOrEmpty(configService.PfxFile))
                {
                    builder.WebHost.UseKestrel(options =>
                    {
                        options.ListenAnyIP(configService.HttpsPort.Value, listenOptions =>
                        {
                            // Загрузка сертификата из файла
                            var certificate = new X509Certificate2(configService.PfxFile, configService.PfxPassword);
                            log.Info($"Загружен сертификат {certificate.Subject}");

                            var connectionOptions = new HttpsConnectionAdapterOptions()
                            {
                                ServerCertificate = certificate
                            };

                            // Привязка сертификата к соединению HTTPS
                            listenOptions.UseHttps(connectionOptions);
                            log.Info($"Сервис запускается на https/{configService.HttpsPort}");
                        });
                    });
                }
                else
                {
                    log.Fatal("Не задан порт для запуска сервиса");
                    return;
                }

                var app = builder.Build();
                WebSocketOptions options = new WebSocketOptions()
                {

                };
                app.UseWebSockets();
                app.Map("/", ProcessRequest);
                app.Run();
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
            }
        }
    }
}