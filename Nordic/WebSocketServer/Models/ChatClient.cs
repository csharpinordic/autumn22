﻿using NLog.Fluent;
using System.Net.WebSockets;
using WebSocketServer.Crypto;
using WebSocketServer.Extensions;

namespace WebSocketServer.Models
{
    /// <summary>
    /// Клиент чат-сервера
    /// </summary>
    public class ChatClient 
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Клиент WebSocket
        /// </summary>
        private WebSocket webSocket;
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private Storage.DB db;
        /// <summary>
        /// Список подключённых клиентов
        /// </summary>
        private static readonly List<ChatClient> clients = new();

        /// <summary>
        /// Клиент Web Socket
        /// </summary>
        public WebSocket Socket => webSocket;

        /// <summary>
        /// Список подключённых клиентов
        /// </summary>
        public static IEnumerable<ChatClient> Clients => clients;

        /// <summary>
        /// Конструктор по контексту
        /// </summary>
        /// <param name="context"></param>
        public ChatClient(HttpContext context)
        {
            webSocket = context.WebSockets.AcceptWebSocketAsync().Result;
            db = new();
            lock (clients)
            {
                clients.Add(this);
            }
        }

        /// <summary>
        /// Деструктор класса
        /// </summary>
        ~ChatClient()
        {
            lock (clients)
            {
                clients.Remove(this);
            }
        }

        /// <summary>
        /// Чтение входящего текстового сообщения
        /// </summary>
        /// <returns></returns>
        public string ReadText()
        {
            var buffer = new byte[1024];
            WebSocketReceiveResult received = webSocket.ReceiveAsync(buffer, CancellationToken.None).Result;

            // Входящее текстовое сообщение
            string text = System.Text.Encoding.UTF8.GetString(buffer, 0, received.Count);
            return text;
        }

        /// <summary>
        /// Регистрация пользователя в базе данных
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public async void Register(string login, string password)
        {
            // Проверка на наличие такого пользователя
            if (db.Users.Any(x => x.Login == login))
            {
                string message = $"Пользователь {login} уже зарегистрирован, для входа используйте команду !LOGIN <login> <password>";
                log.Warn(message);
                webSocket.SendString(message);
                return;
            }

            var user = new Storage.User()
            {
                Login = login,
                Password = password.Encrypt()
            };
            db.Users.Add(user);
            await db.SaveChangesAsync();
        }

        /// <summary>
        /// Вход пользователя в систему
        /// </summary>
        /// <param name="login">Имя пользователя</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        public Storage.User Login(string login, string password)
        {
            var user = db.Users.Find(login);
            if (user == null)
            {
                // [i] возможно, излишняя информация с точки зрения безопасности
                string message = $"Пользователь {login} не зарегистрирован, для регистрации используйте команду !REGSTER <login> <password>";
                log.Warn(message);
                webSocket.SendString(message);
                return null;
            }

            if (user.Password != password.Encrypt())
            {
                // [i] возможно, излишняя информация с точки зрения безопасности
                string message = $"Пароль для пользователя {login} введён некорректно";
                log.Warn(message);
                webSocket.SendString(message);
                return null;
            }

            return user;
        }
    }
}
