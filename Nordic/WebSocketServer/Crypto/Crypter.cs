﻿using System.Security.Cryptography;
using static System.Net.Mime.MediaTypeNames;

namespace WebSocketServer.Crypto
{
    /// <summary>
    /// Класс для шифрования
    /// </summary>
    public static class Crypter
    {
        /// <summary>
        /// Протоколирование
        /// </summary>
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Шифровальщик
        /// </summary>
        private static Aes aes;

        /// <summary>
        /// Инициализация механизма шифрования
        /// </summary>
        static Crypter()
        {
            try
            {
                aes = Aes.Create();
                byte[] key = System.Text.Encoding.UTF8.GetBytes("БольшойСекретДля");
                aes.Key = key;
                aes.IV = System.Text.Encoding.UTF8.GetBytes("ОдинВект");
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
            }
        }

        /// <summary>
        /// Шифрование текста
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(this string text)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(text);
            using var ms = new MemoryStream();
            using (var crStream = new CryptoStream(ms, aes.CreateEncryptor(aes.Key, aes.IV), CryptoStreamMode.Write))
            {
                crStream.Write(data);
            }
            string encrypted = string.Join("", ms.ToArray().Select(x => $"{x:x}"));
            return encrypted;
        }

        /// <summary>
        /// Расшифровка текста
        /// </summary>
        /// <param name="encrypted"></param>
        /// <returns></returns>
        public static string Decrypt(this byte[] encrypted)
        {
            using var ms = new MemoryStream();
            using (var crStream = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, aes.IV), CryptoStreamMode.Write))
            {
                crStream.Write(encrypted);
            }
            byte[] a = ms.ToArray();
            string decrypted = System.Text.Encoding.UTF8.GetString(a);
            return decrypted;
        }
    }
}
