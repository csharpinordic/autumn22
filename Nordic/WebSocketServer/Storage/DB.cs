﻿using Microsoft.EntityFrameworkCore;

namespace WebSocketServer.Storage
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class DB : DbContext
    {
        /// <summary>
        /// Список пользователей
        /// </summary>
        public virtual DbSet<User> Users { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public DB()
        {
            Database.Migrate();
        }

        /// <summary>
        /// Настройка контекста базы данных
        /// </summary>
        /// <param name="options"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string name = System.IO.Path.Combine(AppContext.BaseDirectory, "db.dat");
            options.UseSqlite($"data source={name}");
        }
    }
}
