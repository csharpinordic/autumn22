﻿using System.ComponentModel.DataAnnotations;

namespace WebSocketServer.Storage
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Регистрационное имя пользователя
        /// <para>Не может содержать пробел</para>
        /// </summary>
        [Key()]
        [MaxLength(255)]
        public string Login { get; set; }

        /// <summary>
        /// Пароль - временно в открытой форме
        /// <para>Не может содержать пробел</para>
        /// </summary>
        [MaxLength(255)]
        public string Password { get; set; }

        /// <summary>
        /// Признак активности пользователя
        /// </summary>
        public bool Enabled { get; set; }

        public override string ToString() => $"{Login}: {Password}";
    }
}