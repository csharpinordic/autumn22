﻿namespace WebSocketServer.Settings
{
    /// <summary>
    /// Настройки веб-сервиса
    /// </summary>
    public class Service
    {
        /// <summary>
        /// TCP-порт для HTTP
        /// </summary>
        public int? HttpPort { get; set; }
        /// <summary>
        /// TCP-порт для HTTPS
        /// </summary>
        public int? HttpsPort { get; set; }
        /// <summary>
        /// Полный путь к файлу SSL-сертификата в формате PFX
        /// </summary>
        public string? PfxFile { get; set; }
        /// <summary>
        /// Пароль сертификата
        /// </summary>
        public string? PfxPassword { get; set; }
        /// <summary>
        /// Корневой путь веб-сервиса
        /// </summary>
        public string PathBase { get; set; }
    }
}
