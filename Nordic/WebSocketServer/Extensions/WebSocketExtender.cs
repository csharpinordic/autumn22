﻿using System.Net.WebSockets;

namespace WebSocketServer.Extensions
{
    public static class WebSocketExtender
    {
        /// <summary>
        /// Отправка текстового сообщения через WebSocket
        /// </summary>
        /// <param name="client">Клиент</param>
        /// <param name="message">Текст сообщения</param>
        public static async void SendString(this WebSocket client, string message)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
            if (client.State == WebSocketState.Open)
            {
                await client.SendAsync(data, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }
    }
}
