﻿using System.Drawing;

namespace NanoTetris;

class Program
{
    /// <summary>
    /// Ширина стаканчика, не включая стенки
    /// </summary>
    const int Width = 8;
    /// <summary>
    /// Высота стаканчика, не включая стенки
    /// </summary>
    const int Height = 8;
    /// <summary>
    /// Символ для стенок
    /// </summary>
    const char Wall = 'Ш';
    /// <summary>
    /// Символ для кубика
    /// </summary>
    const char Cube = 'X';

    /// <summary>
    /// Генератор случайных чисел
    /// </summary>
    static Random random = new Random();

    static int x0;
    static int y0;
    static ConsoleColor color;
    /// <summary>
    /// Изначальный цвет фона консоли
    /// </summary>
    static ConsoleColor back;
    static ConsoleColor[,] field;

    static void Move(int x, int y)
    {
        // Проверка входных данных на корректность
        if ((x < 0) || (x >= Width) || (y < 0) || (y >= Height) || IsFilled(x, y))
        {
            Console.Beep();
        }
        else
        {
            Console.CursorLeft = x0 + 1;
            Console.CursorTop = y0;
            Console.Write(" ");
            x0 = x;
            y0 = y;
            Console.CursorLeft = x0 + 1;
            Console.CursorTop = y0;
            Console.BackgroundColor = color;
            Console.Write(Cube);
            Console.ResetColor();
        }
    }

    /// <summary>
    /// Создание нового кубика
    /// </summary>
    static void NewCube()
    {
        x0 = random.Next(0, Width);
        y0 = 0;
        color = (ConsoleColor)random.Next(1, 15);

        // Вывод кубика
        Console.CursorLeft = x0 + 1;
        Console.CursorTop = y0;
        Console.BackgroundColor = color;
        Console.Write(Cube);
        Console.ResetColor();
    }

    /// <summary>
    /// Занята ли клетка кубиком
    /// </summary>
    /// <param name="x">Абсцисса от 0 до <seealso cref="Width"/>-1</param>
    /// <param name="y">Ордината от 0 до <seealso cref="Height"/>-1</param>
    /// <returns></returns>
    static bool IsFilled(int x, int y)
    {
        return field[x, y] != back;
    }

    /// <summary>
    /// Главная функция
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
        string message = null;
        try
        {
            Console.Clear();
            Console.ResetColor();

            back = Console.BackgroundColor;

            // Массив игрового поля
            field = new ConsoleColor[Width, Height];
            // Инициализация текущим цветом консоли
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    field[x, y] = back;
                }
            }

            // Формирование стенок
            for (int y = 0; y < Height; y++)
            {
                Console.Write(Wall);
                for (int x = 0; x < Width; x++)
                {
                    Console.Write(" ");
                }
                Console.WriteLine(Wall);
            }
            // Формирование донышка
            for (int x = 0; x < Width + 2; x++)
            {
                Console.Write(Wall);
            }
            Console.WriteLine();

            // Формирование нового кубика
            NewCube();

            ConsoleKey key;
            do
            {
                // Ожидать нажатия клавиши, но не отображать нажимаемые клавиши
                key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.LeftArrow:
                        Move(x0 - 1, y0);
                        break;

                    case ConsoleKey.RightArrow:
                        Move(x0 + 1, y0);
                        break;

                    case ConsoleKey.DownArrow:
                        Move(x0, y0 + 1);
                        break;

                    case ConsoleKey.Escape:
                        // чтобы не пищало при нажатии Esc перед завершением цикла
                        break;

                    default: // если нажата некорректная кнопка, будем просто пищать
                        Console.Beep();
                        break;
                }

                // Проверка на то, что кубик достиг дна или других кубиков
                if ((y0 == Height - 1) || IsFilled(x0, y0 + 1))
                {
                    // запоминаем упавший кубик на поле
                    field[x0, y0] = color;

                    // Проверка на заполнение стакана полностью
                    if (y0 == 0)
                    {
                        break;
                    }

                    NewCube();
                }

                // Проверка на заполненную строку
                bool filled = true;
                for (int x = 0; x < Width; x++)
                {
                    if (field[x, Height - 1] == Console.BackgroundColor)
                    {
                        filled = false;
                        break;
                    }
                }
                if (filled)
                {
                    // Сдвиг массива field на строку ниже
                    for (int y = Height - 2; y >= 0; y--)
                    {
                        for (int x = 0; x < Width; x++)
                        {
                            field[x, y + 1] = field[x, y];
                        }
                    }
                    // Очистка первой строчки поля
                    for (int x = 0; x < Width; x++)
                    {
                        field[x, 0] = back;
                    }

                    // Обновить экран
                    for (int y = 0; y < Height; y++)
                    {
                        for (int x = 0; x < Width; x++)
                        {
                            Console.CursorLeft = x + 1;
                            Console.CursorTop = y;
                            Console.BackgroundColor = field[x, y];
                            Console.Write(IsFilled(x, y) ? Cube : " ");
                            Console.ResetColor();
                        }
                    }
                }
            }
            while (key != ConsoleKey.Escape);
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            Console.CursorLeft = 0;
            Console.CursorTop = Height + 2;
            Console.WriteLine(message);
            Console.WriteLine("Игра окончена");
        }
    }
}
