﻿using System.Globalization;

namespace Samples
{
    internal class Program
    {
        /// <summary>
        /// Преобразование числа в строку и обратно для разных региональных настроек
        /// </summary>
        static void Sample17()
        {
            var cultures = new CultureInfo[]
            {
                CultureInfo.GetCultureInfo("en-US"),
                CultureInfo.GetCultureInfo("ru-RU") ,
                CultureInfo.GetCultureInfo("de-DE"),
                CultureInfo.InvariantCulture
            };

            double n = 1234.56;
            foreach (var culture in cultures)
            {
                Console.WriteLine($"{culture.DisplayName}: {n.ToString("n", culture)}");
            }
            Console.WriteLine();

            string[] strings = new string[] { "1234.56", "1,234.56", "1 234,56", "1.234,56", "1234,56", "1234 56" };
            foreach (var s in strings)
            {
                Console.WriteLine($"Преобразование строки '{s}':");
                foreach (var culture in cultures)
                {
                    if (double.TryParse(s, NumberStyles.Number, culture, out double d))
                    {
                        Console.WriteLine($"{culture.DisplayName}: {d.ToString(CultureInfo.InvariantCulture)}");
                    }
                    else
                    {
                        Console.WriteLine($"{culture.DisplayName}: ошибка преобразования");
                    }
                }
                Console.WriteLine();
            }
        }

        static void Sample17a()
        {
            double n = 1234.56;
            string s = n.ToString(CultureInfo.InvariantCulture);
            s = n.ToString(CultureInfo.CurrentCulture); // по умолчанию
            s = n.ToString(CultureInfo.GetCultureInfo("en-US"));
        }

        static void Sample17b()
        {
            string s = "1234.56";
            double d = double.Parse(s); // используется CurrentCulture
            d = double.Parse(s, CultureInfo.InvariantCulture);
            bool b = double.TryParse(s, NumberStyles.Number, 
                CultureInfo.GetCultureInfo("en-US"), out d);
        }

        static void Main()
        {
            double n = 1234.56;
            string s = n.ToString(CultureInfo.InvariantCulture);
            s = n.ToString(CultureInfo.CurrentCulture); // по умолчанию
            s = n.ToString(CultureInfo.GetCultureInfo("en-US"));

            

            Sample17();
        }
    }
}