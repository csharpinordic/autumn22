using System;
using System.Security.Cryptography.Xml;

namespace Lissajous
{
    public partial class Form1 : Form
    {
        double a;
        double b;
        double alpha;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("����������?", "���������", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    a = double.Parse(textX.Text);
                    b = double.Parse(textY.Text);
                    alpha = double.Parse(textAlpha.Text) * Math.PI / 180;
                    // ��� ��� ���� ��������
                    panel.Invalidate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        /// <summary>
        /// �������� ������������� ����� �� ������������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void text_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // ����� ���� ��������� ���������� ��� �������������� �����
                double d;
                TextBox text = (TextBox)sender;
                bool valid = double.TryParse(text.Text, out d);
                text.BackColor = valid ? Color.White : Color.MistyRose;
                if (text.Tag == "track")
                {
                    track.Value = (int)d;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Pen pen1 = new Pen(Color.Black, 3);
            Pen pen2 = new Pen(Color.Red, 3);

            // ������������ ���
            e.Graphics.DrawLine(pen1, 0, panel.Height / 2, panel.Width, panel.Height / 2);
            e.Graphics.DrawLine(pen1, panel.Width / 2, 0, panel.Width / 2, panel.Height);

            int n = 250; // [?] ����� �� ��������� ���������� ����� ������ 
            float x1 = (float)(Math.Sin(alpha) + 1) * panel.Width / 2;
            float y1 = (float)(Math.Sin(0) + 1) * panel.Height / 2;
            for (double t = 0; t < 2 * Math.PI; t += 2 * Math.PI / n)
            {
                // [-1...1] -> [0..2] -> [0...Width/Height]
                float x2 = (float)(Math.Sin(a * t + alpha) + 1) * panel.Width / 2;
                float y2 = (float)(Math.Sin(b * t) + 1) * panel.Height / 2;
                e.Graphics.DrawLine(pen2, x1, y1, x2, y2);
                x1 = x2;
                y1 = y2;
            }
        }

        /// <summary>
        /// ��������� ������� �������� ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            panel.Invalidate();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textAlpha.Text = track.Value.ToString();
            alpha = track.Value * Math.PI / 180;
            panel.Invalidate();
        }

        /// <summary>
        /// ����� �� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s = $"{textX.Text};{textY.Text};{textAlpha.Text}";
            Clipboard.SetText(s);
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                string s = Clipboard.GetText();
                string[] a = s.Split(";");
                if (a.Count() == 3)
                {
                    textX.Text = a[0];
                    textY.Text = a[1];
                    textAlpha.Text = a[2];
                }
            }
        }

        private void text_Enter(object sender, EventArgs e)
        {
            TextBox text = (TextBox)sender;
            text.SelectAll();
        }
    }
}