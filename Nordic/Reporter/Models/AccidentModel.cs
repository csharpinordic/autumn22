﻿namespace Reporter.Models
{
    public class AccidentModel
    {
        public DateTime StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public string AccidentType { get; set; }
        public int ButtonNumber { get; set; }
        public int SideNumber { get; set; }
        public int PostNumber { get; set; }
    }
}
