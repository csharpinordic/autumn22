﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Reporter.Models;
using Reporter.Storage;

namespace Reporter.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccidentController : ControllerBase
    {
        private readonly DB db;

        public AccidentController(DB context)
        {
            db = context;
        }

        /// <summary>
        /// Список происшествий в заданном диапазоне дат
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="finishTime"></param>
        /// <returns></returns>
        [HttpGet]
        // http://localhost:5276/api/Accident/GetAccidents?startTime=2021-01-01&finishTime=2021-02-01
        public async Task<ActionResult<IEnumerable<Models.AccidentModel>>> GetAccidents(DateTime startTime, DateTime finishTime)
        {
            // SELECT accident FROM db.Accidents WHERE ...

            // вариант с синтаксисом LINQ
            var q1 = from accident in db.Accidents
                     join atype in db.AccidentClasses on accident.AtypeId equals atype.Id
                     join bevent in db.ButtonEvents on accident.BeventId equals bevent.Id
                     join button in db.Buttons on bevent.Number equals button.Number
                     where
                         accident.StartTime >= startTime
                         && accident.EndTime <= finishTime
                     select new AccidentModel()
                     {
                         StartTime = accident.StartTime,
                         FinishTime = accident.EndTime,
                         AccidentType = atype.Name,
                         ButtonNumber = button.Number,
                         SideNumber = button.Side,
                         PostNumber = button.Post
                     };

            // вариант с синтаксисом Fluent
            /*
            var q2 = db.Accidents
                .Where(accident => startTime <= accident.StartTime && accident.EndTime <= finishTime)
                .Join(db.AccidentClasses, accident => accident.Atype, atype => atype.Id, (accident, atype) => new AccidentModel()
                {
                    StartTime = accident.StartTime,
                    FinishTime = accident.EndTime,
                    AccidentType = atype.Name,
                })
               ;

            */

            var list = q1.ToList();

            return Ok(list);
        }

        // GET: api/Accident/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Accident>> GetAccident(Guid id)
        {
            var accident = await db.Accidents.FindAsync(id);

            if (accident == null)
            {
                return NotFound();
            }

            return accident;
        }

        // PUT: api/Accident/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccident(Guid id, Accident accident)
        {
            if (id != accident.Id)
            {
                return BadRequest();
            }

            db.Entry(accident).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccidentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accident
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Accident>> PostAccident(Accident accident)
        {
            db.Accidents.Add(accident);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AccidentExists(accident.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAccident", new { id = accident.Id }, accident);
        }

        // DELETE: api/Accident/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccident(Guid id)
        {
            var accident = await db.Accidents.FindAsync(id);
            if (accident == null)
            {
                return NotFound();
            }

            db.Accidents.Remove(accident);
            await db.SaveChangesAsync();

            return NoContent();
        }

        private bool AccidentExists(Guid id)
        {
            return db.Accidents.Any(e => e.Id == id);
        }
    }
}
