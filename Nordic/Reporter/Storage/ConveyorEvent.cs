﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class ConveyorEvent
{
    public Guid Id { get; set; }

    public int Sensors { get; set; }

    public int State { get; set; }

    public int Number { get; set; }

    public DateTime TimeStamp { get; set; }
}
