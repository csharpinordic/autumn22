﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class AccessRole
{
    public Guid Id { get; set; }

    public int Number { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;
}
