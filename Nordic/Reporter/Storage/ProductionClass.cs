﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class ProductionClass
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Picture { get; set; } = null!;

    public virtual ICollection<Plan> Plans { get; } = new List<Plan>();
}
