﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class UserAccess
{
    public Guid Id { get; set; }

    public Guid UserId { get; set; }

    public int Role { get; set; }

    public string Code { get; set; } = null!;

    public bool Allow { get; set; }

    public virtual User User { get; set; } = null!;
}
