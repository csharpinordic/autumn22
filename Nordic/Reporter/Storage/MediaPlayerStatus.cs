﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class MediaPlayerStatus
{
    public Guid Id { get; set; }

    public int State { get; set; }

    public DateTime TimeStamp { get; set; }

    public Guid PlayerId { get; set; }

    public int Status { get; set; }

    public string UserName { get; set; } = null!;

    public int Ipstate { get; set; }

    public virtual MediaPlayer Player { get; set; } = null!;
}
