﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Report
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;
}
