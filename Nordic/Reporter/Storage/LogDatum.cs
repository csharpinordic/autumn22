﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class LogDatum
{
    public Guid Id { get; set; }

    public Guid UserLogId { get; set; }

    public string TableName { get; set; } = null!;

    public Guid RecordId { get; set; }

    public string Object { get; set; } = null!;

    public string ColumnName { get; set; } = null!;

    public string? OldValue { get; set; }

    public string? NewValue { get; set; }

    public virtual UserLog UserLog { get; set; } = null!;
}
