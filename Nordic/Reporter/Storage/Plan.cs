﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Plan
{
    public Guid Id { get; set; }

    public int Ptype { get; set; }

    public DateTime TimeStamp { get; set; }

    public int PlanCount { get; set; }

    public int FactCount { get; set; }

    public bool Started { get; set; }

    public bool Finished { get; set; }

    public DateTime Changed { get; set; }

    public string User { get; set; } = null!;

    public Guid ProductionClassId { get; set; }

    public virtual ProductionClass ProductionClass { get; set; } = null!;
}
