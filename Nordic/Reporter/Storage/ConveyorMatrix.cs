﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class ConveyorMatrix
{
    public Guid Id { get; set; }

    public int Sensors { get; set; }

    public int State { get; set; }

    public int NextState { get; set; }
}
