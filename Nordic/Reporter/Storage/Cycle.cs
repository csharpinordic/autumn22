﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Cycle
{
    public Guid Id { get; set; }

    public int Number { get; set; }

    public int Support { get; set; }

    public string? Vin { get; set; }

    public int Ttype { get; set; }

    public string UserName { get; set; } = null!;

    public DateTime TimeStamp { get; set; }

    public DateTime? EndOfCycle { get; set; }

    public int PlanCount { get; set; }
}
