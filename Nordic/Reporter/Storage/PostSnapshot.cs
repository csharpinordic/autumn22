﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class PostSnapshot
{
    public Guid Id { get; set; }

    public int Post { get; set; }

    public DateTime TimeStamp { get; set; }

    public int Support { get; set; }

    public string? Vin { get; set; }

    public int Ttype { get; set; }

    public Guid? AtypeId { get; set; }

    public virtual AccidentClass? Atype { get; set; }

    public virtual ICollection<ButtonSnapshot> ButtonSnapshots { get; } = new List<ButtonSnapshot>();
}
