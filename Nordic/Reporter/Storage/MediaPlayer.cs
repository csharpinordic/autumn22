﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class MediaPlayer
{
    public Guid Id { get; set; }

    public string Ip { get; set; } = null!;

    public int Port { get; set; }

    public string? Comment { get; set; }

    public string Name { get; set; } = null!;

    public int CurrentState { get; set; }

    public int CurrentIpstate { get; set; }

    public virtual ICollection<MediaPlayerStatus> MediaPlayerStatuses { get; } = new List<MediaPlayerStatus>();
}
