﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class ButtonSnapshot
{
    public Guid Id { get; set; }

    public int Btype { get; set; }

    public int Side { get; set; }

    public DateTime TimeStamp { get; set; }

    public Guid? PostId { get; set; }

    public int Counter { get; set; }

    public virtual PostSnapshot? Post { get; set; }
}
