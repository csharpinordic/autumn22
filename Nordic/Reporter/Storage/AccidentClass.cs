﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class AccidentClass
{
    public Guid Id { get; set; }

    public int Number { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<Accident> Accidents { get; } = new List<Accident>();

    public virtual ICollection<PostSnapshot> PostSnapshots { get; } = new List<PostSnapshot>();
}
