﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class User
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public int AccessList { get; set; }

    public virtual ICollection<UserAccess> UserAccesses { get; } = new List<UserAccess>();
}
