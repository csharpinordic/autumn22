﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class UserLog
{
    public Guid Id { get; set; }

    public string Action { get; set; } = null!;

    public string UserName { get; set; } = null!;

    public DateTime TimeStamp { get; set; }

    public string Controller { get; set; } = null!;

    public virtual ICollection<LogDatum> LogData { get; } = new List<LogDatum>();
}
