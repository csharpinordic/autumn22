﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Setting
{
    public Guid Id { get; set; }

    public TimeSpan CycleDuration { get; set; }

    public long ReaderTimerInterval { get; set; }

    public string? ModbusIp { get; set; }

    public int ModbusPort { get; set; }

    public byte ModbusDeviceId { get; set; }

    public string UserName { get; set; } = null!;

    public DateTime TimeStamp { get; set; }

    public long UdpSenderInterval { get; set; }

    public TimeSpan? StartTime { get; set; }

    public TimeSpan? FinishTime { get; set; }

    public int MediaPlayerTimeout { get; set; }

    public int UdpCacheTimeout { get; set; }

    public long DeviceMask { get; set; }

    public int SupportCount { get; set; }

    public int ModbusTimeout { get; set; }

    public int ModbusThreshold { get; set; }

    public int ConveyorTimeout { get; set; }

    public TimeSpan ConveyorTime { get; set; }

    public TimeSpan LunchStart { get; set; }

    public TimeSpan LunchFinish { get; set; }
}
