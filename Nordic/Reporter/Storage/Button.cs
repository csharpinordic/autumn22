﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Button
{
    public Guid Id { get; set; }

    public int Number { get; set; }

    public int Post { get; set; }

    public int Side { get; set; }
}
