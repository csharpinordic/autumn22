﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class UdpVariable
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public string? Source { get; set; }

    public string? Format { get; set; }

    public string? Value { get; set; }

    public DateTime TimeStamp { get; set; }

    public bool? Enabled { get; set; }
}
