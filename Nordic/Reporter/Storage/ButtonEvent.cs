﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class ButtonEvent
{
    public Guid Id { get; set; }

    public int Btype { get; set; }

    public DateTime TimeStamp { get; set; }

    public int Number { get; set; }

    public int Counter1 { get; set; }

    public int Counter2 { get; set; }

    public int Counter3 { get; set; }

    public virtual ICollection<Accident> Accidents { get; } = new List<Accident>();
}
