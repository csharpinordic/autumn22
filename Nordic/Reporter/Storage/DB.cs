﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Reporter.Storage;

/// <summary>
/// Контекст базы данных
/// </summary>
/// Для автоматической генерации использовалась команда:
/// Scaffold-DbContext -Connection "Data source=.\SQLEXPRESS;Initial Catalog=PTZ_DEMO;Integrated Security=true;Encrypt=false;" -Provider "Microsoft.EntityFrameworkCore.SqlServer"  -OutputDir "Storage"
public partial class DB : DbContext
{
    public DB()
    {
    }

    public DB(DbContextOptions<DB> options)
        : base(options)
    {
    }

    public virtual DbSet<AccessRole> AccessRoles { get; set; }

    public virtual DbSet<Accident> Accidents { get; set; }

    public virtual DbSet<AccidentClass> AccidentClasses { get; set; }

    public virtual DbSet<Button> Buttons { get; set; }

    public virtual DbSet<ButtonEvent> ButtonEvents { get; set; }

    public virtual DbSet<ButtonSnapshot> ButtonSnapshots { get; set; }

    public virtual DbSet<CommandMessage> CommandMessages { get; set; }

    public virtual DbSet<ConveyorEvent> ConveyorEvents { get; set; }

    public virtual DbSet<ConveyorMatrix> ConveyorMatrices { get; set; }

    public virtual DbSet<Cycle> Cycles { get; set; }

    public virtual DbSet<LogDatum> LogData { get; set; }

    public virtual DbSet<MediaPlayer> MediaPlayers { get; set; }

    public virtual DbSet<MediaPlayerStatus> MediaPlayerStatuses { get; set; }

    public virtual DbSet<MigrationHistory> MigrationHistories { get; set; }

    public virtual DbSet<Plan> Plans { get; set; }

    public virtual DbSet<PostSnapshot> PostSnapshots { get; set; }

    public virtual DbSet<ProductionClass> ProductionClasses { get; set; }

    public virtual DbSet<Report> Reports { get; set; }

    public virtual DbSet<Setting> Settings { get; set; }

    public virtual DbSet<UdpVariable> UdpVariables { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<UserAccess> UserAccesses { get; set; }

    public virtual DbSet<UserLog> UserLogs { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data source=.\\SQLEXPRESS; Initial catalog=PTZ_DEMO;integrated security=true;Encrypt=false;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AccessRole>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.AccessRoles");

            entity.HasIndex(e => e.Number, "IX_ACCESSROLE_NUMBER").IsUnique();

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(63);
            entity.Property(e => e.Name).HasMaxLength(255);
        });

        modelBuilder.Entity<Accident>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Accidents");

            entity.HasIndex(e => e.AtypeId, "IX_AType_ID");

            entity.HasIndex(e => e.BeventId, "IX_BEvent_ID");

            entity.HasIndex(e => e.NextVersionId, "IX_NextVersion_ID");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.AtypeId).HasColumnName("AType_ID");
            entity.Property(e => e.BeventId).HasColumnName("BEvent_ID");
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.EndTime).HasColumnType("datetime");
            entity.Property(e => e.NextVersionId).HasColumnName("NextVersion_ID");
            entity.Property(e => e.Post).HasMaxLength(10);
            entity.Property(e => e.StartTime).HasColumnType("datetime");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.UserName).HasMaxLength(255);

            entity.HasOne(d => d.Atype).WithMany(p => p.Accidents)
                .HasForeignKey(d => d.AtypeId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_dbo.Accidents_dbo.AccidentClasses_AType_ID");

            entity.HasOne(d => d.Bevent).WithMany(p => p.Accidents)
                .HasForeignKey(d => d.BeventId)
                .HasConstraintName("FK_dbo.Accidents_dbo.ButtonEvents_BEvent_ID");

            entity.HasOne(d => d.NextVersion).WithMany(p => p.InverseNextVersion)
                .HasForeignKey(d => d.NextVersionId)
                .HasConstraintName("FK_dbo.Accidents_dbo.Accidents_NextVersion_ID");
        });

        modelBuilder.Entity<AccidentClass>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.AccidentClasses");

            entity.HasIndex(e => e.Number, "IX_NUMBER").IsUnique();

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Name).HasMaxLength(63);
        });

        modelBuilder.Entity<Button>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Buttons");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
        });

        modelBuilder.Entity<ButtonEvent>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.ButtonEvents");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Btype).HasColumnName("BType");
            entity.Property(e => e.Counter1).HasDefaultValueSql("((-1))");
            entity.Property(e => e.Counter2).HasDefaultValueSql("((-1))");
            entity.Property(e => e.Counter3).HasDefaultValueSql("((-1))");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
        });

        modelBuilder.Entity<ButtonSnapshot>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.ButtonSnapshots");

            entity.HasIndex(e => e.PostId, "IX_Post_ID");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Btype).HasColumnName("BType");
            entity.Property(e => e.PostId).HasColumnName("Post_ID");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");

            entity.HasOne(d => d.Post).WithMany(p => p.ButtonSnapshots)
                .HasForeignKey(d => d.PostId)
                .HasConstraintName("FK_dbo.ButtonSnapshops_dbo.PostSnapshots_Post_ID");
        });

        modelBuilder.Entity<CommandMessage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.CommandMessages");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Ctype).HasColumnName("CType");
            entity.Property(e => e.Text).HasMaxLength(255);
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.UserName).HasMaxLength(255);
        });

        modelBuilder.Entity<ConveyorEvent>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.ConveyorEvents");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
        });

        modelBuilder.Entity<ConveyorMatrix>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.ConveyorMatrices");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
        });

        modelBuilder.Entity<Cycle>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Cycles");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.EndOfCycle).HasColumnType("datetime");
            entity.Property(e => e.PlanCount).HasDefaultValueSql("((1))");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.Ttype).HasColumnName("TType");
            entity.Property(e => e.UserName).HasMaxLength(255);
            entity.Property(e => e.Vin)
                .HasMaxLength(17)
                .HasColumnName("VIN");
        });

        modelBuilder.Entity<LogDatum>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.LogData");

            entity.HasIndex(e => e.UserLogId, "IX_UserLog_ID");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.ColumnName).HasMaxLength(63);
            entity.Property(e => e.NewValue).HasMaxLength(255);
            entity.Property(e => e.Object).HasMaxLength(255);
            entity.Property(e => e.OldValue).HasMaxLength(255);
            entity.Property(e => e.RecordId).HasColumnName("Record_ID");
            entity.Property(e => e.TableName).HasMaxLength(63);
            entity.Property(e => e.UserLogId).HasColumnName("UserLog_ID");

            entity.HasOne(d => d.UserLog).WithMany(p => p.LogData)
                .HasForeignKey(d => d.UserLogId)
                .HasConstraintName("FK_dbo.LogData_dbo.UserLogs_UserLog_ID");
        });

        modelBuilder.Entity<MediaPlayer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.MediaPlayers");

            entity.HasIndex(e => e.Ip, "IX_IP").IsUnique();

            entity.HasIndex(e => e.Name, "IX_NAME").IsUnique();

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Comment).HasMaxLength(255);
            entity.Property(e => e.CurrentIpstate).HasColumnName("CurrentIPState");
            entity.Property(e => e.Ip)
                .HasMaxLength(15)
                .HasColumnName("IP");
            entity.Property(e => e.Name)
                .HasMaxLength(63)
                .HasDefaultValueSql("(newid())");
        });

        modelBuilder.Entity<MediaPlayerStatus>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.MediaPlayerStatus");

            entity.ToTable("MediaPlayerStatus");

            entity.HasIndex(e => e.PlayerId, "IX_Player_ID");

            entity.HasIndex(e => new { e.PlayerId, e.Status, e.TimeStamp }, "IX_STATE");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Ipstate)
                .HasDefaultValueSql("((-1))")
                .HasColumnName("IPState");
            entity.Property(e => e.PlayerId).HasColumnName("Player_ID");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.UserName)
                .HasMaxLength(255)
                .HasDefaultValueSql("('Система')");

            entity.HasOne(d => d.Player).WithMany(p => p.MediaPlayerStatuses)
                .HasForeignKey(d => d.PlayerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_dbo.MediaPlayerStatus_dbo.MediaPlayers_MediaPlayer_ID");
        });

        modelBuilder.Entity<MigrationHistory>(entity =>
        {
            entity.HasKey(e => new { e.MigrationId, e.ContextKey }).HasName("PK_dbo.__MigrationHistory");

            entity.ToTable("__MigrationHistory");

            entity.Property(e => e.MigrationId).HasMaxLength(150);
            entity.Property(e => e.ContextKey).HasMaxLength(300);
            entity.Property(e => e.ProductVersion).HasMaxLength(32);
        });

        modelBuilder.Entity<Plan>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Plans");

            entity.HasIndex(e => new { e.ProductionClassId, e.Ptype, e.TimeStamp }, "IX_PLAN").IsUnique();

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Changed)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.ProductionClassId)
                .HasDefaultValueSql("(N'3098c45d-79a5-4a71-987c-91e120edffeb')")
                .HasColumnName("ProductionClassID");
            entity.Property(e => e.Ptype).HasColumnName("PType");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.User)
                .HasMaxLength(255)
                .HasDefaultValueSql("('СИСТЕМА')");

            entity.HasOne(d => d.ProductionClass).WithMany(p => p.Plans)
                .HasForeignKey(d => d.ProductionClassId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_dbo.Plans_dbo.ProductionClasses_ProductionClassID");
        });

        modelBuilder.Entity<PostSnapshot>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.PostSnapshots");

            entity.HasIndex(e => e.AtypeId, "IX_AType_ID");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.AtypeId).HasColumnName("AType_ID");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.Ttype).HasColumnName("TType");
            entity.Property(e => e.Vin).HasColumnName("VIN");

            entity.HasOne(d => d.Atype).WithMany(p => p.PostSnapshots)
                .HasForeignKey(d => d.AtypeId)
                .HasConstraintName("FK_dbo.PostSnapshots_dbo.AccidentClasses_AType_ID");
        });

        modelBuilder.Entity<ProductionClass>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.ProductionClasses");

            entity.Property(e => e.Id)
                .HasDefaultValueSql("(newid())")
                .HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10);
            entity.Property(e => e.Name).HasMaxLength(100);
            entity.Property(e => e.Picture).HasMaxLength(10);
        });

        modelBuilder.Entity<Report>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Reports");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(31);
            entity.Property(e => e.Description).HasMaxLength(255);
            entity.Property(e => e.Name).HasMaxLength(63);
        });

        modelBuilder.Entity<Setting>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Settings");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.ConveyorTime).HasDefaultValueSql("('00:10:00')");
            entity.Property(e => e.ConveyorTimeout).HasDefaultValueSql("((10))");
            entity.Property(e => e.FinishTime).HasDefaultValueSql("('18:00:00')");
            entity.Property(e => e.LunchFinish).HasDefaultValueSql("('13:30:00')");
            entity.Property(e => e.LunchStart).HasDefaultValueSql("('12:30:00')");
            entity.Property(e => e.ModbusDeviceId)
                .HasDefaultValueSql("((1))")
                .HasColumnName("ModbusDeviceID");
            entity.Property(e => e.ModbusIp)
                .HasMaxLength(255)
                .HasDefaultValueSql("('127.0.0.1')")
                .HasColumnName("ModbusIP");
            entity.Property(e => e.ModbusPort).HasDefaultValueSql("((502))");
            entity.Property(e => e.ModbusThreshold).HasDefaultValueSql("((3))");
            entity.Property(e => e.ModbusTimeout).HasDefaultValueSql("((10000))");
            entity.Property(e => e.ReaderTimerInterval).HasDefaultValueSql("((1000))");
            entity.Property(e => e.StartTime).HasDefaultValueSql("('09:00:00')");
            entity.Property(e => e.SupportCount).HasDefaultValueSql("((27))");
            entity.Property(e => e.TimeStamp)
                .HasDefaultValueSql("('2019-01-01T00:00:00.000')")
                .HasColumnType("datetime");
            entity.Property(e => e.UdpSenderInterval).HasDefaultValueSql("((1000))");
            entity.Property(e => e.UserName)
                .HasMaxLength(255)
                .HasDefaultValueSql("('SYSTEM')");
        });

        modelBuilder.Entity<UdpVariable>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.UdpVariables");

            entity.HasIndex(e => e.Name, "IX_NAME").IsUnique();

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Enabled)
                .IsRequired()
                .HasDefaultValueSql("((1))");
            entity.Property(e => e.Format).HasMaxLength(63);
            entity.Property(e => e.Name).HasMaxLength(63);
            entity.Property(e => e.Source).HasMaxLength(127);
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.Users");

            entity.HasIndex(e => e.Name, "IX_USER").IsUnique();

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Name).HasMaxLength(255);
        });

        modelBuilder.Entity<UserAccess>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.UserAccesses");

            entity.HasIndex(e => e.UserId, "IX_User_ID");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(31);
            entity.Property(e => e.UserId).HasColumnName("User_ID");

            entity.HasOne(d => d.User).WithMany(p => p.UserAccesses)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_dbo.UserAccesses_dbo.Users_User_ID");
        });

        modelBuilder.Entity<UserLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_dbo.UserLogs");

            entity.HasIndex(e => e.TimeStamp, "IX_TIMESTAMP");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");
            entity.Property(e => e.Action).HasMaxLength(255);
            entity.Property(e => e.Controller)
                .HasMaxLength(255)
                .HasDefaultValueSql("('')");
            entity.Property(e => e.TimeStamp).HasColumnType("datetime");
            entity.Property(e => e.UserName).HasMaxLength(255);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
