﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class Accident
{
    public Guid Id { get; set; }

    public string? Post { get; set; }

    public string? Description { get; set; }

    public string UserName { get; set; } = null!;

    public DateTime TimeStamp { get; set; }

    public Guid? AtypeId { get; set; }

    public Guid? NextVersionId { get; set; }

    public DateTime StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public int Plan { get; set; }

    public int Fact { get; set; }

    public int PlanFact { get; set; }

    public Guid? BeventId { get; set; }

    public virtual AccidentClass? Atype { get; set; }

    public virtual ButtonEvent? Bevent { get; set; }

    public virtual ICollection<Accident> InverseNextVersion { get; } = new List<Accident>();

    public virtual Accident? NextVersion { get; set; }
}
