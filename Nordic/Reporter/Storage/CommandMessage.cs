﻿using System;
using System.Collections.Generic;

namespace Reporter.Storage;

public partial class CommandMessage
{
    public Guid Id { get; set; }

    public int Ctype { get; set; }

    public string? Text { get; set; }

    public bool State { get; set; }

    public string UserName { get; set; } = null!;

    public DateTime TimeStamp { get; set; }
}
