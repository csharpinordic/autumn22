using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Reporter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();

            builder.Services.AddControllers()
               .AddJsonOptions(option =>
               {
                   // ���������������� ������������ JSON
                   option.JsonSerializerOptions.WriteIndented = true;
                   // ��� �������������� ��� �������
                   option.JsonSerializerOptions.PropertyNamingPolicy = null;
               });

            // ��������� �������� ���� ������
            builder.Services.AddDbContext<Storage.DB>(options =>
            {
                options.UseSqlServer(ConfigurationManager.Configuration.GetConnectionString("DB"));
            });

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(swagger =>
            {
                var info = new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "API",
                    Description = "REST API",
                    Version = "v1"
                };
                swagger.SwaggerDoc(info.Version, info);
                string name = System.IO.Path.Combine(System.AppContext.BaseDirectory, "XmlDocumentation.xml");
                if (System.IO.File.Exists(name))
                {
                    swagger.IncludeXmlComments(name);
                }
                else
                {
                    // ���� �� ����������������� ������
                }
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapRazorPages();

            // �������� ������� ������������ � ��������� HTTP
            app.MapControllers();

            app.Run();
        }
    }
}