﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Reporter
{
    internal static class ConfigurationManager
    {
        public static IConfigurationRoot Configuration { get; }
        static ConfigurationManager()
        {
            string path = AppContext.BaseDirectory;         
            // Полный путь к файлу конфигурации
            string configName = System.IO.Path.Combine(path, "appsettings.json");
            // Конфигурация, загруженная в память
            Configuration = new ConfigurationBuilder().AddJsonFile(configName).Build();
        }
    }
}
