﻿namespace Homework_19
{
    partial class CarRegistrationForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.tbVin = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.labelNumber = new System.Windows.Forms.Label();
            this.labelVin = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.labelChassisNumber = new System.Windows.Forms.Label();
            this.tbChassisNumber = new System.Windows.Forms.TextBox();
            this.labelMotorPowerKW = new System.Windows.Forms.Label();
            this.labelMotorPowerLS = new System.Windows.Forms.Label();
            this.tbMotorPowerKW = new System.Windows.Forms.TextBox();
            this.tbMotorPowerLS = new System.Windows.Forms.TextBox();
            this.cbManufacturer = new System.Windows.Forms.ComboBox();
            this.lbManufacturer = new System.Windows.Forms.Label();
            this.btnCreateThread = new System.Windows.Forms.Button();
            this.btnCreateBackgroundWorker = new System.Windows.Forms.Button();
            this.epCar = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.epCar)).BeginInit();
            this.SuspendLayout();
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(354, 40);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(259, 39);
            this.tbNumber.TabIndex = 0;
            this.tbNumber.Validating += new System.ComponentModel.CancelEventHandler(this.tbNumber_Validating);
            // 
            // tbVin
            // 
            this.tbVin.Location = new System.Drawing.Point(354, 98);
            this.tbVin.Name = "tbVin";
            this.tbVin.Size = new System.Drawing.Size(259, 39);
            this.tbVin.TabIndex = 1;
            this.tbVin.Validating += new System.ComponentModel.CancelEventHandler(this.tbVin_Validating);
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(354, 157);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(259, 39);
            this.tbYear.TabIndex = 2;
            this.tbYear.Validating += new System.ComponentModel.CancelEventHandler(this.tbYear_Validating);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(11, 43);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(89, 32);
            this.labelNumber.TabIndex = 3;
            this.labelNumber.Text = "Номер";
            // 
            // labelVin
            // 
            this.labelVin.AutoSize = true;
            this.labelVin.Location = new System.Drawing.Point(11, 99);
            this.labelVin.Name = "labelVin";
            this.labelVin.Size = new System.Drawing.Size(53, 32);
            this.labelVin.TabIndex = 4;
            this.labelVin.Text = "VIN";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Location = new System.Drawing.Point(11, 160);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(150, 32);
            this.labelYear.TabIndex = 5;
            this.labelYear.Text = "Год выпуска";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(176, 485);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(439, 46);
            this.btnCreate.TabIndex = 6;
            this.btnCreate.Text = "Создать в основном потоке";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // labelChassisNumber
            // 
            this.labelChassisNumber.AutoSize = true;
            this.labelChassisNumber.Location = new System.Drawing.Point(11, 230);
            this.labelChassisNumber.Name = "labelChassisNumber";
            this.labelChassisNumber.Size = new System.Drawing.Size(163, 32);
            this.labelChassisNumber.TabIndex = 7;
            this.labelChassisNumber.Text = "Номер шасси";
            // 
            // tbChassisNumber
            // 
            this.tbChassisNumber.Location = new System.Drawing.Point(354, 227);
            this.tbChassisNumber.Name = "tbChassisNumber";
            this.tbChassisNumber.Size = new System.Drawing.Size(259, 39);
            this.tbChassisNumber.TabIndex = 3;
            // 
            // labelMotorPowerKW
            // 
            this.labelMotorPowerKW.AutoSize = true;
            this.labelMotorPowerKW.Location = new System.Drawing.Point(11, 294);
            this.labelMotorPowerKW.Name = "labelMotorPowerKW";
            this.labelMotorPowerKW.Size = new System.Drawing.Size(297, 32);
            this.labelMotorPowerKW.TabIndex = 9;
            this.labelMotorPowerKW.Text = "Мощность двигателя(кВт)";
            // 
            // labelMotorPowerLS
            // 
            this.labelMotorPowerLS.AutoSize = true;
            this.labelMotorPowerLS.Location = new System.Drawing.Point(11, 354);
            this.labelMotorPowerLS.Name = "labelMotorPowerLS";
            this.labelMotorPowerLS.Size = new System.Drawing.Size(301, 32);
            this.labelMotorPowerLS.TabIndex = 10;
            this.labelMotorPowerLS.Text = "Мощность двигателя (л\\с)";
            // 
            // tbMotorPowerKW
            // 
            this.tbMotorPowerKW.Location = new System.Drawing.Point(354, 294);
            this.tbMotorPowerKW.Name = "tbMotorPowerKW";
            this.tbMotorPowerKW.Size = new System.Drawing.Size(259, 39);
            this.tbMotorPowerKW.TabIndex = 4;
            this.tbMotorPowerKW.Validating += new System.ComponentModel.CancelEventHandler(this.tbMotorPowerKW_Validating);
            // 
            // tbMotorPowerLS
            // 
            this.tbMotorPowerLS.Location = new System.Drawing.Point(354, 354);
            this.tbMotorPowerLS.Name = "tbMotorPowerLS";
            this.tbMotorPowerLS.Size = new System.Drawing.Size(259, 39);
            this.tbMotorPowerLS.TabIndex = 5;
            this.tbMotorPowerLS.Validating += new System.ComponentModel.CancelEventHandler(this.tbMotorPowerLS_Validating);
            // 
            // cbManufacturer
            // 
            this.cbManufacturer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbManufacturer.FormattingEnabled = true;
            this.cbManufacturer.Location = new System.Drawing.Point(354, 411);
            this.cbManufacturer.Name = "cbManufacturer";
            this.cbManufacturer.Size = new System.Drawing.Size(259, 40);
            this.cbManufacturer.TabIndex = 11;
            // 
            // lbManufacturer
            // 
            this.lbManufacturer.AutoSize = true;
            this.lbManufacturer.Location = new System.Drawing.Point(11, 414);
            this.lbManufacturer.Name = "lbManufacturer";
            this.lbManufacturer.Size = new System.Drawing.Size(186, 32);
            this.lbManufacturer.TabIndex = 12;
            this.lbManufacturer.Text = "Производитель";
            // 
            // btnCreateThread
            // 
            this.btnCreateThread.Location = new System.Drawing.Point(176, 538);
            this.btnCreateThread.Name = "btnCreateThread";
            this.btnCreateThread.Size = new System.Drawing.Size(439, 46);
            this.btnCreateThread.TabIndex = 13;
            this.btnCreateThread.Text = "Создать используя Thread";
            this.btnCreateThread.UseVisualStyleBackColor = true;
            this.btnCreateThread.Click += new System.EventHandler(this.btnCreateThread_Click);
            // 
            // btnCreateBackgroundWorker
            // 
            this.btnCreateBackgroundWorker.Location = new System.Drawing.Point(176, 589);
            this.btnCreateBackgroundWorker.Name = "btnCreateBackgroundWorker";
            this.btnCreateBackgroundWorker.Size = new System.Drawing.Size(439, 46);
            this.btnCreateBackgroundWorker.TabIndex = 14;
            this.btnCreateBackgroundWorker.Text = "Создать используя BackgroundWorker";
            this.btnCreateBackgroundWorker.UseVisualStyleBackColor = true;
            this.btnCreateBackgroundWorker.Click += new System.EventHandler(this.btnCreateBackgroundWorker_Click);
            // 
            // epCar
            // 
            this.epCar.ContainerControl = this;
            // 
            // CarRegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 701);
            this.Controls.Add(this.btnCreateBackgroundWorker);
            this.Controls.Add(this.btnCreateThread);
            this.Controls.Add(this.lbManufacturer);
            this.Controls.Add(this.cbManufacturer);
            this.Controls.Add(this.tbMotorPowerLS);
            this.Controls.Add(this.tbMotorPowerKW);
            this.Controls.Add(this.labelMotorPowerLS);
            this.Controls.Add(this.labelMotorPowerKW);
            this.Controls.Add(this.tbChassisNumber);
            this.Controls.Add(this.labelChassisNumber);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.labelVin);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.tbYear);
            this.Controls.Add(this.tbVin);
            this.Controls.Add(this.tbNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CarRegistrationForm";
            this.Text = "Регистрация транспортного средства";
            ((System.ComponentModel.ISupportInitialize)(this.epCar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox tbNumber;
        private TextBox tbVin;
        private TextBox tbYear;
        private Label labelNumber;
        private Label labelVin;
        private Label labelYear;
        private Button btnCreate;
        private Label labelChassisNumber;
        private TextBox tbChassisNumber;
        private Label labelMotorPowerKW;
        private Label labelMotorPowerLS;
        private TextBox tbMotorPowerKW;
        private TextBox tbMotorPowerLS;
        private ComboBox cbManufacturer;
        private Label lbManufacturer;
        private Button btnCreateThread;
        private Button btnCreateBackgroundWorker;
        private ErrorProvider epCar;
    }
}