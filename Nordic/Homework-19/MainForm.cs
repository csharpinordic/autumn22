﻿namespace Homework_19
{
    /// <summary>
    /// Основное окно программы (меню)
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработка нажатия кнопки "Справочник производителей"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnManufacturerReference_Click(object sender, EventArgs e)
        {
            using(var form = new ManufacturerReferenceForm())
            {
                form.ShowDialog();
            }
        }

        /// <summary>
        /// Обработка нажатия кнопки "Регистрация транспортных средств"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCarRegistration_Click(object sender, EventArgs e)
        {
            using(var form = new CarRegistrationForm())
            {
                form.ShowDialog();
            }
        }
    }
}
