﻿namespace Homework_19
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCarRegistration = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.btnManufacturerReference = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarRegistration
            // 
            this.btnCarRegistration.Location = new System.Drawing.Point(169, 119);
            this.btnCarRegistration.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnCarRegistration.Name = "btnCarRegistration";
            this.btnCarRegistration.Size = new System.Drawing.Size(488, 47);
            this.btnCarRegistration.TabIndex = 0;
            this.btnCarRegistration.Text = "Регистрация транспортных средств";
            this.btnCarRegistration.UseVisualStyleBackColor = true;
            this.btnCarRegistration.Click += new System.EventHandler(this.btnCarRegistration_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.btnManufacturerReference);
            this.mainPanel.Controls.Add(this.btnCarRegistration);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(830, 420);
            this.mainPanel.TabIndex = 1;
            // 
            // btnManufacturerReference
            // 
            this.btnManufacturerReference.Location = new System.Drawing.Point(169, 175);
            this.btnManufacturerReference.Margin = new System.Windows.Forms.Padding(6);
            this.btnManufacturerReference.Name = "btnManufacturerReference";
            this.btnManufacturerReference.Size = new System.Drawing.Size(488, 49);
            this.btnManufacturerReference.TabIndex = 1;
            this.btnManufacturerReference.Text = "Справочник производителей";
            this.btnManufacturerReference.UseVisualStyleBackColor = true;
            this.btnManufacturerReference.Click += new System.EventHandler(this.btnManufacturerReference_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(533, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(7, 9);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 420);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.mainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Основное меню";
            this.mainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnCarRegistration;
        private Panel mainPanel;
        private TableLayoutPanel tableLayoutPanel1;
        private Button btnManufacturerReference;
    }
}