﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace Homework_19.Storage
{
    /// <summary>
    /// Класс транспортных средств
    /// </summary>
    [Index(nameof(Vin), IsUnique = true)]
    public class Car : Entity
    {
        #region "Properties"
        /// <summary>
        /// Регистрационный номер автомобиля
        /// </summary>
        [MaxLength(9)]
        public string Number { get; set; }

        /// <summary>
        /// Vin (идентификационный номер)
        /// </summary>
        [MaxLength(17)]
        public string Vin { get; set; }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Номер шасси
        /// </summary>
        [MaxLength(50)]
        public string? ChassisNumber { get; set; }

        /// <summary>
        /// Мощность двигателя (квт)
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal? MotorPowerKW { get; set; }

        /// <summary>
        /// Мощность двигателя (л\с)
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal? MotorPowerLS { get; set; }

        /// <summary>
        /// Производитель
        /// </summary>
        public Manufacturer? Manufacturer { get; set; }

        /// <summary>
        /// Идентификатор производителя
        /// </summary>
        public Guid? ManufacturerID { get; set; }
        #endregion

        #region "Override"
        public override string ToString() => Number;
        #endregion
    }
}
