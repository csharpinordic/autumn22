﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Homework_19.Storage
{
    public class Database : DbContext
    {
        /// <summary>
        /// Автомобили
        /// </summary>
        public DbSet<Car> Cars { get; set; }

        /// <summary>
        /// Производители автомобилей
        /// </summary>
        public DbSet<Manufacturer> Manufacturers { get; set; }

        public Database()
        {
            Database.Migrate();
        }

        /// <summary>
        /// Конфигурация базы данных
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.Configuration.GetSection("ConnectionString").Value);
        }
    }
}
