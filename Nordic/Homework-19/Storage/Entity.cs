﻿namespace Homework_19.Storage
{
    /// <summary>
    /// Базовый класс сущности
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public Guid ID { get; set; }
    }
}
