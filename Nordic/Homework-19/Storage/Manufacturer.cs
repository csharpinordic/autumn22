﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Homework_19.Storage
{
    /// <summary>
    /// Производитель автомобилей
    /// </summary>
    [Index(nameof(CountryCode), IsUnique = true)]
    public class Manufacturer : Entity
    {
        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Код страны (стандарт ISO 3166-1 alpha 2)
        /// Формат "XX"
        /// </summary>
        [MaxLength(2)]
        public string CountryCode { get; set; }

        public override string ToString() => $"{Name} ({CountryCode})";
    }
}
