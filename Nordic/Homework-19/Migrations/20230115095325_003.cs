﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Homework19.Migrations
{
    /// <inheritdoc />
    public partial class _003 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ManufacturerID",
                table: "Cars",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Manufacturers",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CountryCode = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manufacturers", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ManufacturerID",
                table: "Cars",
                column: "ManufacturerID");

            migrationBuilder.AddForeignKey(
                name: "FK_Cars_Manufacturers_ManufacturerID",
                table: "Cars",
                column: "ManufacturerID",
                principalTable: "Manufacturers",
                principalColumn: "ID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cars_Manufacturers_ManufacturerID",
                table: "Cars");

            migrationBuilder.DropTable(
                name: "Manufacturers");

            migrationBuilder.DropIndex(
                name: "IX_Cars_ManufacturerID",
                table: "Cars");

            migrationBuilder.DropColumn(
                name: "ManufacturerID",
                table: "Cars");
        }
    }
}
