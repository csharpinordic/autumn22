﻿using Homework_19.Storage;

namespace Homework_19
{
    partial class ManufacturerReferenceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lbCountryCode = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.tbCountryCode = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.mainView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsManufacturer = new System.Windows.Forms.BindingSource(this.components);
            this.epManufacturer = new System.Windows.Forms.ErrorProvider(this.components);
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsManufacturer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epManufacturer)).BeginInit();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.btnDelete);
            this.headerPanel.Controls.Add(this.btnEdit);
            this.headerPanel.Controls.Add(this.btnCreate);
            this.headerPanel.Controls.Add(this.lbCountryCode);
            this.headerPanel.Controls.Add(this.lbName);
            this.headerPanel.Controls.Add(this.tbCountryCode);
            this.headerPanel.Controls.Add(this.tbName);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(6);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1415, 176);
            this.headerPanel.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(906, 82);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(181, 46);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(1093, 18);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(310, 110);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Изменить выбранный";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(906, 18);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(181, 46);
            this.btnCreate.TabIndex = 2;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lbCountryCode
            // 
            this.lbCountryCode.AutoSize = true;
            this.lbCountryCode.Location = new System.Drawing.Point(67, 96);
            this.lbCountryCode.Name = "lbCountryCode";
            this.lbCountryCode.Size = new System.Drawing.Size(140, 32);
            this.lbCountryCode.TabIndex = 3;
            this.lbCountryCode.Text = "Код страны";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(26, 41);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(181, 32);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Наименование";
            // 
            // tbCountryCode
            // 
            this.tbCountryCode.Location = new System.Drawing.Point(226, 93);
            this.tbCountryCode.MaxLength = 2;
            this.tbCountryCode.Name = "tbCountryCode";
            this.tbCountryCode.Size = new System.Drawing.Size(142, 39);
            this.tbCountryCode.TabIndex = 1;
            this.tbCountryCode.Validating += new System.ComponentModel.CancelEventHandler(this.tbCountryCode_Validating);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(226, 38);
            this.tbName.MaxLength = 50;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(310, 39);
            this.tbName.TabIndex = 0;
            this.tbName.Validating += new System.ComponentModel.CancelEventHandler(this.tbName_Validating);
            // 
            // mainView
            // 
            this.mainView.AutoGenerateColumns = false;
            this.mainView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.mainView.DataSource = this.bsManufacturer;
            this.mainView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainView.Location = new System.Drawing.Point(0, 176);
            this.mainView.MultiSelect = false;
            this.mainView.Name = "mainView";
            this.mainView.RowHeadersWidth = 82;
            this.mainView.RowTemplate.Height = 41;
            this.mainView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mainView.Size = new System.Drawing.Size(1415, 323);
            this.mainView.TabIndex = 2;
            this.mainView.SelectionChanged += new System.EventHandler(this.mainView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CountryCode";
            this.dataGridViewTextBoxColumn2.HeaderText = "CountryCode";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 10;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // bsManufacturer
            // 
            this.bsManufacturer.DataSource = typeof(Homework_19.Storage.Manufacturer);
            // 
            // epManufacturer
            // 
            this.epManufacturer.ContainerControl = this;
            // 
            // ManufacturerReferenceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1415, 499);
            this.Controls.Add(this.mainView);
            this.Controls.Add(this.headerPanel);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ManufacturerReferenceForm";
            this.Text = "Справочник производителей";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsManufacturer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epManufacturer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel headerPanel;
        private DataGridView dataGridView2;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn countryCodeDataGridViewTextBoxColumn;
        private BindingSource manufacturerBindingSource;
        private TextBox tbCountryCode;
        private TextBox tbName;
        private Button btnCreate;
        private Label lbCountryCode;
        private Label lbName;
        private Button btnDelete;
        private Button btnEdit;
        private DataGridView mainView;
        private DataGridViewTextBoxColumn colName;
        private DataGridViewTextBoxColumn colCountryCode;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private BindingSource bsManufacturer;
        private ErrorProvider epManufacturer;
    }
}