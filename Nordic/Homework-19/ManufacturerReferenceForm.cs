﻿using Homework_19.Storage;

namespace Homework_19
{
    /// <summary>
    /// Форма списка производителей
    /// </summary>
    public partial class ManufacturerReferenceForm : Form
    {
        #region "Variables"

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly Database db = new();

        #endregion

        #region "Константы визуализации ошибок"
        private const string ErrorCaption = "Ошибка";
        private const string ElementExistsMessage = "Элемент с кодом страны \"{0}\" уже существует.";
        private const string ErrorOnLoadMessage = "Во время загрузки списка объектов произошла ошибка:";
        private const string ErrorOnCreateElementMessage = "Во время создания нового элемента произошла ошибка:";
        private const string ErrorOnEditElementMessage = "Во время изменения элемента произошла ошибка:";
        private const string ErrorOnDeleteElementMessage = "Во время удаления элемента произошла ошибка:";
        private const string NotValidInputDataMessage = "Необходимо исправить входящие данные (см. ошибки валидации).";
        #endregion

        #region "Константы валидации"
        private const string DoesNotEmptyMessage = "Не может быть пустым.";
        #endregion

        #region "Инициализация"

        /// <summary>
        /// Конструктор
        /// </summary>
        public ManufacturerReferenceForm()
        {
            InitializeComponent();
            RuntimeInitialize();
        }

        /// <summary>
        /// Инициализация в режиме выполнения
        /// </summary>
        private void RuntimeInitialize()
        {
            try
            {
                bsManufacturer.DataSource = db.Manufacturers.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ErrorOnLoadMessage + Environment.NewLine + ex.ToString(), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region "Обработчики событий"

        /// <summary>
        /// Обработка нажатия кнопки "Создать"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    MessageBox.Show(NotValidInputDataMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (IsContainsCountryCode())
                {
                    MessageBox.Show(string.Format(ElementExistsMessage, tbCountryCode.Text), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Manufacturer newItem = new()
                {
                    Name = tbName.Text,
                    CountryCode = tbCountryCode.Text,
                };

                db.Manufacturers.Add(newItem);
                db.SaveChanges();

                List<Manufacturer>? items = bsManufacturer.DataSource as List<Manufacturer>;

                if (items != null)
                    items.Add(newItem);

                bsManufacturer.ResetBindings(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ErrorOnCreateElementMessage + Environment.NewLine + ex.ToString(), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Обработка нажатия кнопки "Изменить выбранный"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    MessageBox.Show(NotValidInputDataMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var currentItem = GetSelectedItem(out _);

                if (currentItem != null)
                {
                    if (IsContainsCountryCode(currentItem.ID))
                    {
                        MessageBox.Show(string.Format(ElementExistsMessage, tbCountryCode.Text), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    currentItem.Name = tbName.Text;
                    currentItem.CountryCode = tbCountryCode.Text;

                    db.Update(currentItem);
                    db.SaveChanges();
                    bsManufacturer.ResetCurrentItem();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ErrorOnEditElementMessage + Environment.NewLine + ex.ToString(), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Обработка нажатия кнопки "Удалить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = GetSelectedItem(out int selectedIndex);

                if (selectedItem != null)
                {
                    db.Remove(selectedItem);
                    db.SaveChanges();

                    List<Manufacturer>? items = bsManufacturer.DataSource as List<Manufacturer>;

                    if (items != null)
                        items.Remove(selectedItem);
                    bsManufacturer.ResetBindings(true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ErrorOnDeleteElementMessage + Environment.NewLine + ex.ToString(), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Обработка выбора элемента в гриде
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainView_SelectionChanged(object sender, EventArgs e)
        {
            // Ищем текущий элемент
            var currentItem = GetSelectedItem(out _);

            // Если найден - меняем шапку
            if (currentItem != null)
            {
                tbCountryCode.Text = currentItem.CountryCode;
                tbName.Text = currentItem.Name;

                // Обнуляем ошибки в контролах
                epManufacturer.SetError(tbCountryCode, null);
                epManufacturer.SetError(tbName, null);
            }
        }

        /// <summary>
        /// Валидация введенных данных в поле "Наименование"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ValidateName();
        }

        /// <summary>
        /// Валидация введенных данных в поле "Код страны"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbCountryCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ValidateCountryCode();
        }

        #endregion

        #region "Служебные методы"

        /// <summary>
        /// Валидация
        /// </summary>
        private void ValidateAll()
        {
            epManufacturer.Clear();
            ValidateName();
            ValidateCountryCode();
        }

        /// <summary>
        /// Валидация поля "Наименование"
        /// </summary>
        private void ValidateName()
        {
            string? msg = null;
            if (string.IsNullOrEmpty(tbName.Text))
                msg = DoesNotEmptyMessage;

            epManufacturer.SetError(tbName, msg);
        }

        /// <summary>
        /// Валидация поля "Код страны"
        /// </summary>
        private void ValidateCountryCode()
        {
            string? msg = null;

            if (string.IsNullOrEmpty(tbCountryCode.Text))
            {
                msg = DoesNotEmptyMessage;
            }
            else if (tbCountryCode.Text.Length < 2)
            {
                msg = "Необходимо не менее двух символов.";
            }
            else if (tbCountryCode.Text.Length == 2)
            {
                foreach (char ch in tbCountryCode.Text.ToCharArray())
                {
                    if (!char.IsLetter(ch))
                    {
                        msg = "Разрешены только буквы (стандарт ISO 3166-1 alpha 2).";
                        break;
                    }
                }
            }

            epManufacturer.SetError(tbCountryCode, msg);
        }

        /// <summary>
        /// Проверяет поля шапки на валидность
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            ValidateAll();
            foreach (Control item in GetAllControls(epManufacturer.ContainerControl))
            {
                string? currentError = epManufacturer.GetError(item);
                if (!string.IsNullOrEmpty(currentError))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Получение списка контролов <paramref name="control"/> с дочерними элементами
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private IEnumerable<Control> GetAllControls(Control control)
        {
            var result = new List<Control>();
            foreach (Control item in control.Controls)
            {
                if (item.Controls != null)
                    result.AddRange(GetAllControls(item));

                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Получение текущего (выбранного) элемента в списке Grid
        /// </summary>
        /// <param name="index">Индекс элемента, (-1 при отсутствии)</param>
        /// <returns>Объект типа <see cref="Manufacturer"/> или NULL при отсутствии</returns>
        private Manufacturer? GetSelectedItem(out int index)
        {
            Manufacturer? result = null;
            index = -1;

            // Проверяем есть ли выбранные элементы
            if (mainView.SelectedRows.Count > 0)
            {
                // Первый элемент коллекции
                result = mainView.SelectedRows[0].DataBoundItem as Manufacturer;
                // Индекс элемента
                index = mainView.SelectedRows[0].Index;
            }

            return result;
        }


        /// <summary>
        /// Проверка на существование элемента с указанным в шапке кодом страны
        /// </summary>
        /// <param name="excludedId">Исключенный идентификатор элемента</param>
        /// <returns></returns>
        private bool IsContainsCountryCode(Guid? excludedId = null)
        {
            List<Manufacturer>? items = bsManufacturer.DataSource as List<Manufacturer>;

            if (items != null)
            {
                var existsCountryCodeItem = items
                    .FirstOrDefault(item => item.CountryCode == tbCountryCode.Text && (excludedId == null || item.ID != excludedId));

                return existsCountryCodeItem != null;
            }

            return false;
        }

        #endregion

        #region "IDisposable"
        /// <summary>
        /// Очистка управляемых и неуправляемых ресурсов
        /// </summary>
        /// <param name="disposing">true - очистить управляемые ресурсы, false - пропустить</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                components?.Dispose();
                db?.Dispose();
            }

            base.Dispose(disposing);
        }
        #endregion
    }
}
