using Homework_19.Storage;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Homework_19
{
    /// <summary>
    /// ����� ����������� ������������ �������
    /// </summary>
    public partial class CarRegistrationForm : Form
    {
        #region "���������"
        private const string ErrorCaption = "������";
        private const string InformationCaption = "����������";
        private const string NotValidInputDataMessage = "���������� ��������� �������� ������ (��. ������ ���������).";
        private const string SuccessfullOperationMessage = "�������� ��������� �������.";
        private const string CreateErrorMessage = "��� �������� ������� ��������� ������.";
        private const string ElementExistsMessage = "���������� � VIN \"{0}\" ��� ���������������.";
        #endregion

        #region "Variables"
        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private readonly Database db = new();

        /// <summary>
        /// ������� ��� ������������� ������� �������� � ����������
        /// </summary>
        private static readonly Dictionary<char, char> _replaceableNumberChars = new Dictionary<char, char>()
        {
            {'�', 'A' },
            {'�', 'B' },
            {'�', 'E' },
            {'�', 'K' },
            {'�', 'M' },
            {'�', 'H' },
            {'�', 'O' },
            {'�', 'P' },
            {'�', 'C' },
            {'�', 'T' },
            {'�', 'Y' },
            {'�', 'X' }
        };
        #endregion

        #region "�������������"

        public CarRegistrationForm()
        {
            InitializeComponent();
            RuntimeInitialize();
        }

        /// <summary>
        /// ������������� �� ������� ����������
        /// </summary>
        private void RuntimeInitialize()
        {
            var manufacturerList = db.Manufacturers.ToArray();
            cbManufacturer.Items.AddRange(manufacturerList);
        }

        #endregion

        #region "����������� �������"

        /// <summary>
        /// ��������� ������� ������ "�������"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    MessageBox.Show(NotValidInputDataMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (IsContainsVin(tbNumber.Text))
                {
                    MessageBox.Show(string.Format(ElementExistsMessage, tbVin.Text), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string number = tbNumber.Text;
                string year = tbYear.Text;
                string vin = tbVin.Text;
                string motorPowerLS = tbMotorPowerLS.Text;
                string motorPowerKW = tbMotorPowerKW.Text;
                Manufacturer? manufacturer = cbManufacturer.SelectedItem as Manufacturer;
                string chassisNumber = tbChassisNumber.Text;

                UpdateDatabase(number, year, vin, motorPowerLS, motorPowerKW, manufacturer, chassisNumber, out string msg);
                MessageBox.Show(SuccessfullOperationMessage + Environment.NewLine + msg, InformationCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(CreateErrorMessage + Environment.NewLine + ex.Message, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// ��������� ������� ������ "������� ��������� Thread"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateThread_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    MessageBox.Show(NotValidInputDataMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (IsContainsVin(tbNumber.Text))
                {
                    MessageBox.Show(string.Format(ElementExistsMessage, tbVin.Text), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string number = tbNumber.Text;
                string year = tbYear.Text;
                string vin = tbVin.Text;
                string motorPowerLS = tbMotorPowerLS.Text;
                string motorPowerKW = tbMotorPowerKW.Text;
                Manufacturer? manufacturer = cbManufacturer.SelectedItem as Manufacturer;
                string chassisNumber = tbChassisNumber.Text;

                Thread thread = new Thread(() =>
                {
                    UpdateDatabase(number, year, vin, motorPowerLS, motorPowerKW, manufacturer, chassisNumber, out string msg);
                    MessageBox.Show(SuccessfullOperationMessage + Environment.NewLine + msg, InformationCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                });
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(CreateErrorMessage + Environment.NewLine + ex.Message, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// ��������� ������� ������ "������� ��������� BackgroundWorker" 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateBackgroundWorker_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    MessageBox.Show(NotValidInputDataMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (IsContainsVin(tbNumber.Text))
                {
                    MessageBox.Show(string.Format(ElementExistsMessage, tbVin.Text), ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string number = tbNumber.Text;
                string year = tbYear.Text;
                string vin = tbVin.Text;
                string motorPowerLS = tbMotorPowerLS.Text;
                string motorPowerKW = tbMotorPowerKW.Text;
                Manufacturer? manufacturer = cbManufacturer.SelectedItem as Manufacturer;
                string chassisNumber = tbChassisNumber.Text;

                BackgroundWorker bw = new BackgroundWorker();
                bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
                bw.DoWork += (sender, args) =>
                {
                    UpdateDatabase(number, year, vin, motorPowerLS, motorPowerKW, manufacturer, chassisNumber, out string msg);
                    args.Result = msg;
                };
                bw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(CreateErrorMessage + Environment.NewLine + ex.Message, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// ��������� ������ ������ "������� ��������� BackgroundWorker"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Bw_RunWorkerCompleted(object? sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
                MessageBox.Show(SuccessfullOperationMessage + Environment.NewLine + e.Result, InformationCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// ���������� ��������� ���� "�����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbNumber_Validating(object sender, CancelEventArgs e)
        {
            ValidateNumber();
        }

        /// <summary>
        /// ���������� ��������� ���� "VIN"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbVin_Validating(object sender, CancelEventArgs e)
        {
            ValidateVin();
        }

        /// <summary>
        /// ���������� ��������� ���� "��� �������"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbYear_Validating(object sender, CancelEventArgs e)
        {
            ValidateYear();
        }

        /// <summary>
        /// ���������� ��������� ���� "�������� ��������� (���)"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMotorPowerKW_Validating(object sender, CancelEventArgs e)
        {
            ValidateMotorPowerKW();
        }

        /// <summary>
        /// ���������� ��������� ���� "�������� ��������� (�/�)"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMotorPowerLS_Validating(object sender, CancelEventArgs e)
        {
            ValidateMotorPowerLS();
        }
        #endregion

        #region "��������� ������"
        private void UpdateDatabase(string strNumber, string strYear, string strVin, string strMotorPowerLS, string strMotorPowerKW, Manufacturer? manufacturer, string strChassisNumber, out string msg)
        {
            try
            {
                bool isNew = false;
                var car = db.Cars.FirstOrDefault(car => car.Number == strNumber);

                if (car == null)
                {
                    car = new Car();
                    isNew = true;
                }

                car.Number = strNumber;
                car.Vin = strVin;
                car.Year = int.Parse(strYear);
                car.ChassisNumber = strChassisNumber;
                car.MotorPowerKW = string.IsNullOrEmpty(strMotorPowerKW) ? null : decimal.Parse(strMotorPowerKW);
                car.MotorPowerLS = string.IsNullOrEmpty(strMotorPowerLS) ? null : decimal.Parse(strMotorPowerLS);
                car.Manufacturer = manufacturer;

                db.Update(car);
                db.SaveChanges();

                msg = isNew ? "������ ��������." : "������ ��������.";
            }
            catch (Exception ex)
            {
                msg = null;
                MessageBox.Show(CreateErrorMessage + Environment.NewLine + ex.Message, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// ��������� ���� ����� �� ����������
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            ValidateAll();
            foreach (Control item in GetAllControls(epCar.ContainerControl))
            {
                string? currentError = epCar.GetError(item);
                if (!string.IsNullOrEmpty(currentError))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// ��������� ������ ��������� <paramref name="control"/> � ��������� ����������
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private IEnumerable<Control> GetAllControls(Control control)
        {
            var result = new List<Control>();
            foreach (Control item in control.Controls)
            {
                if (item.Controls != null)
                    result.AddRange(GetAllControls(item));

                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// ���������
        /// </summary>
        private void ValidateAll()
        {
            epCar.Clear();
            ValidateNumber();
            ValidateVin();
            ValidateYear();
            ValidateMotorPowerKW();
            ValidateMotorPowerLS();
        }

        /// <summary>
        /// ��������� ���� "�����"
        /// </summary>
        private void ValidateNumber()
        {
            tbNumber.Text = tbNumber.Text.ToUpper();
            string? msg = null;

            foreach (var replaceableChar in _replaceableNumberChars)
                tbNumber.Text = tbNumber.Text.Replace(replaceableChar.Key, replaceableChar.Value);

            var match = Regex.Match(tbNumber.Text, "^([A,B,E,K,M,H,O,P,C,T,Y,X]{1})([0-9]{3})([A,B,E,K,M,H,O,P,C,T,Y,X]{2})([0-9]{2,3}$)");

            if (!match.Success)
                msg = "�������� ������ ������.";

            epCar.SetError(tbNumber, msg);
        }

        /// <summary>
        /// ��������� ���� "VIN"
        /// </summary>
        private void ValidateVin()
        {
            string? msg = null;
            var match = Regex.Match(tbVin.Text, "^([0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,R,S,T,U,V,W,X,Y,Z]{17})$");

            if (!match.Success)
                msg = "�������� ������ VIN.";

            epCar.SetError(tbVin, msg);
        }

        /// <summary>
        /// ��������� ���� "��� �������"
        /// </summary>
        private void ValidateYear()
        {
            string? msg = null;
            if (!int.TryParse(tbYear.Text, out int year))
                msg = "�������� ������ ��������� ������ ���� �������. (���������: \"XXXX\", ��� � - �����)";
            else if (year > DateTime.Now.Year || year < 1983)
                msg = "�������� �������� ���� �������. (���������� ��������: � 1983 �� ��������� �����)";

            epCar.SetError(tbYear, msg);
        }

        /// <summary>
        /// ��������� ���� "�������� ��������� (���)"
        /// </summary>
        private void ValidateMotorPowerKW()
        {
            string? msg = null;

            if (string.IsNullOrEmpty(tbMotorPowerKW.Text) && string.IsNullOrEmpty(tbMotorPowerLS.Text))
            {
                msg = "���������� ������� �������� ��������� � ����� �� ������ ���������.";
            }
            else if (!string.IsNullOrEmpty(tbMotorPowerKW.Text) && !decimal.TryParse(tbMotorPowerKW.Text, out decimal motorPowerKW))
            {
                msg = "�������� ������ ��������� ������ �������� ��������� (���).";
            }

            epCar.SetError(tbMotorPowerKW, msg);
        }

        /// <summary>
        /// ��������� ���� "�������� ��������� (�/�)"
        /// </summary>
        private void ValidateMotorPowerLS()
        {
            string? msg = null;

            if (string.IsNullOrEmpty(tbMotorPowerKW.Text) && string.IsNullOrEmpty(tbMotorPowerLS.Text))
            {
                msg = "���������� ������� �������� ��������� � ����� �� ������ ���������.";
            }
            else if (!string.IsNullOrEmpty(tbMotorPowerLS.Text) && !decimal.TryParse(tbMotorPowerLS.Text, out decimal motorPowerLS))
            {
                msg = "�������� ������ ��������� ������ �������� ��������� (�/�).";
            }

            epCar.SetError(tbMotorPowerLS, msg);
        }

        /// <summary>
        /// �������� �� ������������� � ��������� � ����� VIN
        /// </summary>
        /// <param name="excludedNumber">����������� �����</param>
        /// <returns></returns>
        private bool IsContainsVin(string? excludedNumber = null)
        {
            var existsCountryCodeItem = db.Cars
                .FirstOrDefault(item => item.Vin == tbVin.Text && (excludedNumber == null || item.Number != excludedNumber));

            return existsCountryCodeItem != null;
        }
        #endregion

        #region "IDisposable"
        /// <summary>
        /// ������� ����������� � ������������� ��������
        /// </summary>
        /// <param name="disposing">true - �������� ����������� �������, false - ����������</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                components?.Dispose();
                db?.Dispose();
            }

            base.Dispose(disposing);
        }
        #endregion
    }
}