﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GalaxyClient")]
[assembly: AssemblyDescription("WPF-клиент ")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Nordic IT School")]
[assembly: AssemblyProduct("Пример клиента на базе Windows Presentation Foundation")]
[assembly: AssemblyCopyright("Copyright © Банников С.Н. 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0662CA99-066D-4844-B4F9-743C543C33FB")]

// Информация о версиях формируется централизованно
