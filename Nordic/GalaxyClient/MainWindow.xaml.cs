﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GalaxyClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WeatherAPI wapi = new();
        private Galaxy.API.ApiClient gapi;
        private System.Windows.Threading.DispatcherTimer timer = new();

        /// <summary>
        /// Конструктор формы
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            var http = new HttpClient();
            gapi = new Galaxy.API.ApiClient("http://localhost:5000/", http);
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            try
            {
                if (grid.Items.Count == 0)
                {
                    grid.ItemsSource = gapi.GetAllAsync(null).Result.Payload;
                    statusLabel.Content = null;
                }
            }
            catch (Exception ex)
            {
                statusLabel.Content = ex.ToString();
            }
        }        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid.SelectedItem is Galaxy.API.Game model)
                {
                    var result = gapi.SaveAsync(model).Result;
                }
                else
                {
                    MessageBox.Show("Сохранение не удалось");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Weather_Click(object sender, RoutedEventArgs e)
        {
            var data = wapi.Request().Result;
            MessageBox.Show($"{data}");
        }
    }
}
