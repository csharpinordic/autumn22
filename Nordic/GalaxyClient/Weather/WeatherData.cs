﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GalaxyClient.Weather
{
    /// <summary>
    /// Сведения о погоде
    /// </summary>
    public class WeatherData
    {
        [JsonProperty(propertyName: "base")]
        public string Base { get; set; }

        public override string ToString()
        {
            return Base;
        }
    }
}
