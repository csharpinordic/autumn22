﻿using GalaxyClient.Weather;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GalaxyClient
{
    public class WeatherAPI
    {
        private string configName;
        private HttpClient client;

        public WeatherAPI()
        {
            client = new HttpClient();
            // Имя текущего исполняемого файла
            string name = System.Reflection.Assembly.GetExecutingAssembly().Location;
            // Путь к каталогу с текущим исполняемый файлом
            string path = System.IO.Path.GetDirectoryName(name);
            // Полный путь к файлу конфигурации
            configName = System.IO.Path.Combine(path, "appsettings.json");
        }

        public async Task<WeatherData> Request()
        {
            // Чтение конфигурации
            IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile(configName).Build();
       
            // Конструирование строки запроса
            UriBuilder uri = new UriBuilder("https://api.openweathermap.org/data/2.5/weather");

            // Коллекция именованных параметров
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["lat"] = config.GetSection("Latitude").Value; // широта
            parameters["lon"] = config.GetSection("Longitude").Value;  // долгота
            parameters["appid"] = config.GetSection("OpenWeatherMapKey").Value;
            uri.Query = parameters.ToString();

            // Веб-запрос
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(uri.ToString())
            };

            // Выполенение запроса
            HttpResponseMessage response = client.Send(request);

            // Проверка на успешность выполнения запроса
            if (!response.IsSuccessStatusCode)
            {
                string error = await response.Content.ReadAsStringAsync();
                return null;
            }

            string json = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<Weather.WeatherData>(json);
            return data;
        }
    }
}
