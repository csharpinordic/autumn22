﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyClient
{
    internal class ApiManager
    {
        private Galaxy.API.ApiClient gapi;

        internal ApiManager()
        {
            var http = new HttpClient();
            gapi = new Galaxy.API.ApiClient("http://localhost:5000/", http);
        }

        /// <summary>
        /// Список всех игр
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Galaxy.API.Game> GetAllGames()
        {
            try
            {
                var result = gapi.GetAllAsync(null).Result.Payload;
                return result;
            }
            catch (Exception ex)
            {
                
                return null;
            }
        }
    }
}
