﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Galaxy.Common.Migrations
{
    /// <inheritdoc />
    public partial class _003 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_rl_server_Name",
                table: "rl_server",
                column: "Name");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_rl_server_Name",
                table: "rl_server");
        }
    }
}
