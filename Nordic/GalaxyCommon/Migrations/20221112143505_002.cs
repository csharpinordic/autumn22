﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Galaxy.Common.Migrations
{
    /// <inheritdoc />
    public partial class _002 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "rl_game",
                columns: table => new
                {
                    // [!] Задано значение по умолчанию - генерация уникального идентификатора
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql:"NEWID()"),
                    Prefix = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Number = table.Column<int>(type: "int", nullable: true),
                    ServerID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rl_game", x => x.ID);
                    table.ForeignKey(
                        name: "FK_rl_game_rl_server_ServerID",
                        column: x => x.ServerID,
                        principalTable: "rl_server",
                        principalColumn: "ID",
                        // [!] запрет каскадного удаления
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_rl_game_ServerID",
                table: "rl_game",
                column: "ServerID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "rl_game");
        }
    }
}
