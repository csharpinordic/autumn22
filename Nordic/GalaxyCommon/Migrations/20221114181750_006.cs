﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Galaxy.Common.Migrations
{
    /// <inheritdoc />
    public partial class _006 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_States_rl_game_GameID",
                table: "States");

            migrationBuilder.DropForeignKey(
                name: "FK_States_rl_race_PlayerID",
                table: "States");

            migrationBuilder.DropPrimaryKey(
                name: "PK_States",
                table: "States");

            migrationBuilder.RenameTable(
                name: "States",
                newName: "rl_state");

            migrationBuilder.RenameIndex(
                name: "IX_States_PlayerID",
                table: "rl_state",
                newName: "IX_rl_state_PlayerID");

            migrationBuilder.RenameIndex(
                name: "IX_States_GameID",
                table: "rl_state",
                newName: "IX_rl_state_GameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_rl_state",
                table: "rl_state",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_rl_state_rl_game_GameID",
                table: "rl_state",
                column: "GameID",
                principalTable: "rl_game",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_rl_state_rl_race_PlayerID",
                table: "rl_state",
                column: "PlayerID",
                principalTable: "rl_race",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rl_state_rl_game_GameID",
                table: "rl_state");

            migrationBuilder.DropForeignKey(
                name: "FK_rl_state_rl_race_PlayerID",
                table: "rl_state");

            migrationBuilder.DropPrimaryKey(
                name: "PK_rl_state",
                table: "rl_state");

            migrationBuilder.RenameTable(
                name: "rl_state",
                newName: "States");

            migrationBuilder.RenameIndex(
                name: "IX_rl_state_PlayerID",
                table: "States",
                newName: "IX_States_PlayerID");

            migrationBuilder.RenameIndex(
                name: "IX_rl_state_GameID",
                table: "States",
                newName: "IX_States_GameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_States",
                table: "States",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_States_rl_game_GameID",
                table: "States",
                column: "GameID",
                principalTable: "rl_game",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_States_rl_race_PlayerID",
                table: "States",
                column: "PlayerID",
                principalTable: "rl_race",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
