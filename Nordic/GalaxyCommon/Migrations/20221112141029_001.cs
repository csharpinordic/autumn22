﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Galaxy.Common.Migrations
{
    /// <inheritdoc />
    public partial class _001 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GameServers",
                table: "GameServers");

            migrationBuilder.RenameTable(
                name: "GameServers",
                newName: "rl_server");

            migrationBuilder.AddPrimaryKey(
                name: "PK_rl_server",
                table: "rl_server",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "rl_race",
                columns: table => new
                {
                    // [!] Задано значение по умолчанию - генерация уникального идентификатора
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "NEWID()"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Copyright = table.Column<bool>(type: "bit", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: true),
                    RealName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: true),
                    Icon = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: true),
                    Href = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Sheridan = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rl_race", x => x.ID);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "rl_race");

            migrationBuilder.DropPrimaryKey(
                name: "PK_rl_server",
                table: "rl_server");

            migrationBuilder.RenameTable(
                name: "rl_server",
                newName: "GameServers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameServers",
                table: "GameServers",
                column: "ID");
        }
    }
}
