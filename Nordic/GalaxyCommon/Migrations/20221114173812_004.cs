﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Galaxy.Common.Migrations
{
    /// <inheritdoc />
    public partial class _004 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Prefix",
                table: "rl_game",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<int>(
                name: "Planets",
                table: "rl_game",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Size",
                table: "rl_game",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TPW",
                table: "rl_game",
                type: "int",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Planets",
                table: "rl_game");

            migrationBuilder.DropColumn(
                name: "Size",
                table: "rl_game");

            migrationBuilder.DropColumn(
                name: "TPW",
                table: "rl_game");

            migrationBuilder.AlterColumn<string>(
                name: "Prefix",
                table: "rl_game",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}
