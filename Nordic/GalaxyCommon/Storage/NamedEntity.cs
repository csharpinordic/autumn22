﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Именованная сущность
    /// </summary>
    public abstract class NamedEntity : Entity
    {
        /// <summary>
        /// Имя сущности
        /// </summary>
        [Display(Name = "Имя")]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Строковое представление сущности
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Name;
    }
}
