﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Игровой сервер
    /// </summary>
    [Table("rl_server")]
    [Index(nameof(Name))]
    public class GameServer : NamedEntity
    {       
        /// <summary>
        /// Имя сервера
        /// </summary>
        [MaxLength(50)]
        public string Caption { get; set; }

        /// <summary>
        /// Адрес сервера в Интернет
        /// </summary>
        [MaxLength(128)]
        public string? Url { get; set; }

        /// <summary>
        /// Какой-то флаг ([!] уточнить)
        /// </summary>
        public bool Flag { get; set; }
    }
}
