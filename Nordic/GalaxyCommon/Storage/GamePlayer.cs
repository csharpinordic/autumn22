﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Игрок
    /// </summary>
    [Display(Name = "Игрок")]
    [DisplayName("Игрок")]
    [Description("Игрок")]
    [Table("rl_race")]
    public class GamePlayer : NamedEntity
    {
        /// <summary>
        /// Альтернативное имя игрока
        /// </summary>
        [MaxLength(50)]
        public string? Alias { get; set; }

        /// <summary>
        /// Признак зарегистрированного имени
        /// </summary>
        public bool Copyright { get; set; }

        /// <summary>
        /// Email игрока
        /// </summary>
        [MaxLength(63)]
        public string? Email { get; set; }

        /// <summary>
        /// Настоящее имя игрока
        /// </summary>
        [MaxLength(63)]
        public string? RealName { get; set; }

        /// <summary>
        /// Значок игрока
        /// </summary>
        [MaxLength(63)]
        public string? Icon { get; set; }

        /// <summary>
        /// Ссылка на страницу игрока
        /// </summary>
        [MaxLength(100)]
        public string? Href { get; set; }

        /// <summary>
        /// Идентификатор игрока в ордене "Шеридан"
        /// </summary>      
        public int? Sheridan { get; set; }
    }
}
