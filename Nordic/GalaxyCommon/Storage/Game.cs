﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Сведения о игровой партии
    /// </summary>
    [DisplayName("Игра")]
    [Table("rl_game")]
    public class Game : NamedEntity
    {
        /// <summary>
        /// Префикс партии
        /// </summary>
        [MaxLength(50)]
        public string? Prefix { get; set; }

        /// <summary>
        /// Номер партии
        /// </summary>
        [Column("Number")] // можно указать иное имя столбца для БД
        public int? Number { get; set; }  
        
        /// <summary>
        /// Идентификатор сервера
        /// </summary>
        public Guid ServerID { get; set; }

        /// <summary>
        /// Сервер
        /// </summary>
        [ForeignKey(nameof(ServerID))]
        public virtual GameServer Server { get; set; }

        /// <summary>
        /// Дата начала партии
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Дата окончания партии
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Размер игрового поля
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// Количество планет
        /// </summary>
        public int? Planets { get; set; }

        /// <summary>
        /// Количество ходов в неделю (turn per week)
        /// </summary>
        public int? TPW { get; set; }
    }
}
