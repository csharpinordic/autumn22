﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    /// Полезные команды:
    /// > Add-Migration nnn              : добавление миграции
    /// > Remove-Migration               : удаление миграции
    /// > Update-Database -Migration nnn : преобразование к определённой миграции (структуре)
    /// > EntityFrameworkCore\Add-Migration nnn -Context "имя_класса"
    public class Database : DbContext
    {
        #region "Таблицы базы данных"

        /// <summary>
        /// Список игровых партий
        /// </summary>
        public DbSet<Game> Games { get; set; }

        /// <summary>
        /// Список игроков
        /// </summary>
        public DbSet<GamePlayer> Players { get; set; }

        /// <summary>
        /// Список игровых серверов
        /// </summary>
        public DbSet<GameServer> Servers { get; set; }

        /// <summary>
        /// Список состояний игр для игроков
        /// </summary>
        public DbSet<GameState> States { get; set; }

        #endregion


        public Database()
        { 
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseLazyLoadingProxies();
            options.UseSqlServer("data source=.\\SQLEXPRESS; initial catalog=GALAXY; Integrated security=True; MultipleActiveResultSets=True; App=Galaxy; Encrypt=False;");
        }

        /// <summary>
        /// Конструктор класса для иньекции зависимостей
        /// </summary>
        /// <param name="options"></param>
        public Database(DbContextOptions<Database> options) : base(options)
        {
           // this.Database.Migrate();
        }
    }
}
