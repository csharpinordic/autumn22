﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaxy.Common.Storage
{
    /// <summary>
    /// Состояние игры для игрока
    /// </summary>
    [Table("rl_state")]
    public class GameState : Entity
    {
        /// <summary>
        /// Идентификатор игры
        /// </summary>
        public Guid GameID { get; set; }

        /// <summary>
        /// Игра
        /// </summary>
        [ForeignKey(nameof(GameID))]
        public virtual Game Game { get; set; }

        /// <summary>
        /// Идентификатор игрока
        /// </summary>
        public Guid PlayerID { get; set; }

        /// <summary>
        /// Игрок
        /// </summary>
        [ForeignKey(nameof(PlayerID))]
        public virtual GamePlayer Player { get; set; }

        /// <summary>
        /// Состояние
        /// </summary>     
        [MinLength(1)]
        [MaxLength(1)]
        public string State { get; set; }
    }
}
