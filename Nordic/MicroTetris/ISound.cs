﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroTetris
{
    /// <summary>
    /// Звук
    /// </summary>
    public interface ISound
    {
        /// <summary>
        /// Частота дискретизации в Гц
        /// </summary>
        public int SampleRate { get; }

        /// <summary>
        /// Собственно звук
        /// </summary>
        public byte[] Sound { get; }
    }
}
