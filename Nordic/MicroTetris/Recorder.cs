﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroTetris
{
    /// <summary>
    /// Запись речи
    /// </summary>
    public class Recorder : ISound
    {
        /// <summary>
        /// Записыватель звука
        /// </summary>
        private WaveIn wave;
        /// <summary>
        /// Записанный звук
        /// </summary>
        private List<byte> sound = new();

        /// <summary>
        /// Частота дискретизации в Гц
        /// </summary>
        public int SampleRate => wave.WaveFormat.SampleRate;

        /// <summary>
        /// Собственно звук
        /// </summary>
        public byte[] Sound => sound.ToArray();

        public Recorder()
        {
            wave = new WaveIn();
            wave.WaveFormat = new WaveFormat(Speech.SoundFrequency, 1);
            wave.DataAvailable += Wave_DataAvailable;
            wave.RecordingStopped += Wave_RecordingStopped;            
        }

        public void Start()
        {
            wave.StartRecording();
        }

        public void Stop()
        {
            wave.StopRecording();
        }

        public void Save(string name)
        {
            using WaveFileWriter writer = new WaveFileWriter(name, wave.WaveFormat);
            writer.Write(sound.ToArray());
        }

        /// <summary>
        /// Поступление звуковых данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void Wave_DataAvailable(object? sender, WaveInEventArgs e)
        {
            byte[] data = new byte[e.BytesRecorded];
            Array.Copy(e.Buffer, data, e.BytesRecorded);
            sound.AddRange(data);
        }

        /// <summary>
        /// Завершение записи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wave_RecordingStopped(object? sender, StoppedEventArgs e)
        {            
        }
    }
}
