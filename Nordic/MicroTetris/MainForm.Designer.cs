﻿namespace MicroTetris
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tool = new System.Windows.Forms.ToolStrip();
            this.newGameButton = new System.Windows.Forms.ToolStripButton();
            this.listenButton = new System.Windows.Forms.ToolStripButton();
            this.panel = new System.Windows.Forms.Panel();
            this.status = new System.Windows.Forms.StatusStrip();
            this.scoreLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tool.SuspendLayout();
            this.status.SuspendLayout();
            this.SuspendLayout();
            // 
            // tool
            // 
            this.tool.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameButton,
            this.listenButton});
            this.tool.Location = new System.Drawing.Point(0, 0);
            this.tool.Name = "tool";
            this.tool.Size = new System.Drawing.Size(272, 55);
            this.tool.TabIndex = 0;
            this.tool.Text = "toolStrip1";
            // 
            // newGameButton
            // 
            this.newGameButton.Image = global::MicroTetris.Properties.Resources.Plus;
            this.newGameButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newGameButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newGameButton.Name = "newGameButton";
            this.newGameButton.Size = new System.Drawing.Size(141, 52);
            this.newGameButton.Text = "Новая игра";
            this.newGameButton.Click += new System.EventHandler(this.newGameButton_Click);
            // 
            // listenButton
            // 
            this.listenButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.listenButton.Image = ((System.Drawing.Image)(resources.GetObject("listenButton.Image")));
            this.listenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.listenButton.Name = "listenButton";
            this.listenButton.Size = new System.Drawing.Size(92, 52);
            this.listenButton.Text = "Распознать";
            this.listenButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listenButton_MouseDown);
            this.listenButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listenButton_MouseUp);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Location = new System.Drawing.Point(0, 55);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(272, 400);
            this.panel.TabIndex = 1;
            // 
            // status
            // 
            this.status.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scoreLabel});
            this.status.Location = new System.Drawing.Point(0, 454);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(272, 26);
            this.status.TabIndex = 2;
            this.status.Text = "statusStrip1";
            // 
            // scoreLabel
            // 
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(13, 20);
            this.scoreLabel.Text = " ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 480);
            this.Controls.Add(this.status);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.tool);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Тетрис";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.tool.ResumeLayout(false);
            this.tool.PerformLayout();
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ToolStrip tool;
        private ToolStripButton newGameButton;
        private Panel panel;
        private StatusStrip status;
        private ToolStripStatusLabel scoreLabel;
        private ToolStripButton listenButton;
    }
}