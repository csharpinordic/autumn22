﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace MicroTetris
{
    public class Speech
    {
        private string FolderID = "b1g16tdtjk2q1460tbts";
        private string Key = "***";

        /// <summary>
        /// Частота дискретизации звука в Гц
        /// </summary>
        public const int SoundFrequency = 8000;

        /// <summary>
        /// HTTP-клиент для отправки запросов
        /// </summary>
        private HttpClient client = new();

        /// <summary>
        /// Синтез речи
        /// </summary>
        /// <param name="text"></param>
        public Sound Synthesis(string text)
        {
            Uri uri = new Uri("https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize");
            var parameters = new Dictionary<string, string>()
            {
                { "text", text },
                { "lang", "ru-RU" },
                { "voice", "jane" },
                { "emotion", "good" },
                { "speed", "1.0" },
                { "format", "lpcm" },
                { "sampleRateHertz", SoundFrequency.ToString() },
                { "folderId", FolderID }, // [!] удалить потом
            };

            // aje192d2dl3bkc3s5fho - ключ
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = uri,
                Content = new FormUrlEncodedContent(parameters)
            };           
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Api-Key", Key);

            HttpResponseMessage response = client.Send(request);

            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show("неудача");
                return null;
            }
            
            var sound = new Sound() { 
                Data = response.Content.ReadAsByteArrayAsync().Result,
                SampleRate = SoundFrequency
            };
            return sound;
        }

        /// <summary>
        /// Распознавание речи
        /// </summary>
        /// <param name="sound">Звук</param>
        /// <returns></returns>
        public string Recognize(ISound sound)
        {
            UriBuilder uri = new UriBuilder("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["lang"] = "ru-RU";
            parameters["format"] = "lpcm";
            parameters["sampleRateHertz"] = sound.SampleRate.ToString();
            parameters["folderId"] = FolderID;
            parameters["rawResults"] = true.ToString();
            uri.Query = parameters.ToString();

            var body = new ByteArrayContent(sound.Sound);
            // не обязательно, но полезно
            body.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("audio/vnd.wave");

            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(uri.ToString()),
                Content = body
            };
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Api-Key", Key);

            HttpResponseMessage response = client.Send(request);

            if (!response.IsSuccessStatusCode)
            {               
                return response.ReasonPhrase ?? "Ошибка запроса";
            }

            string json = response.Content.ReadAsStringAsync().Result;
            string result = System.Text.Json.JsonSerializer.Deserialize<Response>(json)?.Result ?? "Ошибка распознавания";
            return result;
        }
    }
}
