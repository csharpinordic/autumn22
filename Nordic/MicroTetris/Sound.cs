﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace MicroTetris
{
    public class Sound
    {
        private WaveOut wave = new();

        /// <summary>
        /// Массив звука в формате LPCM
        /// </summary>
        public byte[] Data { get; set; }
        /// <summary>
        /// Частота дискретизации в Гц
        /// </summary>
        public int SampleRate { get; set; }

        /// <summary>
        /// Проигрывание звука (речи)
        /// </summary>
        public void Play()
        {
            // одноканальный звук 16 бит 
            var format = new WaveFormat(SampleRate, 16, 1);
            IWaveProvider provider = new RawSourceWaveStream(Data, 0, Data.Length, format);         
            wave.Init(provider);
            wave.Play();
        }
    }
}
