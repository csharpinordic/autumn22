namespace MicroTetris
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// ������ ����
        /// </summary>
        Speech speech = new();

        /// <summary>
        /// ������ ����
        /// </summary>
        Recorder recorder;

        System.Windows.Forms.Timer timer;
        Figure figure;
        int width;
        int height;
        /// <summary>
        /// ���������� �����, ��������� �������
        /// </summary>
        int score;

        /// <summary>
        /// ���������� �����, ��������� �������
        /// <para>���� ������������� ������������ �� ������</para>
        /// </summary>
        /// 
        int Score
        {
            get => score; // ������� �����
            set
            {
                score = value;
                scoreLabel.Text = $"����: {score}";
            }
        }

        public MainForm()
        {
            InitializeComponent();

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 500;
            timer.Tick += Timer_Tick;

            width = panel.Width / Figure.FigureSize;
            height = panel.Height / Figure.FigureSize;
            panel.Width = width * Figure.FigureSize;
            panel.Height = height * Figure.FigureSize;
        }

        private void CheckField()
        {
            bool[,] field = new bool[width, height];

            // ����� ���� �����
            foreach (Figure figure in panel.Controls)
            {
                field[figure.X, figure.Y] = true;
            }

            // �������� �� ����������� ������ ����� �����
            for (int y = height - 1; y >= 0; y--)
            {
                bool flag = true;
                for (int x = 0; x < width; x++)
                {
                    flag &= field[x, y];
                }
                // ������ y ��������� ���������
                if (flag)
                {
                    // ���������� ����� ��� ��������
                    var list = new List<Figure>();
                    foreach (Figure figure in panel.Controls)
                    {
                        if (figure.Y == y)
                        {
                            // ��������� ������ ��� ��������
                            list.Add(figure);
                        }
                    }
                    // ���������� ������� ������ 
                    foreach (Figure figure in list)
                    {
                        panel.Controls.Remove(figure);
                        Score += figure.Score;
                    }

                    // ����� ���������� ����� ����
                    foreach (Figure figure in panel.Controls)
                    {
                        if (figure.Y < y)
                        {
                            // �������� ������ ����
                            figure.MoveTo(figure.X, figure.Y + 1);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ������� ������ ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object? sender, EventArgs e)
        {
            if (figure == null) return; // ��� �������� ������ �� ����

            if (figure.MoveTo(figure.X, figure.Y + 1)) return; // ������ ������� ����������

            // �� ������� ����������� ������ ����
            CheckField();

            // �������� �� ����� ����
            if (figure.Y == 0)
            {
                timer.Stop();
                figure = null;
                // ����� ����
                MessageBox.Show("����� ����");
                return;
            }
            // �������� ����� ������
            figure = new Figure(width);
            // ���������� ������ �� ������� ����
            panel.Controls.Add(figure);
        }

        /// <summary>
        /// ������ ����� ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newGameButton_Click(object sender, EventArgs e)
        {
            var sound = speech.Synthesis("���� � ������ ��������");
            sound.Play();

            // �������� ����� ������
            figure = new Figure(width);

            // ���������� ������ �� ������� ����
            panel.Controls.Add(figure);

            // ������ ������� ����
            timer.Start();

            Score = 0;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    figure.MoveTo(figure.X - 1, figure.Y);
                    break;
                case Keys.Right:
                    figure.MoveTo(figure.X + 1, figure.Y);
                    break;
                case Keys.Down:
                    figure.MoveTo(figure.X, figure.Y + 1);
                    break;
            }
        }

      
        /// <summary>
        /// ������ ������ ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listenButton_MouseDown(object sender, MouseEventArgs e)
        {
            recorder = new();
            recorder.Start();
            listenButton.ForeColor= Color.Red;
        }

        private void listenButton_MouseUp(object sender, MouseEventArgs e)
        {
            recorder.Stop();
            listenButton.ForeColor = Color.Black;
            recorder.Save(@"C:\TEMP\SOUND.WAV");
            // �������������
            string text = speech.Recognize(recorder);

            MessageBox.Show(text);
        }
    }
}