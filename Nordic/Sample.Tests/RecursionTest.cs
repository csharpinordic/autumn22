namespace Sample.Tests
{
    [TestClass]
    public class RecursionTest
    {
        [TestMethod]
        public void TestFactoral1()
        {
            Assert.AreEqual(1, Recursion.Program.Factorial(0));
            Assert.AreEqual(1, Recursion.Program.Factorial(1));
            Assert.AreEqual(2, Recursion.Program.Factorial(2));
            Assert.AreEqual(6, Recursion.Program.Factorial(3));
            Assert.AreEqual(24, Recursion.Program.Factorial(4));
            Assert.AreEqual(120, Recursion.Program.Factorial(5));
        }

        /// <summary>
        /// �������� ����, ��� n! = f
        /// </summary>
        /// <param name="n"></param>
        /// <param name="f"></param>
        [TestMethod("������������ ����������")]
        [DataRow(0, 1)]
        [DataRow(1, 1)]
        [DataRow(2, 2)]
        [DataRow(3, 6)]
        [DataRow(4, 24)]
        [DataRow(5, 120)]
        [DataRow(6, 720)]
        public void TestFactoral2(int n, long f)
        {
            Assert.AreEqual(f, Recursion.Program.Factorial(n));
        }       
    }
}