﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Galaxy.Common.Storage;

namespace GalaxyWeb.Controllers
{
    public class GameServersController : Controller
    {
        private readonly Database _context;

        public GameServersController(Database context)
        {
            _context = context;
        }

        // GET: GameServers
        public async Task<IActionResult> Index()
        {
              return View(await _context.Servers.ToListAsync());
        }

        // GET: GameServers/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null || _context.Servers == null)
            {
                return NotFound();
            }

            var gameServer = await _context.Servers
                .FirstOrDefaultAsync(m => m.ID == id);
            if (gameServer == null)
            {
                return NotFound();
            }

            return View(gameServer);
        }

        // GET: GameServers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GameServers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Caption,Url,Flag,Name,ID")] GameServer gameServer)
        {
            if (ModelState.IsValid)
            {
                gameServer.ID = Guid.NewGuid();
                _context.Add(gameServer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(gameServer);
        }

        // GET: GameServers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null || _context.Servers == null)
            {
                return NotFound();
            }

            var gameServer = await _context.Servers.FindAsync(id);
            if (gameServer == null)
            {
                return NotFound();
            }
            return View(gameServer);
        }

        // POST: GameServers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Caption,Url,Flag,Name,ID")] GameServer gameServer)
        {
            if (id != gameServer.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gameServer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameServerExists(gameServer.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(gameServer);
        }

        // GET: GameServers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null || _context.Servers == null)
            {
                return NotFound();
            }

            var gameServer = await _context.Servers
                .FirstOrDefaultAsync(m => m.ID == id);
            if (gameServer == null)
            {
                return NotFound();
            }

            return View(gameServer);
        }

        // POST: GameServers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (_context.Servers == null)
            {
                return Problem("Entity set 'Database.Servers'  is null.");
            }
            var gameServer = await _context.Servers.FindAsync(id);
            if (gameServer != null)
            {
                _context.Servers.Remove(gameServer);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GameServerExists(Guid id)
        {
          return _context.Servers.Any(e => e.ID == id);
        }
    }
}
