using Microsoft.EntityFrameworkCore;

namespace GalaxyWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();

            // ��������� ���� ������
            builder.Services.AddDbContext<Galaxy.Common.Storage.Database>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer("data source=.\\SQLEXPRESS; initial catalog=GALAXY; Integrated security=True; MultipleActiveResultSets=True; App=Galaxy; Encrypt=False;");
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}