-- Демонстрация группировки данных
SELECT
	Number, BType, COUNT(*) As PressCount
FROM [dbo].[ButtonEvents]
GROUP BY Number, BType
ORDER BY PressCount DESC