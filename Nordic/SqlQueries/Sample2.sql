-- Пример последовательных соединений таблиц при помощи JOIN
SELECT 
	A1.TimeStamp,
	A1.ID,
	AC1.Name,
	A2.ID,
	AC2.Name
FROM [dbo].[Accidents] AS A1
INNER JOIN [dbo].[Accidents] AS A2 ON A2.ID = A1.NextVersion_ID
LEFT OUTER JOIN [dbo].[AccidentClasses] AS AC1 ON AC1.ID = A1.AType_ID
LEFT OUTER JOIN [dbo].[AccidentClasses] AS AC2 ON AC2.ID = A2.AType_ID
WHERE ISNULL(A1.AType_ID,'00000000-0000-0000-0000-000000000000') <> 
      ISNULL(A2.AType_ID,'00000000-0000-0000-0000-000000000000')
ORDER BY A1.TimeStamp
