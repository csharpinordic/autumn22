-- Демонстрация группировки данных со сложным вычислением
SELECT 
	Post,
	AVG(CAST(EndTime - StartTime AS FLOAT) * 24 * 60) AS DownTime,
	AVG([Plan]) As PlanCount
FROM [dbo].[Accidents]
-- WHERE Post = '5'
GROUP BY Post
--  HAVING MIN(CAST(EndTime - StartTime AS FLOAT) * 24 * 60) > 60
ORDER BY Post
