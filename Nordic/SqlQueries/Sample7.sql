-- Демонстрация получения описания базы данных из метаданных (служебных таблиц) сервера
select 
	s.name as SchemaName,
	t.name as TableName,
	c.column_id as ColumnNumber,
	c.name as ColumnName,
	IIF (st.name = 'uniqueidentifier','GUID',st.name) as TypeName,
	c.max_length, 
	IIF(c.is_nullable = 1,'Да','Нет') as is_nullable, 
	IIF(c.is_computed = 1,'Да','Нет') as is_computed, 
	isnull (cast(pt.value as varchar(1023)), '') as TableDescription, 
	isnull (cast(pc.value as varchar(1023)), '') as ColumnDescription
from sys.tables as t
inner join sys.schemas as s on s.schema_id = t.schema_id
inner join sys.columns as c on c.object_id = t.object_id
inner join sys.types as st on st.user_type_id = c.user_type_id
left outer join sys.extended_properties pt on pt.major_id = t.object_id and pt.minor_id = 0 and pt.name = 'MS_Description'
left outer join sys.extended_properties pc on pc.major_id = t.object_id and pc.minor_id = c.column_id and pc.name = 'MS_Description'
