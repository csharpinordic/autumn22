-- Демонстрация каскадного удаления объектов
BEGIN TRAN

SELECT 'AccidentClasses' AS TableName, COUNT(*) AS RecordCount FROM [dbo].[AccidentClasses]
UNION 
SELECT 'Accidents', COUNT(*) FROM [dbo].[Accidents]

DELETE FROM [dbo].[AccidentClasses] WHERE Number = 4

SELECT 'AccidentClasses' AS TableName, COUNT(*) AS RecordCount FROM [dbo].[AccidentClasses]
UNION 
SELECT 'Accidents', COUNT(*) FROM [dbo].[Accidents]

COMMIT
-- ROLLBACK

GO

-- Восстановление целостности базы данных
UPDATE [dbo].[Accidents] SET NextVersion_ID = NULL
WHERE ID IN
(
	SELECT ID
	FROM [dbo].[Accidents] AS A1
	WHERE NextVersion_ID IS NOT NULL
	AND NOT EXISTS
	(
		SELECT ID FROM [dbo].[Accidents] AS A2 WHERE A1.NextVersion_ID = A2.ID
	)
)

