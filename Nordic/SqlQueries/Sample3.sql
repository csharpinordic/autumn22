-- Демонстрация подзапроса
SELECT * FROM [dbo].[ButtonEvents]
WHERE Number IN
(
	SELECT Number FROM [dbo].[Buttons] WHERE Side = 0
)

-- Сравнение с использованием JOIN
SELECT DISTINCT BE.* FROM [dbo].[ButtonEvents] BE
INNER JOIN [dbo].[Buttons] B ON B.Number = BE.Number
WHERE B.Side = 0
