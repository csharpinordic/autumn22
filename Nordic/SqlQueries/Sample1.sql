-- Пример последовательного соединения таблиц при помощи JOIN
SELECT A.[ID]
      ,A.[Post]
      ,[Description]
      ,A.[TimeStamp]	 
      ,AC.Name AS AccidentName
      -- ,[NextVersion_ID]
      ,[StartTime]
      ,[EndTime] 
      ,BE.BType
      ,BE.Number
      ,B.*
FROM [dbo].[Accidents] AS A
LEFT OUTER JOIN [dbo].[AccidentClasses] AS AC ON AC.ID = A.AType_ID
INNER JOIN [dbo].[ButtonEvents] AS BE ON BE.ID = A.BEvent_ID
INNER JOIN [dbo].[Buttons] AS B ON B.Number = BE.Number
-- WHERE BE.Number = 19
ORDER BY A.Timestamp
