﻿using System.Drawing;

namespace HW04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите набор символов с клавиатуры >> ");
            string words = Console.ReadLine();

            Console.Write("Введите символ для определения его количества, ввод чувствителен к регистру >> ");
            char yourChar = Console.ReadKey().KeyChar;

            int count = 0;

            foreach (var c in words)
            {
                if (c == yourChar)
                {
                    count++;
                }
            }

            Console.Write("\nКоличество повторений символа " + yourChar + " в " + words + " >> " + count + " ");
            foreach (var c in words)
            {
                if (c == yourChar)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(c);
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(c);
                    Console.ResetColor();
                }
            }
        }
    }
}