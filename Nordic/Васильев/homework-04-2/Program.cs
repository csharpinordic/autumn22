﻿using System.ComponentModel.DataAnnotations;

namespace HW04_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите строку >> ");
            string words = Console.ReadLine();

            Console.Write("Введите символ-разделитель >> ");
            char yourChar = Console.ReadKey().KeyChar;

            string[] splitter = words.Split(yourChar);
            int max = 0;

            for (int i = 0; i < splitter.Length; i++)
            {
                if (splitter[i].Length > max)
                {
                    max = splitter[i].Length;
                }
            }

            Console.Write("Максимальное длина слова >> " + max);
        }
    }
}