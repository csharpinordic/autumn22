﻿namespace HW03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов массива: ");
            int n = int.Parse(Console.ReadLine());
            double[] numbers = new double[n];

            double min = double.MaxValue;
            double max = double.MinValue;
            double summ = 0;
            double average = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                Random rnd = new Random();
                numbers[i] = rnd.Next(-100, 100);
            }

            /*
            for (int i = 0; i < numbers.Length; i++)
            {
                min = Math.Min(min, numbers[i]);
                max = Math.Max(max, numbers[i]);
                summ += numbers[i]; //
            }
            average = summ / numbers.Length;
            */

            min = numbers.Min(); //в рамках эксперимента, хочу попробавать следующую запись
            max = numbers.Max(); //будет ли она считаться корректной?
            summ = numbers.Sum();
            average = numbers.Average();

            Console.WriteLine("Минимальное значение: " + min);
            Console.WriteLine("Максимумальное значение: " + max);
            Console.WriteLine("Сумма элементов: " + summ);
            Console.WriteLine("Среднее значение элементов: " + average);
        }
    }
}