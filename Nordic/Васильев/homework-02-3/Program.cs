﻿namespace homework_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                try
                {
                    //Теория вероятности, задача 3

                    Console.Write("Введите количество учеников N для определения вероятности совпадения дней рождения > ");
                    int n = int.Parse(Console.ReadLine());
                    if (n < 2)
                    {
                        throw new ApplicationException("Число должно быть больше 1");
                    }
                    double e = Math.Pow(Math.E, -(n * (n - 1) / (2d * 365)));

                    double p = 1 - e;

                    // Форматированный вывод процентов
                    Console.Write($"Вероятность совпадения равна {p:P}");
                }
                catch (ApplicationException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.GetType().FullName} : {ex.Message}");
                    Console.WriteLine(ex.StackTrace);
                }
            }
            catch 
            {
                // ну это совсем ужас какой-то...               
            }
        }
    }
}