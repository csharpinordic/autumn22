﻿namespace homework_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Решение квадратного уравнения, задача 1

            Console.Write("Введите значение коэффициента уравнения A> ");
            double a = double.Parse(Console.ReadLine());

            Console.Write("Введите значение коэффициента уравнения B> ");
            double b = double.Parse(Console.ReadLine());

            Console.Write("Введите значение коэффициента уравнения C> ");
            double c = double.Parse(Console.ReadLine());

            double d = (b * b) - (4 * a * c);

            if (d < 0)
            {
                Console.WriteLine("Дискриминант меньше нуля, попробуйте использовать другие значения A, B, C");
                return;
            }

            double x1, x2;
            double sqrtD = (double)Math.Sqrt(d);

            x1 = (-b + sqrtD) / (2 * a);
            x2 = (-b - sqrtD) / (2 * a);

            Console.WriteLine("");
            Console.Write("Дискриминант D = " + d);
            Console.Write("Корень X1 = " + x1);
            Console.Write("Корень X2 = " + x2);
        }
    }
}