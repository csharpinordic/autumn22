﻿namespace homework_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Ищем площадь многоугольника, задача 2

            Console.Write("Введите значение стороны многоугольника А > ");
            double a = double.Parse(Console.ReadLine());

            Console.Write("Введите количество сторон многоугольника N> ");
            double n = double.Parse(Console.ReadLine());

            double square = ((a * a) * n) / (4 * Math.Tan(Math.PI / n));

            Console.Write("Площадь вашего многоугольника = " + square);
        }
    }
}