﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace MicroTetris
{
    public class Figure : Panel
    {
        public const int FigureSize = 32;

        Color[] colors = new Color[] { Color.DarkBlue, Color.Red, Color.Gold, Color.Green, Color.Pink, Color.Purple };

        public int X;
        public int Y;
        public int Score;
        /// <summary>
        /// Индекс цвета последней созданной фигуры в массиве <seealso cref="colors"/>
        /// </summary>
        static int prevColor = -1;
        /// <summary>
        /// Генератор случайных чисел
        /// </summary>
        static Random random = new Random();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wigth">Ширина игрового поля</param>
        public Figure(int wigth)
        {
            // Формирование нового цвета, чтобы отличался от предыдущего
            int nextColor;
            do
            {
                nextColor = random.Next(0, colors.Length);
            }
            while (nextColor == prevColor);
            BackColor = colors[nextColor];
            prevColor = nextColor;

            Score = nextColor + 1;
            Size = new Size(FigureSize, FigureSize);
            X = random.Next(0, wigth);
            Y = 0;
            Location = new Point(X * FigureSize, Y * FigureSize);
        }
        /// <summary>
        /// Переместить фигуру в заданную точку
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Признак успешного перемещения, false - если переместить нельзя</returns>
        public bool MoveTo(int x, int y)
        {
            //обращаемся классом к размерам родительского поля 
            int width = Parent.Width / FigureSize;
            int height = Parent.Height / FigureSize;

            // Проверка на предельные значения координат, чтобы кубик не выходил за границы окна
            if (x < 0 || x >= width || y < 0 || y >= height)
            {
                return false;
            }

            // Проверка на наложение фигур
            foreach (Figure figure in Parent.Controls)
            {
                if (figure.X == x && figure.Y == y)
                {
                    return false;
                }
            }

            X = x;
            Y = y;
            Location = new Point(X * FigureSize, Y * FigureSize);
            return true;
        }
    }
}
