using Timer = System.Windows.Forms.Timer;
using System.Linq;

namespace MicroTetris
{
    public partial class MainForm : Form
    {

        System.Windows.Forms.Timer timer;
        Figure figure;
        int width;
        int height;
        int score;

        public MainForm()
        {
            InitializeComponent();

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;

            width = panel.Width / Figure.FigureSize;
            height = panel.Height / Figure.FigureSize;
            panel.Width = width*Figure.FigureSize;
            panel.Height=height*Figure.FigureSize;
        }

        private void CheckField()
        {
            bool[,] field = new bool[width, height];

            //����� ���� �����
            foreach (Figure figure in panel.Controls)
            {
                field[figure.X, figure.Y] = true;
            }
            // �������� �� ����������� ������ ����� �����
            for (int y = height - 1; y >= 0; y--)
            {
                bool flag = true;
                for (int x = 0; x < width; x++)
                {
                    flag &= field[x, y];
                }
                // ������ y ��������� ���������
                if (flag)
                {
                    // ���������� ����� � ��������
                    var list = new List<Figure>();
                    foreach (Figure figure in panel.Controls)
                    {
                        if (figure.Y == y)
                        {
                            // ��������� ������ ��� ��������
                            list.Add(figure);
                        }
                    }
                    // ���������� ������� ������

                    foreach (Figure figure in list)
                    {
                        score += figure.Score;
                        panel.Controls.Remove(figure);
                    }
                    scoreLabel.Text = $"����: {score}";
                    // ����� ���������� ����� ����

                    foreach (Figure figure in panel.Controls)
                    {
                        if (figure.Y < y)
                        {
                            // �������� ������ ����
                            figure.MoveTo(figure.X, figure.Y + 1);
                        }
                    }
                    
                    
                }
            }
        }

        /// <summary>
        /// ������� ������ ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object? sender, EventArgs e)
        {
            // ��� �������� ������ �� ����
            if (figure == null) return;

            // ������ ������� ����������
            if (figure.MoveTo(figure.X, figure.Y + 1)) return;

            // �� ������� ����������� ������ ����
            CheckField();

            // �������� �� ����� ����
            if (figure.Y == 0)
            {
                //����� ���� - ��������� ������, ����� ��������� ������ � ��������� �������� � ����� ���������
                timer.Stop();
                figure = null;
                MessageBox.Show("����� ����");
                return;
            }
            // �������� ����� ������
            figure = new Figure(width);
            // ���������� ������ �� ������� ����
            panel.Controls.Add(figure);


        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            // �������� ����� ������
            figure = new Figure(width);


            // ���������� ������ �� ������� ����
            panel.Controls.Add(figure);

            // ������ �������
            timer.Start();

            score = 0;
            scoreLabel.Text = $"����: {score}";
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    figure.MoveTo(figure.X - 1, figure.Y);
                    break;
                case Keys.Right:
                    figure.MoveTo(figure.X + 1, figure.Y);
                    break;
                case Keys.Down:
                    figure.MoveTo(figure.X, figure.Y + 1);
                    break;
            }
        }
    }
}