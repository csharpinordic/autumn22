﻿using System.Globalization;
using System.Text;

namespace Queens
{
    internal class Program
    {
        /// <summary>
        /// Размер шахматного поля
        /// </summary>
        const int Size = 8;

        /// <summary>
        /// Ширина клетки шахматного поля в символах
        /// </summary>
        const int Width = 2;

        /// <summary>
        /// Изображение ферзя (королевы) на доске
        /// </summary>
        const string Queen = "()";

        /// <summary>
        /// Проверка на то, что бьют ли два ферзя друг друга
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        static bool IsHit(int x1, int y1, int x2, int y2)
        {
            // Если ферзи находятся на одной горизонтали
            if (y1==y2) return true;
            // Если ферзи находятся на одной вертикали
            if (x1==x2) return true;
            // Если ферзи находятся на одной диагонали \
            if ((x2-x1)==(y2-y1)) return true;
            // Если ферзи находятся на одной диагонали /
            if ((x2 - x1) == (y1 - y2)) return true;
            return false;
        }
        static void Main(string[] args)
        {
            //Описание двумерного массива
            ConsoleColor[,] board;
            bool[,] queens;
            board = new ConsoleColor[Size, Size];
            queens = new bool[Size, Size];

            // Случайная расстановка ферзей
            var random = new Random();
            for (int i = 0; i < Size; i++)
            {
                int x = random.Next(0, Size); // Случайное число от 0 до Size - 1
                int y = random.Next(0, Size); // Случайное число от 0 до Size - 1

                // Разместить ферзя в случайной клетке
                queens[x, y] = true;
            }

            //Раскрашивание доски
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    int n = x + y;

                    switch (n % 2) //Аналог if else ниже в комментарии и тернарному оператору
                    {
                        case 0:
                            board[x, y] = ConsoleColor.White;
                            break;
                        case 1:
                            board[x, y] = ConsoleColor.Black;
                            break;
                        default: // так быть не должно
                            board[x, y] = ConsoleColor.Blue;
                            break;
                    }
                    /*
                    if (n % 2 == 0)
                    {
                        board[x, y] = ConsoleColor.White;
                    }
                    else
                    {
                        board[x, y] = ConsoleColor.Black;
                    }
                    */
                    // Тернарный оператор
                    //board[x, y] = (n % 2 == 0) ? ConsoleColor.White : ConsoleColor.Black; // ? - if : - else
                }
            }

            //очистка экрана
            Console.Clear();

            // Подпись доски. Вывод букв
            StringBuilder sb = new StringBuilder(); // 3 вариант
            string label = string.Empty; //переменная строки, начиная с пустой строки
                                         // char letter = 'A'; //переменная символа. 1 вариант
            const string chars = "abcdefgh"; // 2 вариант с любыми значениями не по порядку
            for (int x = 0; x < Size; x++)
            {
                //label += letter + " "; //запись символа в строку + 1, а далее запись пробела и повторить до x < Size раз
                //label += $"{letter,Width}"; // 1 вариант использования букв по порядку
                //label += $"{chars[x],Width}"; // 2 вариант с любыми значениями не по порядку
                sb.Append($"{chars[x],Width}"); // 3 вариант
                                                //letter++; // 1 вариант использования букв по порядку
            }
            Console.WriteLine(sb.ToString().ToUpper()); //увеличение регистра

            // Вывод доски на экран
            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    switch ((x + y) % 2)
                    {
                        case 0:
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                            break;
                        case 1:
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                        default: // так быть не должно
                            throw new Exception("Так быть не должно");

                            //Console.CursorLeft = 2 * x; (шаг вправо по строке)
                            //Console.CursorTop = y;      (шаг вниз по столбцу)
                    }
                    if (queens[x, y]) // == true
                    {
                        Console.Write($"{Queen,Width}");
                    }
                    else
                    {
                        Console.Write($"{string.Empty,Width}");
                    }

                    Console.ResetColor();

                }
                Console.WriteLine();


                /*
                //генератор случайных чисел
                Random random = new Random();

                int[] numbers;
                numbers = new int[50];

                //используем запрос длины массива
                for (int i = 0; i < numbers.Length; i++) // i++ тоже самое i=i+1
                {
                    numbers[i] = random.Next(0, 1000); //формирование случайного числа
                }

                // суммирование всех элементов массива
                int summa = 0;
                for (int i = 0; i < numbers.Length; i++)
                {
                    summa = summa + numbers[i];
                }
                //Вычисление среднего значения
                double average = summa / numbers.Length;

                Console.ForegroundColor = ConsoleColor.Red; //цвет текста
                Console.BackgroundColor = ConsoleColor.Yellow; //цвет фона 

                Console.WriteLine("Среднее значение массива из " + numbers.Length + " равно " + average);

                // равно верхнему значению цикла
                number[0] = random.Next(0, 1000); //формирование случайного числа
                number[1] = random.Next(0, 1000);
                number[2] = random.Next(0, 1000);
                number[3] = random.Next(0, 1000);
                number[4] = random.Next(0, 1000);
                */

                /*
                int a = 5;
                int b = a-- - --a;
                int c = a++ + ++a;

                Console.WriteLine(a);
                Console.WriteLine(b);
                Console.WriteLine(c);
                */
            }
            Console.ReadLine();
        }

    }
}
