﻿using System.ComponentModel;
using System.Globalization;

namespace SampleApp
{
    internal class Program
    {
        /// <summary>
        /// Точка входа в программу
        /// </summary>
        /// <param name="args"> Параметры программы из командной строки </param>
        static void Main(string[] args)
        {
            /*
            int a;
            int b;
            int c; 
            a = 10;
            a = a + 10;
            b = a + 10;
            c = a + 2 * b + 3;
            int m = 7 % 3;
            */
            /*
            double radius;
            Console.Write("Введите радиус >");
            string s = Console.ReadLine();

            if (!double.TryParse(s, NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out radius))
            {
                Console.WriteLine("Введено не число: " + s);
                Console.WriteLine("Следует ввести число");
            }
            // Parse - при вводе данных не используется, т.к. нельзя предугадать значение
            radius=double.Parse(s,CultureInfo.InvariantCulture);

            double lenght = 2 * Math.PI * radius;
            double square = Math.PI * Math.Pow(radius, 2); // Pow - возведение в степень
            
            Console.WriteLine("Длина окружности =" + lenght.ToString(CultureInfo.InvariantCulture));
            Console.WriteLine("Площадь круга =" + square.ToString(CultureInfo.InvariantCulture));
            */

            Console.Write("Введите числитель дроби >");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Введите знаменатель дроби >");
            int b = int.Parse(Console.ReadLine());

            //Расширение типа данных - неявное преобразование
            double da = a;
            // Сужение типа данных - явное преобразование

            int ia = checked((int)da);

            //Конвертация
            byte[]array=BitConverter.GetBytes(b);

            if (!(b == 0) && (a / b >= 1))
            {
                Console.WriteLine("Дробь не правильная");
            }
        }
    }
}