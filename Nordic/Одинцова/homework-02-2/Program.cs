﻿namespace homework02_variant2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            double n = 4;
            double S;
            Console.WriteLine("Количество сторон многоугольника: " + n);
            Console.Write("Введите длину стороны многоугольника: ");
            double a = double.Parse(Console.ReadLine());
            
            S = (Math.Pow(a, 2) *n)/(4*Math.Tan(Math.PI/n));
            
            Console.WriteLine("Площадь многоугольника " + S);
            
          
        }
    }
}