﻿namespace homework_04_variant2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите строку текста >");
            string str = Console.ReadLine();
            Console.Write("Введите символ-разделитель >");
            // Ввод строки, первый символ которой будет разделителем
            string delimiter = Console.ReadLine();
            // Если строка пустая, то считаем, что разделитель - пробел
            char smb = string.IsNullOrEmpty(delimiter) ? ' ' : delimiter[0];

            int max = 0;

            // Преобразование строки в массив, разделённый символами 'smb'
            // StringSplitOptions.RemoveEmptyEntries - чтобы не было пустых строк
            string[] symbol = str.Split(smb, StringSplitOptions.RemoveEmptyEntries);

            for (int y = 0; y < symbol.Length; y++)
            {
                max = Math.Max(max, symbol[y].Length);
                Console.WriteLine($"{symbol[y].Length} : {max}"); //хотела посмотреть количество букв внутри слов, выдает не правильный результат!
            }

            Console.WriteLine($"Максимальная длина отдельного слова: {max}");
        }
    }
}