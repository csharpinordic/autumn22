namespace homework_07_variant1
{
    public partial class Form1 : Form
    {
        double a;
        double t;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("�� �������?", "���������� ������", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    a = double.Parse(textA.Text);
                    t = double.Parse(textT.Text);
                    panel.Invalidate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void text_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //�������� �� ������������ �����
                double d;
                TextBox text = (TextBox)sender;
                bool valid = double.TryParse(text.Text, out d);
                text.BackColor = valid ? Color.White : Color.MistyRose;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Pen pen1 = new Pen(Color.Black, 3);
            Pen pen2 = new Pen(Color.Blue, 3);

            //������������ ���
            // �������������� ��� Ox
            e.Graphics.DrawLine(pen1, 0, panel.Height / 2, panel.Width, panel.Height / 2);
            // ������������ ��� Oy
            e.Graphics.DrawLine(pen1, panel.Width / 2, 0, panel.Width / 2, panel.Height);

            int n = 350;
            // x = -3a....3a
            float x1 = (float)((2 * a * Math.Cos(0) + 1) - (a * Math.Cos(0) + 1));
            // y = -3a...3a
            float y1 = (float)((2 * a * Math.Sin(0) + 1) - (a * Math.Sin(0) + 1));

            for (double i = 0; i < 2 * Math.PI; i += 2 * Math.PI / t)
            {
                float x2 = (float)((2 * a * Math.Cos(i)) - (a * Math.Cos(2 * i)) + 1);
                float y2 = (float)((2 * a * Math.Sin(i)) - (a * Math.Sin(2 * i)) + 1);

                // (x1,y1) -- (x2,y2)
                // ������� �� '��������������' ��������� � ��������
                float sx1 = panel.Width / 2 + x1;
                float sy1 = panel.Height / 2 - y1; // ������ ��� �������� ��� Oy ���������� ����, � �� �����
                float sx2 = panel.Width / 2 + x2;
                float sy2 = panel.Height / 2 - y2;
                e.Graphics.DrawLine(pen2, sx1, sy1, sx2, sy2);
                x1 = x2;
                y1 = y2;
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            panel.Invalidate();
        }

        private void track_Scroll(object sender, EventArgs e)
        {
            textT.Text = track.Value.ToString();
            t = track.Value;
            panel.Invalidate();
        }
    }
}