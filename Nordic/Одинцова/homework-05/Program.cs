﻿using System.Drawing;

namespace homework_05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double x;
                double y;
                Console.Write("Введите значение a: ");
                string A = Console.ReadLine();
                double a = Convert.ToInt32(A);

                Console.Write("Введите значение b: ");
                string B = Console.ReadLine();
                double b = Convert.ToInt32(B);

                Console.Write("Введите значение c: ");
                string C = Console.ReadLine();
                double c = Convert.ToInt32(C);

                if (a != 0)
                {
                    y = 0;
                    x = (-c) / a;
                    Console.WriteLine($"x={x}; y={y}");
                }
                if (b != 0)
                {
                    y = (-c) / b;
                    x = 0;
                    Console.WriteLine($"x={x}; y={y}");
                }
               
            }
            catch (DivideByZeroException ex) // Проверка деления на ноль, которая в данной программе не произойдет
            {
                Console.WriteLine("Деление на ноль");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();

        }
    }
}
