﻿namespace homework02_variant3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double p; 
            
            Console.Write("Введите количество человек в группе: ");
            double n = double.Parse(Console.ReadLine());

            double stepen = -((n * (n - 1)) / (2 * 365));
            p = (1 - Math.Pow(Math.E, stepen))*100;

            /* Какой тип данных для процентов?
            double d = 0.1;
            Console.WriteLine($"{d:P}"); //вариант с использованием специального модификатора для процентов
            */
            Console.WriteLine(stepen);
            Console.WriteLine(p + "%");
        }
    }
}