﻿namespace homework_07_variant2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.button = new System.Windows.Forms.Button();
            this.textA = new System.Windows.Forms.TextBox();
            this.textK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.track = new System.Windows.Forms.TrackBar();
            this.labelFI = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.track)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Location = new System.Drawing.Point(12, 194);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(439, 248);
            this.panel.TabIndex = 0;
            this.panel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Paint);
            // 
            // button
            // 
            this.button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button.Location = new System.Drawing.Point(288, 41);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(163, 96);
            this.button.TabIndex = 1;
            this.button.Text = "Жми";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // textA
            // 
            this.textA.Location = new System.Drawing.Point(158, 41);
            this.textA.Name = "textA";
            this.textA.Size = new System.Drawing.Size(100, 23);
            this.textA.TabIndex = 2;
            this.textA.TextChanged += new System.EventHandler(this.text_TextChanged);
            // 
            // textK
            // 
            this.textK.Location = new System.Drawing.Point(158, 99);
            this.textK.Name = "textK";
            this.textK.Size = new System.Drawing.Size(100, 23);
            this.textK.TabIndex = 2;
            this.textK.TextChanged += new System.EventHandler(this.text_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введите значение a:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Введите значение k:";
            // 
            // track
            // 
            this.track.Location = new System.Drawing.Point(12, 143);
            this.track.Maximum = 360;
            this.track.Minimum = 1;
            this.track.Name = "track";
            this.track.Size = new System.Drawing.Size(440, 45);
            this.track.TabIndex = 5;
            this.track.Value = 1;
            this.track.Scroll += new System.EventHandler(this.track_Scroll);
            this.track.ValueChanged += new System.EventHandler(this.track_ValueChanged);
            // 
            // labelFI
            // 
            this.labelFI.AutoSize = true;
            this.labelFI.Location = new System.Drawing.Point(220, 173);
            this.labelFI.Name = "labelFI";
            this.labelFI.Size = new System.Drawing.Size(13, 15);
            this.labelFI.TabIndex = 6;
            this.labelFI.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 461);
            this.Controls.Add(this.labelFI);
            this.Controls.Add(this.track);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textK);
            this.Controls.Add(this.textA);
            this.Controls.Add(this.button);
            this.Controls.Add(this.panel);
            this.MinimumSize = new System.Drawing.Size(480, 500);
            this.Name = "Form1";
            this.Text = "Form1";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.track)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel panel;
        private Button button;
        private TextBox textA;
        private TextBox textK;
        private Label label1;
        private Label label2;
        private TrackBar track;
        private Label labelFI;
    }
}