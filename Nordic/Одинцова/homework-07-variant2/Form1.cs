namespace homework_07_variant2
{
    public partial class Form1 : Form
    {
        double a;
        double k;
        double fi;
        double rad;
        public Form1()
        {
            InitializeComponent();
            fi = track.Value;
            labelFI.Text = track.Value.ToString();
        }

        private void button_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("�� �������?", "���������� ������", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    a = double.Parse(textA.Text);
                    k = double.Parse(textK.Text);
                    panel.Invalidate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void text_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //�������� �� ������������ �����
                double d;
                TextBox text = (TextBox)sender;
                bool valid = double.TryParse(text.Text, out d);
                text.BackColor = valid ? Color.White : Color.MistyRose;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Pen pen1 = new Pen(Color.Black, 3);
            Pen pen2 = new Pen(Color.Red, 3);
            if (fi == 0)
                return;
            //������������ ���
            // �������������� ��� Ox
            e.Graphics.DrawLine(pen1, 0, panel.Height / 2, panel.Width, panel.Height / 2);
            // ������������ ��� Oy
            e.Graphics.DrawLine(pen1, panel.Width / 2, 0, panel.Width / 2, panel.Height);

            float r1 = (float)(a * Math.Sin(k * 0));
            float x1 = (float)(r1 * Math.Cos(0));
            float y1 = (float)(r1 * Math.Sin(0));


            for (double i = 0; i <= 360; i += fi)
            {
                rad = (float)(i * Math.PI / 180);
                float r2 = (float)(a * Math.Sin(k * rad));
                float x2 = (float)(r2 * Math.Cos(rad));
                float y2 = (float)(r2 * Math.Sin(rad));

                
                // ������� �� '��������������' ��������� � ��������
                float sx1 = panel.Width / 2 + x1;
                float sy1 = panel.Height / 2 - y1; // ������ ��� �������� ��� Oy ���������� ����, � �� �����
                float sx2 = panel.Width / 2 + x2;
                float sy2 = panel.Height / 2 - y2;
                
                e.Graphics.DrawLine(pen2, sx1, sy1, sx2, sy2);
                x1 = x2;
                y1 = y2;
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            panel.Invalidate();
        }


        private void track_ValueChanged(object sender, EventArgs e)
        {
            fi = track.Value;
            labelFI.Text = track.Value.ToString();
        }

        private void track_Scroll(object sender, EventArgs e)
        {
            labelFI.Text = track.Value.ToString();
            rad = track.Value;
            panel.Invalidate();
        }
    }
}