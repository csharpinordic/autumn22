﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace homework_04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите строку текста >");
            string str = Console.ReadLine();
            Console.Write("Введите символ, который нужно посчитать >");
            char smb = (char)Console.Read();

            // Количество найденных символов с учётом регистра
            int sumsmb = 0;
            // Количество найденных символов без учёта регистра
            int SUMSMB = 0;

            for (int x = 0; x < str.Length; x++)
            {
                if (str[x] == smb) // 'a' == 'a'
                {
                    sumsmb++;
                }

                if (char.ToUpper(str[x]) == char.ToUpper(smb)) // 'A' == 'a'
                {
                    SUMSMB++;
                }
            }
            // изменить цвет введенного символа с учетом регистра строки
            Console.Write($"Количество выбранных символов - {sumsmb}: "); //Необходимо по строчно проговорить про все методы!

            ConsoleColor clr = Console.ForegroundColor;
            for (int x = 0; x < str.Length; x++)
            {
                if (str[x] == smb)
                    Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(str[x]);
                Console.ForegroundColor = clr;
                // Вариант - Сброс цвета фона и цвета текста к исходному
                Console.ResetColor();
            }
            Console.WriteLine();

            // изменить цвет введенного символа без учета регистра строки
            Console.Write($"Количество выбранных символов - {SUMSMB}: ");

            for (int x = 0; x < str.Length; x++)
            {
                if(char.ToUpper(str[x]) == char.ToUpper(smb))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.Write(str[x]);
                Console.ForegroundColor = clr;
            }
            
        }
    }
}