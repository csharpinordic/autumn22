﻿using System;
using System.Globalization;

namespace homework_03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double min = double.MaxValue; // Почему переменной min должна присваиватся максимальная константа double.MaxValue, а к maх наоборот?
            double max = double.MinValue;

            Console.Write("Введите целое число ");
            string N = Console.ReadLine();

            int r = int.Parse(N);

            //создание массива
            double[] mass;
            mass = new double[r];

            //создание генератора случайных чисел
            Random rnd = new Random();

            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = rnd.Next(-100, 100); //формирование случайного числа
                Console.WriteLine(mass[i]);
            }

            //суммирование всех элементов массива
            double summa = 0;

            for (int i = 0; i < mass.Length; i++)
            {
                summa = summa + mass[i];

                //Минимальное значение (как программа узнает, что нужно минимальное значение вывести из массива?)
                bool x = mass[i] < min;
                if (x)
                {
                    min = mass[i];
                }

                //Максимальное значение
                max = Math.Max(max, mass[i]);
            }
            //Вычисление среднего значения
            double average = summa / mass.Length;

            Console.WriteLine("Сумма всех элементов массива " + summa);
            Console.WriteLine("Среднее значение " + average);
            Console.WriteLine("Минимальное значение " + min);
            Console.WriteLine("Максимальное значение " + max);
        }
    }
}