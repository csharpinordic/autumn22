﻿using System;
using System.Drawing;

namespace homework_06_variant2
{
    internal class Program
    {
        /// <summary>
        /// символ для фигуры
        /// </summary>
        const char symbol1 = 'O';
        const char symbol2 = ' ';
        //static int ReadNumber();
        static void Main(string[] args)
        {
            Console.Clear();
            Console.ResetColor();

            Console.Write("Введите целое число ");
            string R = Console.ReadLine();

            int number = int.Parse(R);

            //создание массива

            char[,] mass = new char[number, number]; // [x - строка, y - столбец]

            // формирование стенок
            for (int x = 0; x < number; x++)
            {
                for (int y = 0; y < number; y++)
                {
                    int n = x; //по горизонтали
                    //int n = y; // по вертикали
                    //int n = x + y; // по шахматному
                    switch (n % 2)
                    {
                        case 0:
                            mass[x, y] = symbol1;
                            break;
                        case 1:
                            mass[x, y] = symbol2;
                            break;
                        default:
                            mass[x, y] = (char)number;
                            break;
                    }

                }

            }
            for (int x = 0; x < number; x++)
            {
                for (int y = 0; y < number; y++)
                {
                    Console.Write($"{mass[x, y]}.");
                }
                Console.WriteLine();
            }
        }
    }
}
