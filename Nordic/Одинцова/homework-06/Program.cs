﻿using System;
using System.Drawing;

namespace homework_06_variant1
{
    internal class Program
    {
        /// <summary>
        /// символ для фигуры
        /// </summary>
        const char symbol = 'O';
        //static int ReadNumber();
        static void Main(string[] args)
        {
            Console.Clear();
            Console.ResetColor();

            Console.Write("Введите целое число ");
            string R = Console.ReadLine();

            int number = int.Parse(R);

            //создание массива

            char[,] mass = new char[number, number]; // [x - колонка, y - строка]

            //формирование крышки
            for (int x = 0; x < number; x++)
            {
                // Console.Write($"{symbol,2}");
                mass[x, 0] = symbol; // крышка
            }
            Console.WriteLine();

            // формирование стенок
            for (int y = 1; y < number - 1; y++)
            {
                mass[0, y] = symbol; // левая часть стенки
                // Console.Write($"{symbol,2}");
                for (int x = 1; x < number - 1; x++)
                {
                    mass[x, y] = ' '; // пустота в середине
                    // Console.Write("  ");
                }
                mass[number - 1, y] = symbol; // правая часть стенки
                // Console.WriteLine($"{symbol,2}");
            }
            // формирование донышка
            for (int x = 0; x < number; x++)
            {
                // Console.Write($"{symbol,2}");
                mass[x, number - 1] = symbol; // донышко
            }

            for (int y = 0; y < number; y++)
            {
                for (int x = 0; x < number; x++)
                {
                    Console.Write($"{mass[x, y],2}");
                }
                Console.WriteLine();
            }

            //    Console.WriteLine();
        }
    }
}
